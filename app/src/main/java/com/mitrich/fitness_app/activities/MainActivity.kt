package com.mitrich.fitness_app.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.mitrich.fitness_app.R
import com.mitrich.fitness_app.models.ExerciseGroup
import com.mitrich.fitness_app.models.TrainDay
import com.mitrich.fitness_app.models.User
import com.mitrich.fitness_app.utils.ExercisesUtils
import com.mitrich.fitness_app.utils.TrainExercisesUtils
import com.mitrich.fitness_app.views.ExerciseGroupRow
import com.mitrich.fitness_app.views.TrainDayRow
import com.google.gson.Gson
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import java.util.*
import kotlin.system.exitProcess

@Suppress("DEPRECATION")
class MainActivity : AppCompatActivity() {

    companion object {
        @SuppressLint("SdCardPath")
        const val filesDir = "/data/user/0/com.mitrich.fitness_app/files"
        const val workoutListFileName = "workoutList.json"
        const val userDataFileName = "userData.json"
        const val trainDaysListFileName = "trainDaysList.json"
        val adapter = GroupAdapter<ViewHolder>()
        var darkTheme = false
        var allOpened = false
        var firstWindow = true
        var trainProgram = false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        val currentYear = Calendar.getInstance().get(Calendar.YEAR)
        val currentMonth = Calendar.getInstance().get(Calendar.MONTH)

        if (!File("$filesDir/$userDataFileName").exists()) {
            val newUser = User(
                "Никнейм", "Статус", 0, 0, currentMonth, currentYear, 0, "Мужской", false,
                darkTheme = false,
                imperialSystem = false
            )
            File.createTempFile("userData", ".json")
            File("$filesDir/$userDataFileName").writeText(Gson().toJson(newUser))
        }

        if (!File("$filesDir/$trainDaysListFileName").exists()) {
            val jsonArray = arrayListOf<Any>()
            jsonArray.add(
                Gson().toJson(
                    TrainDay(
                        1,
                        false,
                        arrayListOf(
                            ExerciseGroup(1, false, "Ноги", arrayListOf()),
                            ExerciseGroup(2, false, "Грудь", arrayListOf()),
                            ExerciseGroup(3, false, "Спина", arrayListOf()),
                            ExerciseGroup(4, false, "Плечи", arrayListOf()),
                            ExerciseGroup(5, false, "Руки", arrayListOf()),
                            ExerciseGroup(6, false, "Прочее", arrayListOf())
                        ),
                        "Понедельник",
                        "Пн"
                    )
                )
            )
            jsonArray.add(
                Gson().toJson(
                    TrainDay(
                        2,
                        false,
                        arrayListOf(
                            ExerciseGroup(1, false, "Ноги", arrayListOf()),
                            ExerciseGroup(2, false, "Грудь", arrayListOf()),
                            ExerciseGroup(3, false, "Спина", arrayListOf()),
                            ExerciseGroup(4, false, "Плечи", arrayListOf()),
                            ExerciseGroup(5, false, "Руки", arrayListOf()),
                            ExerciseGroup(6, false, "Прочее", arrayListOf())
                        ),
                        "Вторник",
                        "Вт"
                    )
                )
            )
            jsonArray.add(
                Gson().toJson(
                    TrainDay(
                        3,
                        false,
                        arrayListOf(
                            ExerciseGroup(1, false, "Ноги", arrayListOf()),
                            ExerciseGroup(2, false, "Грудь", arrayListOf()),
                            ExerciseGroup(3, false, "Спина", arrayListOf()),
                            ExerciseGroup(4, false, "Плечи", arrayListOf()),
                            ExerciseGroup(5, false, "Руки", arrayListOf()),
                            ExerciseGroup(6, false, "Прочее", arrayListOf())
                        ),
                        "Среда",
                        "Ср"
                    )
                )
            )
            jsonArray.add(
                Gson().toJson(
                    TrainDay(
                        4,
                        false,
                        arrayListOf(
                            ExerciseGroup(1, false, "Ноги", arrayListOf()),
                            ExerciseGroup(2, false, "Грудь", arrayListOf()),
                            ExerciseGroup(3, false, "Спина", arrayListOf()),
                            ExerciseGroup(4, false, "Плечи", arrayListOf()),
                            ExerciseGroup(5, false, "Руки", arrayListOf()),
                            ExerciseGroup(6, false, "Прочее", arrayListOf())
                        ),
                        "Четверг",
                        "Чт"
                    )
                )
            )
            jsonArray.add(
                Gson().toJson(
                    TrainDay(
                        5,
                        false,
                        arrayListOf(
                            ExerciseGroup(1, false, "Ноги", arrayListOf()),
                            ExerciseGroup(2, false, "Грудь", arrayListOf()),
                            ExerciseGroup(3, false, "Спина", arrayListOf()),
                            ExerciseGroup(4, false, "Плечи", arrayListOf()),
                            ExerciseGroup(5, false, "Руки", arrayListOf()),
                            ExerciseGroup(6, false, "Прочее", arrayListOf())
                        ),
                        "Пятница",
                        "Пт"
                    )
                )
            )
            jsonArray.add(
                Gson().toJson(
                    TrainDay(
                        6,
                        false,
                        arrayListOf(
                            ExerciseGroup(1, false, "Ноги", arrayListOf()),
                            ExerciseGroup(2, false, "Грудь", arrayListOf()),
                            ExerciseGroup(3, false, "Спина", arrayListOf()),
                            ExerciseGroup(4, false, "Плечи", arrayListOf()),
                            ExerciseGroup(5, false, "Руки", arrayListOf()),
                            ExerciseGroup(6, false, "Прочее", arrayListOf())
                        ),
                        "Суббота",
                        "Сб"
                    )
                )
            )
            jsonArray.add(
                Gson().toJson(
                    TrainDay(
                        7,
                        false,
                        arrayListOf(
                            ExerciseGroup(1, false, "Ноги", arrayListOf()),
                            ExerciseGroup(2, false, "Грудь", arrayListOf()),
                            ExerciseGroup(3, false, "Спина", arrayListOf()),
                            ExerciseGroup(4, false, "Плечи", arrayListOf()),
                            ExerciseGroup(5, false, "Руки", arrayListOf()),
                            ExerciseGroup(6, false, "Прочее", arrayListOf())
                        ),
                        "Воскресенье",
                        "Вс"
                    )
                )
            )


            File.createTempFile("trainDaysList", ".json")
            File("$filesDir/$trainDaysListFileName").writeText(jsonArray.toString())
        }

        if (!File("$filesDir/$workoutListFileName").exists()) {
            val jsonArray = arrayListOf<Any>()

            jsonArray.add(Gson().toJson(ExerciseGroup(1, true, "Ноги", arrayListOf())))
            jsonArray.add(Gson().toJson(ExerciseGroup(2, true, "Грудь", arrayListOf())))
            jsonArray.add(Gson().toJson(ExerciseGroup(3, true, "Спина", arrayListOf())))
            jsonArray.add(Gson().toJson(ExerciseGroup(4, true, "Плечи", arrayListOf())))
            jsonArray.add(Gson().toJson(ExerciseGroup(5, true, "Руки", arrayListOf())))
            jsonArray.add(Gson().toJson(ExerciseGroup(6, true, "Прочее", arrayListOf())))

            File.createTempFile("workoutList", ".json")
            File("$filesDir/$workoutListFileName").writeText(jsonArray.toString())
        }

        File("$filesDir/$userDataFileName").forEachLine {
            val user = Gson().fromJson(it, User::class.java)
            darkTheme = user.darkTheme
            trainProgram = user.trainProgram
        }

        if (darkTheme) {
            setTheme(R.style.MenuActivityTheme)
        } else {
            setTheme(R.style.MenuActivityTheme_Light)
        }

        val muscleGroupsArray = arrayListOf<String>()
        adapter.clear()

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (darkTheme) {
            main_toolbar.setBackgroundResource(R.color.dark_bar)
            bottom_navigation_toolbar.setBackgroundResource(R.color.dark_bar)
        } else {
            main_toolbar.setBackgroundResource(R.color.white)
            bottom_navigation_toolbar.setBackgroundResource(R.color.white)
        }

        ic_profile.setOnClickListener {
            val intent = Intent(this, ProfileActivity::class.java)
            if (firstWindow) {
                intent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
                firstWindow = !firstWindow
            } else {
                intent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
            }
            startActivity(intent)
        }

        main_recyclerview.adapter = adapter

        main_recyclerview.addItemDecoration(
            DividerItemDecoration(
                this,
                DividerItemDecoration.VERTICAL
            )
        )

        val openAllImageView = findViewById<ImageView>(R.id.open_all_image)
        val exercisesUtils = ExercisesUtils()
        val trainExercisesUtils = TrainExercisesUtils()

        if (!trainProgram) {
            val workoutList = exercisesUtils.getWorkoutList("$filesDir/$workoutListFileName")
            for (i in 0 until workoutList.length()) {
                val jsonExerciseGroup = workoutList.getJSONObject(i)
                val exerciseGroup = Gson().fromJson(
                    jsonExerciseGroup.toString(),
                    ExerciseGroup::class.java
                )
                adapter.add(
                    ExerciseGroupRow(
                        exerciseGroup,
                        this,
                        false,
                        openAllImageView,
                        null
                    )
                )
                muscleGroupsArray.add(
                    Gson().toJson(
                        ExerciseGroup(
                            exerciseGroup.id,
                            exerciseGroup.isDisplayed,
                            exerciseGroup.name,
                            exerciseGroup.exercises
                        )
                    )
                )
            }
        } else {
            val trainDaysList = trainExercisesUtils.getTrainDaysList("$filesDir/$trainDaysListFileName")
            for (i in 0 until trainDaysList.length()) {
                val trainDay = Gson().fromJson(
                    trainExercisesUtils.getTrainDay(
                        "$filesDir/$trainDaysListFileName",
                        i + 1
                    ).toString(), TrainDay::class.java
                )
                if (trainDay.isDisplayed) {
                    adapter.add(TrainDayRow(trainDay, this, false, openAllImageView))
                }
            }

        }

        if (!trainProgram) {
            open_all_image.setOnClickListener {
                main_recyclerview.removeAllViews()
                adapter.clear()
                if (!allOpened) {
                    allOpened = !allOpened
                    open_all_image.setImageResource(R.drawable.ic_less)
                } else {
                    allOpened = !allOpened
                    open_all_image.setImageResource(R.drawable.ic_more)
                }
                val workoutList = exercisesUtils.getWorkoutList("$filesDir/$workoutListFileName")
                for (i in 0 until workoutList.length()) {
                    val jsonExerciseGroup = workoutList.getJSONObject(i)
                    val exerciseGroup = Gson().fromJson(
                        jsonExerciseGroup.toString(),
                        ExerciseGroup::class.java
                    )
                    adapter.add(
                        ExerciseGroupRow(
                            exerciseGroup,
                            this,
                            allOpened,
                            openAllImageView,
                            null
                        )
                    )
                    muscleGroupsArray.add(
                        Gson().toJson(
                            ExerciseGroup(
                                exerciseGroup.id,
                                exerciseGroup.isDisplayed,
                                exerciseGroup.name,
                                exerciseGroup.exercises
                            )
                        )
                    )
                }
            }
        } else {
            open_all_image.setOnClickListener {
                main_recyclerview.removeAllViews()
                adapter.clear()
                if (!allOpened) {
                    allOpened = !allOpened
                    open_all_image.setImageResource(R.drawable.ic_less)
                } else {
                    allOpened = !allOpened
                    open_all_image.setImageResource(R.drawable.ic_more)
                }
                val trainDaysList =
                    trainExercisesUtils.getTrainDaysList("$filesDir/$trainDaysListFileName")
                for (i in 0 until trainDaysList.length()) {
                    val trainDay = Gson().fromJson(trainExercisesUtils.getTrainDay("$filesDir/$trainDaysListFileName", i + 1).toString(), TrainDay::class.java)
                    if (trainDay.isDisplayed) {
                        adapter.add(TrainDayRow(trainDay, this, allOpened, openAllImageView))
                    }
                }
            }
        }

        if (!trainProgram) {
            val touchHelper = ItemTouchHelper(object :
                ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP or ItemTouchHelper.DOWN, 0) {
                override fun onMove(
                    p0: RecyclerView,
                    p1: RecyclerView.ViewHolder,
                    p2: RecyclerView.ViewHolder
                ): Boolean {
                    val sourcePosition = p1.adapterPosition
                    val targetPosition = p2.adapterPosition
                    Collections.swap(muscleGroupsArray, sourcePosition, targetPosition)
                    adapter.notifyItemMoved(sourcePosition, targetPosition)
                    File("$filesDir/$workoutListFileName").writeText(
                        muscleGroupsArray.toString()
                    )
                    return true
                }

                override fun onSwiped(p0: RecyclerView.ViewHolder, p1: Int) {}
            })
            touchHelper.attachToRecyclerView(main_recyclerview)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finishAffinity()
        exitProcess(1)
    }
}
