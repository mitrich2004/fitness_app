@file:Suppress("DEPRECATION")

package com.mitrich.fitness_app.activities

import android.app.Dialog
import android.content.Intent
import android.content.res.Resources
import android.graphics.*
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.view.Gravity
import android.view.View
import android.view.Window
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.exifinterface.media.ExifInterface
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.mitrich.fitness_app.R
import com.mitrich.fitness_app.models.TrainDay
import com.mitrich.fitness_app.models.User
import com.mitrich.fitness_app.utils.ProfileUtils
import com.mitrich.fitness_app.utils.TrainExercisesUtils
import com.mitrich.fitness_app.views.GoalWeightRow
import com.mitrich.fitness_app.views.ProfileRow
import com.mitrich.fitness_app.views.SexRow
import com.mitrich.fitness_app.views.WeekDayItem
import com.google.gson.Gson
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_exercise_results.weight_layout
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.add_days_dialog.*
import kotlinx.android.synthetic.main.date_dialog.*
import kotlinx.android.synthetic.main.height_dialog.*
import kotlinx.android.synthetic.main.input_dialog.*
import kotlinx.android.synthetic.main.sex_dialog.*
import kotlinx.android.synthetic.main.weight_dialog.*
import java.io.File
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.util.*
import kotlin.collections.ArrayList

class ProfileActivity : AppCompatActivity() {
    private lateinit var user: User

    override fun onCreate(savedInstanceState: Bundle?) {

        if (MainActivity.darkTheme) {
            setTheme(R.style.MenuActivityTheme)
        } else {
            setTheme(R.style.MenuActivityTheme_Light)
        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        if (MainActivity.darkTheme) {
            bottom_navigation_toolbar.setBackgroundResource(R.color.dark_bar)
            profile_layout.setBackgroundResource(R.drawable.dark_rounded_layout)
            settings_layout.setBackgroundResource(R.drawable.dark_rounded_layout)
        } else {
            bottom_navigation_toolbar.setBackgroundResource(R.color.white)
            profile_layout.setBackgroundResource(R.drawable.rounded_layout)
            settings_layout.setBackgroundResource(R.drawable.rounded_layout)
        }

        theme_switcher.isChecked = MainActivity.darkTheme

        ic_home.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
            startActivity(intent)
        }

        val trainExercisesUtils = TrainExercisesUtils()
        val profileUtils = ProfileUtils()
        val trainFilePath = "${MainActivity.filesDir}/${MainActivity.trainDaysListFileName}"
        val userFilePath = "${MainActivity.filesDir}/${MainActivity.userDataFileName}"
        var weightName = "кг"

        val adapter = GroupAdapter<ViewHolder>()
        edit_profile_recyclerview.adapter = adapter
        edit_profile_recyclerview.addItemDecoration(
            DividerItemDecoration(
                this,
                DividerItemDecoration.VERTICAL
            )
        )

        val daysAdapter = GroupAdapter<ViewHolder>()
        days_recyclerview.adapter = daysAdapter
        days_recyclerview.addItemDecoration(
            DividerItemDecoration(
                this,
                DividerItemDecoration.HORIZONTAL
            )
        )

        File(userFilePath).forEachLine {
            user = Gson().fromJson(it, User::class.java)

            //system_switcher.isChecked = user.imperialSystem
            program_switcher.isChecked = user.trainProgram

            days_recyclerview.visibility = if (user.trainProgram) View.VISIBLE else View.GONE

            user_username_text.text = user.username
            user_quote_text.text = user.quote

            val currentYear = Calendar.getInstance().get(Calendar.YEAR)
            val currentMonth = Calendar.getInstance().get(Calendar.MONTH)
            weightName = if (user.imperialSystem) {
                "lb"
            } else {
                "кг"
            }

            val userAge = if (user.monthOfBirth < currentMonth + 1) {
                currentYear - user.yearOfBirth
            } else {
                currentYear - user.yearOfBirth - 1
            }

            adapter.add(ProfileRow("Пол", user.sex))
            adapter.add(ProfileRow("Возраст", userAge.toString()))
            adapter.add(ProfileRow("Рост", user.height.toString()))

            adapter.add(
                ProfileRow(
                    "Вес",
                    user.currentWeight.toString() + weightName
                )
            )
            adapter.add(GoalWeightRow(user.goalWeight.toString() + weightName))

        }

        val screenWidth = Resources.getSystem().displayMetrics.widthPixels

        val trainDaysList = trainExercisesUtils.getTrainDaysList(trainFilePath)
        for (i in 0 until trainDaysList.length()) {
            val trainDay =
                Gson().fromJson(
                    trainExercisesUtils.getTrainDay(trainFilePath, i + 1).toString(),
                    TrainDay::class.java
                )
            daysAdapter.add(WeekDayItem(trainDay, screenWidth))
        }

        daysAdapter.setOnItemClickListener { item, _ ->
            var trainDaysExist = false
            val weekDay = item as WeekDayItem

            val trainDay = trainExercisesUtils.changeDayVisibility(
                trainFilePath,
                weekDay.trainDay.id,
                !weekDay.trainDay.isDisplayed
            )

            daysAdapter.removeGroup(weekDay.trainDay.id - 1)
            daysAdapter.add(weekDay.trainDay.id - 1, WeekDayItem(trainDay, screenWidth))

            if (weekDay.trainDay.isDisplayed) {
                for (i in 0 until daysAdapter.itemCount) {
                    val day = daysAdapter.getGroup(i) as WeekDayItem
                    if (day.trainDay.isDisplayed) {
                        trainDaysExist = true
                    }
                }

                if (!trainDaysExist) {
                    days_recyclerview.visibility = View.GONE
                    program_switcher.isChecked = false
                    user = profileUtils.changeProgram(userFilePath, false)
                }
            }
        }

        user_avatar.setOnClickListener {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE),
                1
            )
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(intent, 0)
        }

        user_username_text.setOnClickListener {
            val usernameText = findViewById<TextView>(R.id.user_username_text)

            val inputDialog = Dialog(this)
            inputDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            inputDialog.setCancelable(true)
            inputDialog.setContentView(R.layout.input_dialog)
            inputDialog.window!!.setGravity(Gravity.BOTTOM)
            inputDialog.window!!.attributes.windowAnimations = R.style.DialogAnimation
            inputDialog.window!!.setLayout(
                ConstraintLayout.LayoutParams.MATCH_PARENT,
                ConstraintLayout.LayoutParams.WRAP_CONTENT
            )
            val cancelButton = inputDialog.findViewById<Button>(R.id.cancel_button)
            val saveButton = inputDialog.findViewById<Button>(R.id.save_button)
            inputDialog.edit_layout_title.text = "Имя"
            inputDialog.editText.setText(user.username)
            inputDialog.editText.inputType = android.text.InputType.TYPE_TEXT_FLAG_CAP_WORDS

            inputDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            if (MainActivity.darkTheme) {
                inputDialog.input_layout.setBackgroundResource(R.drawable.dark_dialog_background)
            } else {
                inputDialog.input_layout.setBackgroundResource(R.drawable.light_dialog_background)
            }

            if (MainActivity.darkTheme) {
                cancelButton.setBackgroundResource(R.drawable.dark_theme_button_background)
            } else {
                cancelButton.setBackgroundResource(R.drawable.light_theme_button_background)
            }
            saveButton.setBackgroundResource(R.drawable.active_save_button_background)
            saveButton.setTextColor(Color.WHITE)

            inputDialog.editText.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {}

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    if (inputDialog.editText.text.toString()
                            .isNotBlank() && inputDialog.editText.text.toString().isNotEmpty()
                    ) {
                        saveButton.setBackgroundResource(R.drawable.active_save_button_background)
                        saveButton.setTextColor(Color.WHITE)
                    } else {
                        saveButton.setTextColor(Color.GRAY)
                        if (MainActivity.darkTheme) {
                            saveButton.setBackgroundResource(R.drawable.dark_theme_button_background)
                        } else {
                            saveButton.setBackgroundResource(R.drawable.light_theme_button_background)
                        }
                    }
                }
            })

            cancelButton.setOnClickListener {
                inputDialog.dismiss()
            }

            saveButton.setOnClickListener {
                if (inputDialog.editText.text.toString().isNotBlank()) {
                    user = profileUtils.changeName(
                        userFilePath,
                        inputDialog.editText.text.toString().trim()
                    )
                    usernameText.text = user.username
                    inputDialog.dismiss()
                }
            }

            inputDialog.show()
        }

        user_quote_text.setOnClickListener {
            val quoteText = findViewById<TextView>(R.id.user_quote_text)
            val inputDialog = Dialog(this)
            inputDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            inputDialog.setCancelable(true)
            inputDialog.setContentView(R.layout.input_dialog)
            inputDialog.window!!.setGravity(Gravity.BOTTOM)
            inputDialog.window!!.attributes.windowAnimations = R.style.DialogAnimation
            inputDialog.window!!.setLayout(
                ConstraintLayout.LayoutParams.MATCH_PARENT,
                ConstraintLayout.LayoutParams.WRAP_CONTENT
            )
            val cancelButton = inputDialog.findViewById<Button>(R.id.cancel_button)
            val saveButton = inputDialog.findViewById<Button>(R.id.save_button)
            inputDialog.edit_layout_title.text = "Статус"
            inputDialog.editText.setText(user.quote)
            inputDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            inputDialog.editText.inputType = android.text.InputType.TYPE_TEXT_FLAG_CAP_SENTENCES

            inputDialog.editText.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {}

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    if (inputDialog.editText.text.toString()
                            .isNotBlank() && inputDialog.editText.text.toString().isNotEmpty()
                    ) {
                        saveButton.setBackgroundResource(R.drawable.active_save_button_background)
                        saveButton.setTextColor(Color.WHITE)
                    } else {
                        saveButton.setTextColor(Color.GRAY)
                        if (MainActivity.darkTheme) {
                            saveButton.setBackgroundResource(R.drawable.dark_theme_button_background)
                        } else {
                            saveButton.setBackgroundResource(R.drawable.light_theme_button_background)
                        }
                    }
                }
            })

            if (MainActivity.darkTheme) {
                cancelButton.setBackgroundResource(R.drawable.dark_theme_button_background)
                inputDialog.input_layout.setBackgroundResource(R.drawable.dark_dialog_background)
            } else {
                cancelButton.setBackgroundResource(R.drawable.light_theme_button_background)
                inputDialog.input_layout.setBackgroundResource(R.drawable.light_dialog_background)
            }
            saveButton.setBackgroundResource(R.drawable.active_save_button_background)
            saveButton.setTextColor(Color.WHITE)

            cancelButton.setOnClickListener {
                inputDialog.dismiss()
            }

            saveButton.setOnClickListener {
                if (inputDialog.editText.text.toString().isNotBlank()) {
                    inputDialog.dismiss()
                    user = profileUtils.changeQuote(
                        userFilePath,
                        inputDialog.editText.text.toString().trim()
                    )
                    quoteText.text = user.quote
                }
            }

            inputDialog.show()
        }

        adapter.setOnItemClickListener { item, _ ->
            try {
                val itemUser = item as ProfileRow

                when (itemUser.title) {
                    "Пол" -> {
                        val chooseDialog = Dialog(this)
                        chooseDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                        chooseDialog.setCancelable(true)
                        chooseDialog.setContentView(R.layout.sex_dialog)
                        chooseDialog.window!!.setGravity(Gravity.BOTTOM)
                        chooseDialog.window!!.attributes.windowAnimations = R.style.DialogAnimation
                        chooseDialog.window!!.setLayout(
                            ConstraintLayout.LayoutParams.MATCH_PARENT,
                            ConstraintLayout.LayoutParams.WRAP_CONTENT
                        )
                        val chooseAdapter = GroupAdapter<ViewHolder>()
                        chooseDialog.sex_recyclerview.adapter = chooseAdapter
                        if (user.sex == "Мужской") {
                            chooseAdapter.add(SexRow(true, user.sex))
                            chooseAdapter.add(SexRow(false, "Женский"))
                        } else {
                            chooseAdapter.add(SexRow(false, "Мужской"))
                            chooseAdapter.add(SexRow(true, user.sex))
                        }
                        chooseDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

                        if (MainActivity.darkTheme) {
                            chooseDialog.choose_layout.setBackgroundResource(R.drawable.dark_dialog_background)
                            chooseDialog.cancel_sex_button.setBackgroundResource(R.drawable.dark_theme_button_background)
                        } else {
                            chooseDialog.choose_layout.setBackgroundResource(R.drawable.light_dialog_background)
                            chooseDialog.cancel_sex_button.setBackgroundResource(R.drawable.light_theme_button_background)
                        }

                        chooseDialog.cancel_sex_button.setOnClickListener {
                            chooseDialog.dismiss()
                        }

                        chooseAdapter.setOnItemClickListener { row, _ ->
                            val adapterItem = row as SexRow

                            val currentYear = Calendar.getInstance().get(Calendar.YEAR)
                            val currentMonth = Calendar.getInstance().get(Calendar.MONTH)

                            val age: Int = if (user.monthOfBirth < currentMonth + 1) {
                                currentYear - user.yearOfBirth
                            } else {
                                currentYear - user.yearOfBirth - 1
                            }

                            var idealWeight = user.height - 110

                            if (age !in 20..50) {
                                idealWeight -= 10
                            }

                            if (adapterItem.sex == "Женский") {
                                idealWeight -= 10
                            }

                            if (idealWeight < 30) {
                                idealWeight = 30
                            }

                            user = profileUtils.changeSex(userFilePath, adapterItem.sex, idealWeight)

                            adapter.removeGroup(4)
                            adapter.add(4, GoalWeightRow(idealWeight.toString() + weightName))
                            adapter.removeGroup(0)
                            adapter.add(0, ProfileRow("Пол", user.sex))


                            chooseDialog.dismiss()
                            chooseAdapter.clear()

                        }

                        chooseDialog.show()
                    }
                    "Рост" -> {
                        val heightDialog = Dialog(this)
                        heightDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                        heightDialog.setCancelable(true)
                        heightDialog.setContentView(R.layout.height_dialog)
                        heightDialog.window!!.setGravity(Gravity.BOTTOM)
                        heightDialog.window!!.attributes.windowAnimations = R.style.DialogAnimation
                        heightDialog.window!!.setLayout(
                            ConstraintLayout.LayoutParams.MATCH_PARENT,
                            ConstraintLayout.LayoutParams.WRAP_CONTENT
                        )
                        val cancelButton = heightDialog.findViewById<Button>(R.id.cancel_button)
                        val saveButton = heightDialog.findViewById<Button>(R.id.save_button)
                        heightDialog.number_picker.minValue = 120
                        heightDialog.number_picker.maxValue = 210
                        if (user.height == 0 || user.height < 120) {
                            heightDialog.number_picker.value = 160
                        } else {
                            heightDialog.number_picker.value = user.height
                        }
                        heightDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

                        if (MainActivity.darkTheme) {
                            cancelButton.setBackgroundResource(R.drawable.dark_theme_button_background)
                            heightDialog.height_layout.setBackgroundResource(R.drawable.dark_dialog_background)
                        } else {
                            cancelButton.setBackgroundResource(R.drawable.light_theme_button_background)
                            heightDialog.height_layout.setBackgroundResource(R.drawable.light_dialog_background)
                        }

                        cancelButton.setOnClickListener {
                            heightDialog.dismiss()
                        }

                        saveButton.setOnClickListener {
                            adapter.removeGroup(2)
                            adapter.add(
                                2,
                                ProfileRow("Рост", heightDialog.number_picker.value.toString())
                            )

                            val currentYear = Calendar.getInstance().get(Calendar.YEAR)
                            val currentMonth = Calendar.getInstance().get(Calendar.MONTH)

                            val age: Int = if (user.monthOfBirth < currentMonth + 1) {
                                currentYear - user.yearOfBirth
                            } else {
                                currentYear - user.yearOfBirth - 1
                            }

                            var idealWeight = heightDialog.number_picker.value - 110

                            if (age !in 20..50) {
                                idealWeight -= 10
                            }

                            if (user.sex == "Женский") {
                                idealWeight -= 10
                            }

                            if (idealWeight < 30) {
                                idealWeight = 30
                            }

                            adapter.removeGroup(4)

                            adapter.add(4, GoalWeightRow(idealWeight.toString() + weightName))

                            user = profileUtils.changeHeight(
                                userFilePath,
                                idealWeight,
                                heightDialog.number_picker.value
                            )

                            heightDialog.dismiss()
                        }

                        heightDialog.show()
                    }

                    "Вес" -> {
                        val weightDialog = Dialog(this)
                        weightDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                        weightDialog.setCancelable(true)
                        weightDialog.setContentView(R.layout.weight_dialog)
                        weightDialog.window!!.attributes.windowAnimations = R.style.DialogAnimation
                        weightDialog.window!!.setGravity(Gravity.BOTTOM)
                        weightDialog.window!!.setLayout(
                            ConstraintLayout.LayoutParams.MATCH_PARENT,
                            ConstraintLayout.LayoutParams.WRAP_CONTENT
                        )
                        val cancelButton = weightDialog.findViewById<Button>(R.id.cancel_button)
                        val saveButton = weightDialog.findViewById<Button>(R.id.save_button)
                        weightDialog.kg_number_picker.minValue = 20
                        weightDialog.kg_number_picker.maxValue = 150
                        if (user.currentWeight == 0) {
                            weightDialog.kg_number_picker.value = 60
                        } else {
                            weightDialog.kg_number_picker.value = user.currentWeight
                        }

                        weightDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

                        if (MainActivity.darkTheme) {
                            cancelButton.setBackgroundResource(R.drawable.dark_theme_button_background)
                            weightDialog.weight_layout.setBackgroundResource(R.drawable.dark_dialog_background)
                        } else {
                            cancelButton.setBackgroundResource(R.drawable.light_theme_button_background)
                            weightDialog.weight_layout.setBackgroundResource(R.drawable.light_dialog_background)
                        }

                        cancelButton.setOnClickListener {
                            weightDialog.dismiss()
                        }

                        saveButton.setOnClickListener {
                            val currentWeight = weightDialog.kg_number_picker.value
                            user = profileUtils.changeWeight(userFilePath, currentWeight)
                            adapter.removeGroup(3)
                            adapter.add(
                                3,
                                ProfileRow(
                                    "Вес",
                                    currentWeight.toString().substring(
                                        0,
                                        weightDialog.kg_number_picker.value.toString().length
                                    ) + weightName
                                )
                            )

                            weightDialog.dismiss()
                        }
                        weightDialog.show()
                    }
                    "Возраст" -> {
                        val ageDialog = Dialog(this)
                        ageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                        ageDialog.setCancelable(true)
                        ageDialog.setContentView(R.layout.date_dialog)
                        ageDialog.window!!.attributes.windowAnimations = R.style.DialogAnimation
                        ageDialog.window!!.setGravity(Gravity.BOTTOM)
                        ageDialog.window!!.setLayout(
                            ConstraintLayout.LayoutParams.MATCH_PARENT,
                            ConstraintLayout.LayoutParams.WRAP_CONTENT
                        )

                        val monthNumberPicker =
                            ageDialog.findViewById<NumberPicker>(R.id.month_number_picker)
                        val yearNumberPicker =
                            ageDialog.findViewById<NumberPicker>(R.id.year_number_picker)
                        val cancelButton = ageDialog.findViewById<Button>(R.id.cancel_button)
                        val saveButton = ageDialog.findViewById<Button>(R.id.save_button)

                        val currentYear = Calendar.getInstance().get(Calendar.YEAR)
                        val currentMonth = Calendar.getInstance().get(Calendar.MONTH)

                        monthNumberPicker.minValue = 1
                        monthNumberPicker.maxValue = 12
                        monthNumberPicker.value = user.monthOfBirth

                        yearNumberPicker.minValue = currentYear - 80
                        yearNumberPicker.maxValue = currentYear - 1
                        yearNumberPicker.value = user.yearOfBirth

                        ageDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

                        if (MainActivity.darkTheme) {
                            cancelButton.setBackgroundResource(R.drawable.dark_theme_button_background)
                            ageDialog.age_layout.setBackgroundResource(R.drawable.dark_dialog_background)
                        } else {
                            cancelButton.setBackgroundResource(R.drawable.light_theme_button_background)
                            ageDialog.age_layout.setBackgroundResource(R.drawable.light_dialog_background)
                        }

                        cancelButton.setOnClickListener {
                            ageDialog.dismiss()
                        }

                        saveButton.setOnClickListener {
                            val age: Int = if (monthNumberPicker.value < currentMonth + 1) {
                                currentYear - yearNumberPicker.value
                            } else {
                                currentYear - yearNumberPicker.value - 1
                            }

                            var idealWeight = user.height - 110

                            if (age !in 20..50) {
                                idealWeight -= 10
                            }

                            if (user.sex == "Женский") {
                                idealWeight -= 10
                            }

                            if (idealWeight < 30) {
                                idealWeight = 30
                            }

                            user = profileUtils.changeAge(
                                userFilePath,
                                monthNumberPicker.value,
                                yearNumberPicker.value,
                                idealWeight
                            )

                            adapter.removeGroup(4)
                            adapter.add(4, GoalWeightRow(idealWeight.toString() + weightName))
                            adapter.removeGroup(1)
                            adapter.add(1, ProfileRow("Возраст", age.toString()))

                            ageDialog.dismiss()
                        }
                        ageDialog.show()
                    }
                }
            } catch (e: ClassCastException) {
                e.printStackTrace()
            }
        }

        theme_switcher.setOnClickListener {
            user = profileUtils.changeTheme(userFilePath, theme_switcher.isChecked)
            MainActivity.darkTheme = user.darkTheme
            recreate()
        }

        /*system_switcher.setOnClickListener {
            user = profileUtils.changeSystem(userFilePath, system_switcher.isChecked)

            weightName = if (user.imperialSystem) {
                "lb"
            } else {
                "кг"
            }
            adapter.removeGroup(4)
            adapter.removeGroup(3)
            adapter.add(3, ProfileRow("Вес", user.currentWeight.toString() + weightName))
            adapter.add(4, GoalWeightRow(user.goalWeight.toString() + weightName))
        }*/

        program_switcher.setOnClickListener {
            var daysAreShown = false
            val trainDays = trainExercisesUtils.getTrainDaysList(trainFilePath)
            for (i in 0 until trainDays.length()) {
                val trainDay =
                    Gson().fromJson(
                        trainExercisesUtils.getTrainDay(trainFilePath, i + 1).toString(),
                        TrainDay::class.java
                    )
                if (trainDay.isDisplayed) {
                    daysAreShown = true
                }
            }
            if (program_switcher.isChecked && daysAreShown) {
                days_recyclerview.visibility = View.VISIBLE
                user = profileUtils.changeProgram(userFilePath, program_switcher.isChecked)
            } else if (!program_switcher.isChecked) {
                days_recyclerview.visibility = View.GONE
                user = profileUtils.changeProgram(userFilePath, program_switcher.isChecked)
            } else if (!daysAreShown && program_switcher.isChecked) {
                val addDaysDialog = Dialog(this)
                addDaysDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                addDaysDialog.setCancelable(false)
                addDaysDialog.setContentView(R.layout.add_days_dialog)
                addDaysDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                val cancelButton = addDaysDialog.findViewById<Button>(R.id.cancel_button)
                val saveButton = addDaysDialog.findViewById<Button>(R.id.save_button)
                val addDaysRecyclerView =
                    addDaysDialog.findViewById<RecyclerView>(R.id.add_days_recyclerview)
                saveButton.setTextColor(Color.GRAY)
                if (MainActivity.darkTheme) {
                    saveButton.setBackgroundResource(R.drawable.dark_theme_button_background)
                    cancelButton.setBackgroundResource(R.drawable.dark_theme_button_background)
                    addDaysDialog.add_days_layout.setBackgroundResource(R.drawable.dark_dialog_background)
                } else {
                    cancelButton.setBackgroundResource(R.drawable.light_theme_button_background)
                    saveButton.setBackgroundResource(R.drawable.light_theme_button_background)
                    addDaysDialog.add_days_layout.setBackgroundResource(R.drawable.light_dialog_background)
                }

                val addDaysAdapter = GroupAdapter<ViewHolder>()
                addDaysRecyclerView.adapter = addDaysAdapter
                addDaysRecyclerView.addItemDecoration(
                    DividerItemDecoration(
                        this,
                        DividerItemDecoration.HORIZONTAL
                    )
                )

                for (i in 0 until trainDays.length()) {
                    val day =
                        Gson().fromJson(
                            trainExercisesUtils.getTrainDay(trainFilePath, i + 1).toString(),
                            TrainDay::class.java
                        )
                    addDaysAdapter.add(WeekDayItem(day, screenWidth))
                }

                cancelButton.setOnClickListener {
                    addDaysDialog.dismiss()
                    program_switcher.isChecked = false
                    user = profileUtils.changeProgram(userFilePath, program_switcher.isChecked)
                    for (i in 0..6) {
                        trainExercisesUtils.changeDayVisibility(
                            trainFilePath,
                            i + 1,
                            false
                        )
                    }
                }

                val addedDaysIdList = ArrayList<Int>()

                addDaysAdapter.setOnItemClickListener { item, _ ->
                    val weekDay = item as WeekDayItem

                    addDaysAdapter.removeGroup(weekDay.trainDay.id - 1)
                    addDaysAdapter.add(
                        weekDay.trainDay.id - 1,
                        WeekDayItem(
                            TrainDay(
                                weekDay.trainDay.id,
                                !weekDay.trainDay.isDisplayed,
                                weekDay.trainDay.muscleGroupsList,
                                weekDay.trainDay.name,
                                weekDay.trainDay.shortName
                            ), screenWidth)
                    )

                    if (!weekDay.trainDay.isDisplayed) {
                        daysAreShown = true
                        addedDaysIdList.add(weekDay.trainDay.id)
                        saveButton.setBackgroundResource(R.drawable.active_save_button_background)
                        saveButton.setTextColor(Color.WHITE)
                    } else {
                        daysAreShown = false
                        addedDaysIdList.remove(weekDay.trainDay.id)
                        for (i in 0 until addDaysAdapter.itemCount) {
                            val day = addDaysAdapter.getGroup(i) as WeekDayItem
                            if (day.trainDay.isDisplayed) {
                                daysAreShown = true
                            }
                        }

                        if (!daysAreShown) {
                            saveButton.setTextColor(Color.GRAY)
                            if (MainActivity.darkTheme) {
                                saveButton.setBackgroundResource(R.drawable.dark_theme_button_background)
                            } else {
                                saveButton.setBackgroundResource(R.drawable.light_theme_button_background)
                            }
                        }
                    }
                }

                saveButton.setOnClickListener {
                    if (daysAreShown) {
                        daysAdapter.clear()
                        user = profileUtils.changeProgram(userFilePath, program_switcher.isChecked)

                        addedDaysIdList.forEach {
                            trainExercisesUtils.changeDayVisibility(trainFilePath, it, true)
                        }

                        val daysList = trainExercisesUtils.getTrainDaysList(trainFilePath)
                        for (i in 0 until daysList.length()) {
                            val trainDay =
                                Gson().fromJson(
                                    trainExercisesUtils.getTrainDay(trainFilePath, i + 1)
                                        .toString(),
                                    TrainDay::class.java
                                )
                            daysAdapter.add(WeekDayItem(trainDay, screenWidth))
                        }

                        days_recyclerview.visibility = View.VISIBLE
                        addDaysDialog.dismiss()
                    }
                }

                addDaysDialog.show()

            }
        }

        try {
            val file = File(MainActivity.filesDir, "profile.img")
            val avatarImage = findViewById<ImageView>(R.id.user_avatar)
            val bitmap = BitmapFactory.decodeStream(FileInputStream(file))
            avatarImage.setImageBitmap(bitmap)
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 0) {
            try {
                user_avatar.setImageURI(data!!.data)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        try {
            val imageFile = File(data!!.data!!.lastPathSegment!!)
            val matrix = Matrix()
            var rotate = 0
            val exif = ExifInterface(imageFile.absolutePath)

            when (exif.getAttributeInt(
                ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_NORMAL
            )) {
                ExifInterface.ORIENTATION_ROTATE_270 -> rotate = 270
                ExifInterface.ORIENTATION_ROTATE_180 -> rotate = 180
                ExifInterface.ORIENTATION_ROTATE_90 -> rotate = 90
            }

            matrix.postRotate(rotate.toFloat())
            val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, data.data)
            val rotatedBitmap =
                Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
            val fos = FileOutputStream(File(MainActivity.filesDir, "profile.img"))
            rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}
