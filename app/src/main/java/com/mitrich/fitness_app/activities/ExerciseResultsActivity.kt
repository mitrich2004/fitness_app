package com.mitrich.fitness_app.activities

import android.app.Dialog
import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.ColorDrawable
import android.icu.text.SimpleDateFormat
import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.InputType
import android.text.TextWatcher
import android.view.Window
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.google.gson.Gson
import com.mitrich.fitness_app.R
import com.mitrich.fitness_app.models.Exercise
import com.mitrich.fitness_app.models.ExerciseResult
import com.mitrich.fitness_app.utils.ExercisesUtils
import com.mitrich.fitness_app.utils.TrainExercisesUtils
import com.mitrich.fitness_app.views.ExerciseResultItem
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_exercise_results.*
import kotlinx.android.synthetic.main.change_name_dialog.*
import kotlinx.android.synthetic.main.delete_exercise_dialog.*
import kotlinx.android.synthetic.main.goal_dialog.*
import java.util.*
import kotlin.collections.ArrayList

@Suppress("DEPRECATION")
class ExerciseResultsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        val trainExercisesUtils = TrainExercisesUtils()
        val exercisesUtils = ExercisesUtils()

        if (MainActivity.darkTheme) {
            setTheme(R.style.MenuActivityTheme)
        } else {
            setTheme(R.style.MenuActivityTheme_Light)
        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exercise_results)

        if (MainActivity.darkTheme) {
            exercise_toolbar.setBackgroundResource(R.color.dark_bar)
            weight_layout.setBackgroundResource(R.drawable.dark_tab_layout_backgorund)
        } else {
            exercise_toolbar.setBackgroundResource(R.color.white)
            weight_layout.setBackgroundResource(R.drawable.tab_layout_background)
        }

        val filePath = "$filesDir/${MainActivity.workoutListFileName}"
        val trainFilePath = "${MainActivity.filesDir}/${MainActivity.trainDaysListFileName}"
        var showReps = false
        var showingTime = "AllTime"
        val imperialSystem =
            exercisesUtils.getImperialSystem("$filesDir/${MainActivity.userDataFileName}")

        val xAxes = ArrayList<String>()
        val yAxes = ArrayList<Entry>()
        val resultsYAxes = ArrayList<Entry>()

        var exerciseResults: ArrayList<ExerciseResult>
        var goalWeight: Int
        var goalReps: Int

        val exerciseId = intent.getStringExtra("exerciseId")
        val exerciseGroupId = intent.getIntExtra("exerciseGroupId", 0)
        val trainDayId = intent.getIntExtra("trainDayId", 0)

        val currentMonth = SimpleDateFormat("MM", Locale.getDefault()).format(Date()).toInt()
        val currentDay = SimpleDateFormat("dd", Locale.getDefault()).format(Date()).toInt()
        val currentYear = SimpleDateFormat("yy", Locale.getDefault()).format(Date()).toInt()

        val adapter = GroupAdapter<ViewHolder>()
        results_recyclerView.adapter = adapter
        results_recyclerView.addItemDecoration(
            DividerItemDecoration(
                this,
                DividerItemDecoration.HORIZONTAL
            )
        )

        val exercise: Exercise

        if (trainDayId == 0) {
            exercise = Gson().fromJson(
                exercisesUtils.getExercise(exerciseGroupId, exerciseId!!, filePath).toString(),
                Exercise::class.java
            )
        } else {
            exercise = Gson().fromJson(
                trainExercisesUtils.getExercise(
                    trainDayId,
                    exerciseGroupId,
                    exerciseId!!,
                    trainFilePath
                ).toString(),
                Exercise::class.java
            )
        }

        exerciseResults = exercise.exerciseResults
        exercise_title.text = exercise.name
        goalWeight = exercise.goalWeight.toInt()
        goalReps = exercise.goalReps

        ic_left.setOnClickListener {
            results_recyclerView.scrollToPosition(0)
        }

        ic_right.setOnClickListener {
            results_recyclerView.scrollToPosition(exerciseResults.size - 1)
        }

        exerciseResults.forEach {
            xAxes.add(it.date)
            yAxes.add(Entry(it.weight.toFloat(), yAxes.size))
            adapter.add(ExerciseResultItem(it, showReps, imperialSystem))
        }

        if (yAxes.size == 1) {
            yAxes.add(Entry(yAxes[0].`val`, yAxes[0].xIndex++))
            xAxes.add(xAxes[0])
        }

        resultsYAxes.add(Entry(goalWeight.toFloat(), 0))
        resultsYAxes.add(Entry(goalWeight.toFloat(), yAxes.size - 1))

        results_recyclerView.smoothScrollToPosition(adapter.itemCount)

        val lineChart = findViewById<LineChart>(R.id.line_chart)
        val lineDataSets = ArrayList<ILineDataSet>()

        val lineDataSet = LineDataSet(yAxes, "Reps")
        val resultLineDataSet = LineDataSet(resultsYAxes, "Goal")

        if (goalWeight > 0) {
            lineDataSets.add(resultLineDataSet)
        }

        lineDataSet.color = Color.rgb(42, 143, 230)
        lineDataSet.setCircleColor(Color.rgb(42, 143, 230))
        lineDataSets.add(lineDataSet)

        resultLineDataSet.color = Color.RED
        resultLineDataSet.setCircleColor(Color.RED)

        lineDataSet.setDrawCircleHole(false)
        resultLineDataSet.setDrawCircleHole(false)
        lineChart.data = LineData(xAxes, lineDataSets)
        lineChart.data.setDrawValues(false)
        lineDataSet.lineWidth = 3f
        resultLineDataSet.lineWidth = 3f
        lineDataSet.disableDashedLine()
        lineChart.animateX(1000)
        lineChart.animateY(750)

        lineChart.isDoubleTapToZoomEnabled = false
        lineChart.setDrawBorders(true)
        lineChart.legend.isEnabled = false
        lineChart.axisRight.isEnabled = false
        lineChart.xAxis.isEnabled = false
        lineChart.setDescription("")
        lineChart.xAxis.textColor = Color.GRAY
        lineChart.axisLeft.textColor = Color.GRAY
        lineChart.setBorderColor(Color.GRAY)

        target_text.paintFlags = target_text.paintFlags or Paint.UNDERLINE_TEXT_FLAG

        target_text.setOnClickListener {
            val goalDialog = Dialog(this)
            goalDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            goalDialog.setCancelable(true)
            goalDialog.setContentView(R.layout.goal_dialog)
            val titleText = goalDialog.findViewById<TextView>(R.id.title_text)
            val editText = goalDialog.findViewById<EditText>(R.id.goal_edit_text)
            val cancelButton = goalDialog.findViewById<TextView>(R.id.cancel_button)
            val saveButton = goalDialog.findViewById<TextView>(R.id.save_button)
            if (showReps) {
                titleText.text = "Повторения: "
            } else {
                titleText.text = "Вес: "
            }
            editText.filters += InputFilter.LengthFilter(3)

            saveButton.setTextColor(Color.GRAY)
            goalDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            if (MainActivity.darkTheme) {
                goalDialog.goal_layout.setBackgroundResource(R.drawable.dark_dialog_background)
                saveButton.setBackgroundResource(R.drawable.dark_theme_button_background)
                cancelButton.setBackgroundResource(R.drawable.dark_theme_button_background)
            } else {
                goalDialog.goal_layout.setBackgroundResource(R.drawable.light_dialog_background)
                saveButton.setBackgroundResource(R.drawable.light_theme_button_background)
                cancelButton.setBackgroundResource(R.drawable.light_theme_button_background)
            }

            cancelButton.setOnClickListener {
                goalDialog.dismiss()
            }

            editText.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {}

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    if (editText.text.toString().isNotBlank() && editText.text.toString()
                            .isNotEmpty()
                    ) {
                        saveButton.setBackgroundResource(R.drawable.active_save_button_background)
                        saveButton.setTextColor(Color.WHITE)
                    } else {
                        saveButton.setTextColor(Color.GRAY)
                        if (MainActivity.darkTheme) {
                            saveButton.setBackgroundResource(R.drawable.dark_theme_button_background)
                        } else {
                            saveButton.setBackgroundResource(R.drawable.light_theme_button_background)
                        }
                    }
                }
            })

            editText.filters += InputFilter.LengthFilter(3)

            saveButton.setOnClickListener {
                if (editText.text.toString().isNotBlank() && editText.text.toString()
                        .isNotEmpty()) {

                    val newGoalValue = editText.text.toString().toIntOrNull()

                    if (newGoalValue != null && newGoalValue > 0) {
                        if (trainDayId == 0) {
                            exercisesUtils.addExerciseGoal(
                                exerciseGroupId,
                                exerciseId,
                                showReps,
                                newGoalValue,
                                filePath
                            )

                            exercisesUtils.redrawLineChart(
                                xAxes,
                                yAxes,
                                lineChart,
                                newGoalValue
                            )
                        } else {
                            trainExercisesUtils.addExerciseGoal(
                                exerciseGroupId,
                                exerciseId,
                                showReps,
                                newGoalValue,
                                trainDayId,
                                trainFilePath
                            )

                            exercisesUtils.redrawLineChart(
                                xAxes,
                                yAxes,
                                lineChart,
                                newGoalValue
                            )
                        }

                        if (showReps) {
                            goalReps = newGoalValue
                        } else {
                            goalWeight = newGoalValue
                        }

                        goalDialog.dismiss()
                    } else {
                        Toast.makeText(this, "Цель может быть задана только целым числом больше 0", Toast.LENGTH_LONG).show()
                        editText.setText("")
                    }
                }
            }
            goalDialog.show()
        }

        adapter.setOnItemLongClickListener { item, _ ->
            var previousExerciseResultItem: ExerciseResultItem
            val exerciseResultItem = item as ExerciseResultItem
            val position = adapter.getAdapterPosition(item)
            if (adapter.itemCount != 1) {
                val deleteDialog = Dialog(this)
                deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                deleteDialog.setCancelable(true)
                deleteDialog.setContentView(R.layout.delete_exercise_dialog)
                val title = deleteDialog.findViewById<TextView>(R.id.deleting_title)
                val deleteButton = deleteDialog.findViewById<TextView>(R.id.deleting_text)
                val cancelButton =
                    deleteDialog.findViewById<TextView>(R.id.cancel_deleting_text)
                title.text = "Удалить результат?"

                if (MainActivity.darkTheme) {
                    deleteButton.setBackgroundResource(R.drawable.dark_theme_button_background)
                    cancelButton.setBackgroundResource(R.drawable.dark_theme_button_background)
                    deleteDialog.delete_layout.setBackgroundResource(R.drawable.dark_delete_dialog_background)
                } else {
                    deleteButton.setBackgroundResource(R.drawable.light_theme_button_background)
                    cancelButton.setBackgroundResource(R.drawable.light_theme_button_background)
                    deleteDialog.delete_layout.setBackgroundResource(R.drawable.light_delete_dialog_background)
                }

                deleteDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

                deleteButton.setOnClickListener {
                    previousExerciseResultItem = if (adapter.getAdapterPosition(item) != 0) {
                        adapter.getItem(position - 1) as ExerciseResultItem
                    } else {
                        adapter.getItem(position) as ExerciseResultItem
                    }

                    if (trainDayId == 0) {
                        exerciseResults = exercisesUtils.removeExerciseResult(
                            exerciseResultItem.exerciseResult,
                            exerciseGroupId,
                            exerciseId,
                            previousExerciseResultItem.exerciseResult,
                            filePath,
                            position == adapter.itemCount - 1
                        )

                        exercisesUtils.removeExerciseResultFromGraph(
                            adapter,
                            exerciseGroupId,
                            exerciseId,
                            position,
                            lineChart,
                            showReps,
                            xAxes,
                            yAxes,
                            filePath
                        )


                    } else {
                        exerciseResults = trainExercisesUtils.removeExerciseResult(
                            exerciseResultItem.exerciseResult.id.toString(),
                            exerciseGroupId,
                            exerciseId,
                            previousExerciseResultItem.exerciseResult,
                            trainDayId,
                            position == adapter.itemCount - 1,
                            trainFilePath
                        )

                        trainExercisesUtils.removeExerciseResultFromGraph(
                            adapter,
                            exerciseGroupId,
                            exerciseId,
                            position,
                            lineChart,
                            showReps,
                            xAxes,
                            yAxes,
                            trainFilePath,
                            trainDayId
                        )
                    }

                    deleteDialog.dismiss()
                }

                cancelButton.setOnClickListener {
                    deleteDialog.dismiss()
                }

                deleteDialog.show()
            }
            true
        }

        ic_back.setOnClickListener {
            finish()
        }

        ic_edit_exercise_name.setOnClickListener {
            val newEditDialog = Dialog(this)
            newEditDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            newEditDialog.setCancelable(true)
            newEditDialog.setContentView(R.layout.change_name_dialog)
            val addText = newEditDialog.findViewById<TextView>(R.id.title_text)
            val editText = newEditDialog.findViewById<EditText>(R.id.goal_edit_text)
            val cancelButton = newEditDialog.findViewById<TextView>(R.id.cancel_button)
            val saveButton = newEditDialog.findViewById<TextView>(R.id.save_button)
            editText.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS
            addText.text = "Название:"
            editText.setText(exercise.name)

            newEditDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            saveButton.setBackgroundResource(R.drawable.active_save_button_background)
            if (MainActivity.darkTheme) {
                newEditDialog.change_name_layout.setBackgroundResource(R.drawable.dark_dialog_background)
                cancelButton.setBackgroundResource(R.drawable.dark_theme_button_background)
            } else {
                newEditDialog.change_name_layout.setBackgroundResource(R.drawable.light_dialog_background)
                cancelButton.setBackgroundResource(R.drawable.light_theme_button_background)
            }

            editText.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {}

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    if (editText.text.toString().isNotBlank() && editText.text.toString()
                            .isNotEmpty()
                    ) {
                        saveButton.setBackgroundResource(R.drawable.active_save_button_background)
                        saveButton.setTextColor(Color.WHITE)
                    } else {
                        saveButton.setTextColor(Color.GRAY)
                        if (MainActivity.darkTheme) {
                            saveButton.setBackgroundResource(R.drawable.dark_theme_button_background)
                        } else {
                            saveButton.setBackgroundResource(R.drawable.light_theme_button_background)
                        }
                    }
                }
            })

            cancelButton.setOnClickListener {
                newEditDialog.dismiss()
            }

            saveButton.setOnClickListener {
                if (editText.text.toString().isNotBlank() && editText.text.toString()
                        .isNotEmpty()
                ) {
                    if (trainDayId == 0) {
                        exercisesUtils.changeExerciseName(
                            exerciseGroupId,
                            exerciseId,
                            filePath,
                            editText.text.toString()
                        )
                    } else {
                        trainExercisesUtils.changeExerciseName(
                            trainDayId,
                            exerciseGroupId,
                            exerciseId,
                            trainFilePath,
                            editText.text.toString()
                        )
                    }

                    exercise_title.text = editText.text.toString()
                    newEditDialog.dismiss()
                }
            }

            newEditDialog.show()
        }

        ic_add.setOnClickListener {
            val newResultDialog = Dialog(this)
            newResultDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            newResultDialog.setCancelable(true)
            newResultDialog.setContentView(R.layout.goal_dialog)
            val dialogTitle = newResultDialog.findViewById<TextView>(R.id.dialog_title_text)
            val addText = newResultDialog.findViewById<TextView>(R.id.title_text)
            val editText = newResultDialog.findViewById<EditText>(R.id.goal_edit_text)
            val cancelButton = newResultDialog.findViewById<TextView>(R.id.cancel_button)
            val saveButton = newResultDialog.findViewById<TextView>(R.id.save_button)
            editText.hint = if (showReps) "Введите кол-во..." else "Введите вес..."
            if (!showReps) {
                editText.inputType =
                    InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL
                editText.filters += InputFilter.LengthFilter(5)
            } else {
                editText.filters += InputFilter.LengthFilter(3)
            }
            addText.text = if (showReps) "Повторения:" else "Вес:"
            dialogTitle.text = "Добавить результат"

            newResultDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            saveButton.setBackgroundResource(R.drawable.active_save_button_background)
            if (MainActivity.darkTheme) {
                newResultDialog.goal_layout.setBackgroundResource(R.drawable.dark_dialog_background)
                cancelButton.setBackgroundResource(R.drawable.dark_theme_button_background)
            } else {
                newResultDialog.goal_layout.setBackgroundResource(R.drawable.light_dialog_background)
                cancelButton.setBackgroundResource(R.drawable.light_theme_button_background)
            }

            editText.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {}

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    if (editText.text.toString().isNotBlank() && editText.text.toString()
                            .isNotEmpty()
                    ) {
                        saveButton.setBackgroundResource(R.drawable.active_save_button_background)
                        saveButton.setTextColor(Color.WHITE)
                    } else {
                        saveButton.setTextColor(Color.GRAY)
                        if (MainActivity.darkTheme) {
                            saveButton.setBackgroundResource(R.drawable.dark_theme_button_background)
                        } else {
                            saveButton.setBackgroundResource(R.drawable.light_theme_button_background)
                        }
                    }
                }
            })

            editText.setOnFocusChangeListener { _, focused ->
                if (focused) {
                    editText.setText("")
                }
            }

            cancelButton.setOnClickListener {
                newResultDialog.dismiss()
            }

            saveButton.setOnClickListener {
                if (editText.text.toString().isNotBlank() && editText.text.toString()
                        .isNotEmpty()
                ) {
                    val uuid = UUID.randomUUID()

                    val date = SimpleDateFormat(
                        "dd.MM.yy",
                        Locale.getDefault()
                    ).format(Date())

                    val newResultValue = editText.text.toString().toDoubleOrNull()

                    if (newResultValue != null) {
                        if (newResultValue >= 0 && !showReps || newResultValue > 0) {
                            if (trainDayId == 0) {
                                exercisesUtils.addExerciseResult(
                                    exerciseResults,
                                    exerciseGroupId,
                                    exerciseId,
                                    showReps,
                                    newResultValue,
                                    filePath,
                                    date,
                                    uuid
                                )

                                exercisesUtils.addExerciseResultToGraph(
                                    exerciseResults,
                                    date,
                                    exerciseId,
                                    exerciseGroupId,
                                    yAxes,
                                    xAxes,
                                    showReps,
                                    newResultValue,
                                    lineChart,
                                    adapter,
                                    filePath
                                )
                            } else {
                                exerciseResults = trainExercisesUtils.addResult(
                                    exerciseGroupId,
                                    exerciseId,
                                    showReps,
                                    exerciseResults,
                                    trainDayId,
                                    date,
                                    uuid,
                                    newResultValue,
                                    trainFilePath
                                )

                                trainExercisesUtils.addExerciseResultToGraph(
                                    trainDayId,
                                    exerciseResults,
                                    date,
                                    exerciseId,
                                    exerciseGroupId,
                                    yAxes,
                                    xAxes,
                                    showReps,
                                    newResultValue,
                                    lineChart,
                                    adapter,
                                    trainFilePath
                                )
                            }
                            newResultDialog.dismiss()
                        } else {
                            if (showReps) {
                                Toast.makeText(this, "Повторения могут быть только целым числом больше ноля", Toast.LENGTH_LONG).show()
                                editText.setText("")
                            } else {
                                Toast.makeText(this, "Вес может быть только неотрицательным числом", Toast.LENGTH_LONG).show()
                                editText.setText("")
                            }
                        }
                    } else {
                        if (showReps) {
                            Toast.makeText(this, "Повторения могут быть только целым числом больше ноля", Toast.LENGTH_LONG).show()
                            editText.setText("")
                        } else {
                            Toast.makeText(this, "Вес может быть только неотрицательным числом", Toast.LENGTH_LONG).show()
                            editText.setText("")
                        }
                    }
                }
            }

            if (trainDayId == 0) {
                if (showReps) {
                    editText.setText(
                        exercisesUtils.getExerciseReps(
                            exerciseGroupId,
                            exerciseId,
                            filePath
                        ).toString()
                    )
                } else {
                    editText.setText(
                        exercisesUtils.getExerciseWeight(
                            exerciseGroupId,
                            exerciseId,
                            filePath
                        ).toString()
                    )
                }
            } else {
                if (showReps) {
                    editText.setText(
                        trainExercisesUtils.getExercise(
                            trainDayId,
                            exerciseGroupId,
                            exerciseId,
                            trainFilePath
                        ).getInt("reps").toString()
                    )
                } else {
                    editText.setText(
                        trainExercisesUtils.getExercise(
                            trainDayId,
                            exerciseGroupId,
                            exerciseId,
                            trainFilePath
                        ).getInt("weight").toString()
                    )
                }
            }
            newResultDialog.show()
        }

        weight_layout.setOnClickListener {
            if (showReps) {
                showReps = !showReps
                weight_text.setTextColor(resources.getColor(R.color.active))
                reps_text.setTextColor(resources.getColor(android.R.color.darker_gray))
                reps_layout.setBackgroundResource(R.color.transparent)
                if (MainActivity.darkTheme) {
                    weight_layout.setBackgroundResource(R.drawable.dark_tab_layout_backgorund)
                } else {
                    weight_layout.setBackgroundResource(R.drawable.tab_layout_background)
                }
                adapter.clear()
                xAxes.clear()
                yAxes.clear()
                resultsYAxes.clear()

                exerciseResults.forEach {
                    xAxes.add(it.date)
                    yAxes.add(Entry(it.weight.toFloat(), yAxes.size))
                    adapter.add(ExerciseResultItem(it, showReps, imperialSystem))
                }

                resultsYAxes.add(Entry(goalWeight.toFloat(), 0))
                resultsYAxes.add(Entry(goalWeight.toFloat(), yAxes.size - 1))

                if (showReps) {
                    exercisesUtils.redrawLineChart(xAxes, yAxes, lineChart, goalReps)
                } else {
                    exercisesUtils.redrawLineChart(xAxes, yAxes, lineChart, goalWeight)
                }

                if (showingTime != "AllTime") {
                    showingTime = "AllTime"
                    last_year_button.setTextColor(resources.getColor(android.R.color.darker_gray))
                    last_year_button.setBackgroundResource(R.drawable.button_background)
                    all_time_button.setTextColor(resources.getColor(R.color.active))
                    all_time_button.setBackgroundResource(R.drawable.active_button_background)
                    last_month_button.setTextColor(resources.getColor(android.R.color.darker_gray))
                    last_month_button.setBackgroundResource(R.drawable.button_background)
                }
            }
        }

        reps_layout.setOnClickListener {
            if (!showReps) {
                showReps = !showReps
                weight_text.setTextColor(resources.getColor(android.R.color.darker_gray))
                reps_text.setTextColor(resources.getColor(R.color.active))
                weight_layout.setBackgroundResource(R.color.transparent)
                if (MainActivity.darkTheme) {
                    reps_layout.setBackgroundResource(R.drawable.dark_tab_layout_backgorund)
                } else {
                    reps_layout.setBackgroundResource(R.drawable.tab_layout_background)
                }
                adapter.clear()
                xAxes.clear()
                yAxes.clear()
                resultsYAxes.clear()

                exerciseResults.forEach {
                    xAxes.add(it.date)
                    yAxes.add(Entry(it.reps.toFloat(), yAxes.size))
                    adapter.add(ExerciseResultItem(it, showReps, imperialSystem))
                }

                resultsYAxes.add(Entry(goalReps.toFloat(), 0))
                resultsYAxes.add(Entry(goalReps.toFloat(), yAxes.size - 1))

                if (showReps) {
                    exercisesUtils.redrawLineChart(xAxes, yAxes, lineChart, goalReps)
                } else {
                    exercisesUtils.redrawLineChart(xAxes, yAxes, lineChart, goalWeight)
                }

                if (showingTime != "AllTime") {
                    showingTime = "AllTime"
                    last_year_button.setTextColor(resources.getColor(android.R.color.darker_gray))
                    last_year_button.setBackgroundResource(R.drawable.button_background)
                    all_time_button.setTextColor(resources.getColor(R.color.active))
                    all_time_button.setBackgroundResource(R.drawable.active_button_background)
                    last_month_button.setTextColor(resources.getColor(android.R.color.darker_gray))
                    last_month_button.setBackgroundResource(R.drawable.button_background)
                }
            }
        }

        all_time_button.setOnClickListener {
            if (showingTime != "AllTime") {
                showingTime = "AllTime"
                last_year_button.setTextColor(resources.getColor(android.R.color.darker_gray))
                last_year_button.setBackgroundResource(R.drawable.button_background)
                all_time_button.setTextColor(resources.getColor(R.color.active))
                all_time_button.setBackgroundResource(R.drawable.active_button_background)
                last_month_button.setTextColor(resources.getColor(android.R.color.darker_gray))
                last_month_button.setBackgroundResource(R.drawable.button_background)

                adapter.clear()
                xAxes.clear()
                yAxes.clear()
                resultsYAxes.clear()

                exerciseResults.forEach {
                    xAxes.add(it.date)
                    if (showReps) {
                        yAxes.add(Entry(it.reps.toFloat(), yAxes.size))
                        resultsYAxes.add(Entry(goalReps.toFloat(), 0))
                        resultsYAxes.add(Entry(goalReps.toFloat(), yAxes.size - 1))
                    } else {
                        yAxes.add(Entry(it.weight.toFloat(), yAxes.size))
                        resultsYAxes.add(Entry(goalWeight.toFloat(), 0))
                        resultsYAxes.add(Entry(goalWeight.toFloat(), yAxes.size - 1))
                    }
                    adapter.add(ExerciseResultItem(it, showReps, imperialSystem))
                }
                if (showReps) {
                    exercisesUtils.redrawLineChart(xAxes, yAxes, lineChart, goalReps)
                } else {
                    exercisesUtils.redrawLineChart(xAxes, yAxes, lineChart, goalWeight)
                }
            }
        }

        last_year_button.setOnClickListener {
            if (showingTime != "LastYear") {
                showingTime = "LastYear"
                last_year_button.setTextColor(resources.getColor(R.color.active))
                last_year_button.setBackgroundResource(R.drawable.active_button_background)
                all_time_button.setTextColor(resources.getColor(android.R.color.darker_gray))
                all_time_button.setBackgroundResource(R.drawable.button_background)
                last_month_button.setTextColor(resources.getColor(android.R.color.darker_gray))
                last_month_button.setBackgroundResource(R.drawable.button_background)

                adapter.clear()
                xAxes.clear()
                yAxes.clear()
                resultsYAxes.clear()

                exerciseResults.forEach {
                    val month = it.date.substring(3, 5).toInt()
                    val year = it.date.substring(6, 8).toInt()
                    if ((year < currentYear && month >= currentMonth) || (year == currentYear && month <= currentMonth)) {
                        xAxes.add(it.date)
                        if (showReps) {
                            yAxes.add(Entry(it.reps.toFloat(), yAxes.size))
                            resultsYAxes.add(Entry(goalReps.toFloat(), 0))
                            resultsYAxes.add(Entry(goalReps.toFloat(), yAxes.size - 1))
                        } else {
                            yAxes.add(Entry(it.weight.toFloat(), yAxes.size))
                            resultsYAxes.add(Entry(goalWeight.toFloat(), 0))
                            resultsYAxes.add(Entry(goalWeight.toFloat(), yAxes.size - 1))
                        }
                        adapter.add(ExerciseResultItem(it, showReps, imperialSystem))
                    }
                }

                if (xAxes.isEmpty()) {
                    xAxes.add("$currentDay.$currentMonth.$currentYear")
                    xAxes.add("${currentDay + 1}.$currentMonth.$currentYear")
                }

                if (showReps) {
                    exercisesUtils.redrawLineChart(xAxes, yAxes, lineChart, goalReps)
                } else {
                    exercisesUtils.redrawLineChart(xAxes, yAxes, lineChart, goalWeight)
                }
            }
        }

        last_month_button.setOnClickListener {
            if (showingTime != "LastMonth") {
                showingTime = "LastMonth"
                last_month_button.setTextColor(resources.getColor(R.color.active))
                last_month_button.setBackgroundResource(R.drawable.active_button_background)
                all_time_button.setTextColor(resources.getColor(android.R.color.darker_gray))
                all_time_button.setBackgroundResource(R.drawable.button_background)
                last_year_button.setTextColor(resources.getColor(android.R.color.darker_gray))
                last_year_button.setBackgroundResource(R.drawable.button_background)

                adapter.clear()
                xAxes.clear()
                yAxes.clear()
                resultsYAxes.clear()

                exerciseResults.forEach {
                    val month = it.date.substring(3, 5).toInt()
                    val day = it.date.substring(0, 2).toInt()
                    val year = it.date.substring(6, 8).toInt()
                    if (year == currentYear || currentMonth == 1) {
                        if ((month < currentMonth && day >= currentDay) || (month == currentMonth && day <= currentDay)
                            || (currentMonth == 1 && year == currentYear - 1 && month == 12 && day >= currentDay)
                            || (currentMonth == 1 && year == currentYear - 1 && month == 1 && day <= currentDay)
                        ) {
                            xAxes.add(it.date)
                            if (showReps) {
                                yAxes.add(Entry(it.reps.toFloat(), yAxes.size))
                                resultsYAxes.add(Entry(goalReps.toFloat(), 0))
                                resultsYAxes.add(Entry(goalReps.toFloat(), yAxes.size - 1))
                            } else {
                                yAxes.add(Entry(it.weight.toFloat(), yAxes.size))
                                resultsYAxes.add(Entry(goalWeight.toFloat(), 0))
                                resultsYAxes.add(Entry(goalWeight.toFloat(), yAxes.size - 1))
                            }
                            adapter.add(ExerciseResultItem(it, showReps, imperialSystem))
                        }
                    }
                }
                if (xAxes.isEmpty()) {
                    xAxes.add("$currentDay.$currentMonth.$currentYear")
                    xAxes.add("$currentDay.$currentMonth.$currentYear")
                }
                if (showReps) {
                    exercisesUtils.redrawLineChart(xAxes, yAxes, lineChart, goalReps)
                } else {
                    exercisesUtils.redrawLineChart(xAxes, yAxes, lineChart, goalWeight)
                }
            }
        }
    }
}
