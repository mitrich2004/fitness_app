package com.mitrich.fitness_app.views

import android.annotation.SuppressLint
import com.mitrich.fitness_app.R
import com.mitrich.fitness_app.activities.MainActivity
import com.mitrich.fitness_app.models.Exercise
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.exercise_row.view.*

class ExerciseRow(val exercise: Exercise, private val imperialSystem: Boolean) : Item<ViewHolder>() {
    override fun getLayout(): Int {
        return R.layout.exercise_row
    }

    @SuppressLint("SetTextI18n")
    override fun bind(viewHolder: ViewHolder, position: Int) {
        if (MainActivity.darkTheme) {
            if (position % 2 == 1) {
                viewHolder.itemView.exercise_row_layout.setBackgroundResource(R.color.dark_strip)
            } else {
                viewHolder.itemView.exercise_row_layout.setBackgroundResource(R.color.dark)
            }
        } else {
            if (position % 2 == 1) {
                viewHolder.itemView.exercise_row_layout.setBackgroundResource(R.color.white)
            } else {
                viewHolder.itemView.exercise_row_layout.setBackgroundResource(R.color.light_strip)
            }
        }

        viewHolder.itemView.exercise_name.text = exercise.name
        if (imperialSystem) {
            viewHolder.itemView.exercise_weight.text = exercise.weight.toString() + "lb"
        } else {
            viewHolder.itemView.exercise_weight.text = exercise.weight.toString() + "кг"
        }
        viewHolder.itemView.exercise_repetitions.text = exercise.reps.toString()
    }

}