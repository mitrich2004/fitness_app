package com.mitrich.fitness_app.views

import com.mitrich.fitness_app.R
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.goal_weight_row.view.*

class GoalWeightRow(private val value: String) : Item<ViewHolder>() {
    override fun getLayout(): Int {
        return R.layout.goal_weight_row
    }

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.goal_weight_text.text = value
    }
}
