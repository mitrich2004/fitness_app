package com.mitrich.fitness_app.views

import android.graphics.Color
import com.mitrich.fitness_app.R
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.sex_row.view.*

class SexRow(private val selected: Boolean, val sex: String): Item<ViewHolder>() {
    override fun getLayout(): Int {
        return R.layout.sex_row
    }

    override fun bind(viewHolder: ViewHolder, position: Int) {
        if (selected) {
            val color = Color.parseColor("#2A8FE6")
            viewHolder.itemView.sex_text.text = sex
            viewHolder.itemView.sex_text.setTextColor(color)
            viewHolder.itemView.image.setImageResource(R.drawable.ic_chevron_blue)
        } else {
            viewHolder.itemView.sex_text.text = sex
            viewHolder.itemView.image.setImageResource(R.drawable.ic_chevron)
        }
    }
}
