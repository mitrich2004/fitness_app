package com.mitrich.fitness_app.views

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.mitrich.fitness_app.R
import com.mitrich.fitness_app.activities.MainActivity
import com.mitrich.fitness_app.models.TrainDay
import com.mitrich.fitness_app.utils.TrainExercisesUtils
import com.google.gson.Gson
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.muscle_groups_dialog.*
import kotlinx.android.synthetic.main.train_day_row.view.*

class TrainDayRow(
    private val trainDay: TrainDay,
    private val mainActivityContext: Context,
    private val openAll: Boolean,
    private val openAllImage: ImageView
) : Item<ViewHolder>() {

    companion object {
        var muscleGroupsShow: Boolean = true
    }

    override fun getLayout(): Int {
        return R.layout.train_day_row
    }

    override fun bind(viewHolder: ViewHolder, position: Int) {
        val trainExercisesUtils = TrainExercisesUtils()
        val filePath = "${MainActivity.filesDir}/${MainActivity.trainDaysListFileName}"

        viewHolder.itemView.train_day_text.text = trainDay.name

        val groupsAdapter = GroupAdapter<ViewHolder>()
        viewHolder.itemView.groups_recyclerview.adapter = groupsAdapter
        viewHolder.itemView.groups_recyclerview.addItemDecoration(
            DividerItemDecoration(
                mainActivityContext,
                DividerItemDecoration.VERTICAL
            )
        )
        groupsAdapter.clear()

        viewHolder.itemView.groups_recyclerview.removeAllViews()

        if (openAll) {
            groupsAdapter.clear()
            muscleGroupsShow = true
            viewHolder.itemView.groups_recyclerview.removeAllViews()
            val muscleGroupsList = trainDay.muscleGroupsList
            viewHolder.itemView.ic_open_day.setImageResource(R.drawable.ic_less)
            if (muscleGroupsList.size > 0) {
                for (muscleGroup in muscleGroupsList) {
                    if (muscleGroup.isDisplayed) {
                        groupsAdapter.add(
                            ExerciseGroupRow(
                                muscleGroup,
                                mainActivityContext,
                                openAll,
                                openAllImage,
                                trainDay.id
                            )
                        )
                    }
                }
            }
        } else {
            openAllImage.setImageResource(R.drawable.ic_more)
            muscleGroupsShow = false
            viewHolder.itemView.ic_open_day.setImageResource(R.drawable.ic_more)
        }

        viewHolder.itemView.ic_add_group.setOnClickListener {
            val muscleGroupsDialog = Dialog(mainActivityContext)
            muscleGroupsDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            muscleGroupsDialog.setCancelable(true)
            muscleGroupsDialog.setContentView(R.layout.muscle_groups_dialog)
            val dialogTitleText = muscleGroupsDialog.findViewById<TextView>(R.id.dialog_title)
            dialogTitleText.text = trainDay.name
            val recyclerView =
                muscleGroupsDialog.findViewById<RecyclerView>(R.id.muscle_groups_recyclerview)
            val adapter = GroupAdapter<ViewHolder>()
            recyclerView.adapter = adapter
            recyclerView.addItemDecoration(
                DividerItemDecoration(
                    mainActivityContext,
                    DividerItemDecoration.VERTICAL
                )
            )

            val confirmButton = muscleGroupsDialog.findViewById<Button>(R.id.confirm_button)

            val trainDaysList = trainExercisesUtils.getTrainDaysList(filePath)

            muscleGroupsDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            if (MainActivity.darkTheme) {
                muscleGroupsDialog.muscle_groups_layout.setBackgroundResource(R.drawable.dark_dialog_background)
            } else {
                muscleGroupsDialog.muscle_groups_layout.setBackgroundResource(R.drawable.light_dialog_background)
            }

            val trainDay = Gson().fromJson(
                trainExercisesUtils.getTrainDay(
                    filePath,
                    trainDay.id
                ).toString(), TrainDay::class.java
            )

            for (x in 0 until trainDay.muscleGroupsList.size) {
                val exerciseGroup = trainDay.muscleGroupsList[x]
                val isChecked = exerciseGroup.isDisplayed
                adapter.add(
                    TrainDayGroupRow(
                        exerciseGroup,
                        isChecked,
                        trainDay,
                        confirmButton,
                        trainDaysList,
                        muscleGroupsDialog,
                        groupsAdapter,
                        mainActivityContext,
                        openAll,
                        openAllImage,
                        muscleGroupsShow,
                        viewHolder.itemView.ic_open_day
                    )
                )
            }

            muscleGroupsDialog.show()
        }

        viewHolder.itemView.ic_open_day.setOnClickListener {
            muscleGroupsShow = !muscleGroupsShow
            if (muscleGroupsShow) {
                groupsAdapter.clear()
                viewHolder.itemView.groups_recyclerview.removeAllViews()
                viewHolder.itemView.ic_open_day.setImageResource(R.drawable.ic_less)

                val trainDay = Gson().fromJson(
                    trainExercisesUtils.getTrainDay(
                        filePath,
                        trainDay.id
                    ).toString(), TrainDay::class.java
                )

                for (i in 0 until trainDay.muscleGroupsList.size) {
                    val muscleGroup = trainDay.muscleGroupsList[i]
                    if (muscleGroup.isDisplayed) {
                        groupsAdapter.add(
                            ExerciseGroupRow(
                                muscleGroup,
                                mainActivityContext,
                                openAll,
                                openAllImage,
                                trainDay.id
                            )
                        )
                    }
                }

            } else {
                MainActivity.allOpened = false
                openAllImage.setImageResource(R.drawable.ic_more)
                viewHolder.itemView.ic_open_day.setImageResource(R.drawable.ic_more)
                groupsAdapter.clear()
            }
        }

        viewHolder.itemView.day_layout.setOnClickListener {
            muscleGroupsShow = !muscleGroupsShow
            if (muscleGroupsShow) {
                groupsAdapter.clear()
                viewHolder.itemView.groups_recyclerview.removeAllViews()
                viewHolder.itemView.ic_open_day.setImageResource(R.drawable.ic_less)

                val trainDay = Gson().fromJson(
                    trainExercisesUtils.getTrainDay(
                        filePath,
                        trainDay.id
                    ).toString(), TrainDay::class.java
                )

                for (i in 0 until trainDay.muscleGroupsList.size) {
                    val muscleGroup = trainDay.muscleGroupsList[i]
                    if (muscleGroup.isDisplayed) {
                        groupsAdapter.add(
                            ExerciseGroupRow(
                                muscleGroup,
                                mainActivityContext,
                                openAll,
                                openAllImage,
                                trainDay.id
                            )
                        )
                    }
                }

            } else {
                MainActivity.allOpened = false
                openAllImage.setImageResource(R.drawable.ic_more)
                viewHolder.itemView.ic_open_day.setImageResource(R.drawable.ic_more)
                groupsAdapter.clear()
            }
        }
    }
}