package com.mitrich.fitness_app.views

import android.annotation.SuppressLint
import android.view.View
import com.mitrich.fitness_app.R
import com.mitrich.fitness_app.activities.MainActivity
import com.mitrich.fitness_app.models.ExerciseResult
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.exercise_result_item.view.*

class ExerciseResultItem(val exerciseResult: ExerciseResult, private val showReps: Boolean, val imperialSystem: Boolean) : Item<ViewHolder>() {
    override fun getLayout(): Int {
        return R.layout.exercise_result_item
    }

    @SuppressLint("SetTextI18n")
    override fun bind(viewHolder: ViewHolder, position: Int) {
        if (MainActivity.darkTheme) {
            viewHolder.itemView.result_item.setBackgroundResource(R.color.dark_strip)
        } else {
            viewHolder.itemView.result_item.setBackgroundResource(android.R.color.white)
        }

        if (showReps) {
            viewHolder.itemView.result_text.text = exerciseResult.reps.toString()
            viewHolder.itemView.system_text.visibility = View.INVISIBLE
        } else {
            viewHolder.itemView.result_text.text = exerciseResult.weight.toString()
            viewHolder.itemView.system_text.visibility = View.VISIBLE
        }

        if (imperialSystem) {
            viewHolder.itemView.system_text.text = "lb"
        } else {
            viewHolder.itemView.system_text.text = "кг"
        }

        viewHolder.itemView.date_text.text = exerciseResult.date
    }

}