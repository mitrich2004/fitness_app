package com.mitrich.fitness_app.views

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.icu.text.SimpleDateFormat
import android.os.Vibrator
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.Log
import android.view.Window
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import com.google.gson.Gson
import com.mitrich.fitness_app.R
import com.mitrich.fitness_app.activities.ExerciseResultsActivity
import com.mitrich.fitness_app.activities.MainActivity
import com.mitrich.fitness_app.models.Exercise
import com.mitrich.fitness_app.models.ExerciseGroup
import com.mitrich.fitness_app.utils.ExercisesUtils
import com.mitrich.fitness_app.utils.TrainExercisesUtils
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.muscle_group_row.view.*
import org.json.JSONArray
import java.util.*

@Suppress("DEPRECATION")
class ExerciseGroupRow(
    private val exerciseGroup: ExerciseGroup,
    private val mainActivityContext: Context,
    private val openAll: Boolean,
    private val openAllImage: ImageView,
    private val trainDayId: Int?
) : Item<ViewHolder>() {

    private var exercisesShow = false
    private val exercisesUtils: ExercisesUtils = ExercisesUtils()
    private val trainExercisesUtils: TrainExercisesUtils = TrainExercisesUtils()

    override fun getLayout(): Int {
        return R.layout.muscle_group_row
    }

    @SuppressLint("SetTextI18n")
    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.muscle_group_name.text = exerciseGroup.name
        val adapter = GroupAdapter<ViewHolder>()
        adapter.clear()

        val filePath = "${MainActivity.filesDir}/${MainActivity.workoutListFileName}"
        val trainFilePath = "${MainActivity.filesDir}/${MainActivity.trainDaysListFileName}"

        viewHolder.itemView.exercises_recyclerView.removeAllViews()
        viewHolder.itemView.show_exercises_icon.setImageResource(R.drawable.ic_more)

        val imperialSystem =
            exercisesUtils.getImperialSystem("${MainActivity.filesDir}/${MainActivity.userDataFileName}")

        when (exerciseGroup.name) {
            "Руки" -> viewHolder.itemView.muscle_group_icon.setImageResource(R.drawable.biceps)
            "Грудь" -> viewHolder.itemView.muscle_group_icon.setImageResource(R.drawable.muscle)
            "Прочее" -> viewHolder.itemView.muscle_group_icon.setImageResource(R.drawable.dumbbell)
            "Спина" -> viewHolder.itemView.muscle_group_icon.setImageResource(R.drawable.human_back)
            "Ноги" -> viewHolder.itemView.muscle_group_icon.setImageResource(R.drawable.men_legs)
            "Плечи" -> viewHolder.itemView.muscle_group_icon.setImageResource(R.drawable.shoulder)
        }

        viewHolder.itemView.exercises_recyclerView.adapter = adapter

        if (openAll) {
            adapter.clear()
            viewHolder.itemView.exercises_recyclerView.removeAllViews()
            exercisesShow = true

            val exercises = if (trainDayId == null) {
                exercisesUtils.getExercisesList(exerciseGroup.id, filePath)
            } else {
                trainExercisesUtils.getExercises(trainDayId, exerciseGroup.id, trainFilePath)
            }

            viewHolder.itemView.show_exercises_icon.setImageResource(R.drawable.ic_less)
            if (exercises.length() > 0) {
                adapter.add(ExerciseTableRow())
                for (i in 0 until exercises.length()) {
                    val exercise =
                        Gson().fromJson(
                            exercises[i].toString(),
                            Exercise::class.java
                        )
                    adapter.add(ExerciseRow(exercise, imperialSystem))
                }
            }
        } else {
            openAllImage.setImageResource(R.drawable.ic_more)
            exercisesShow = false
            viewHolder.itemView.show_exercises_icon.setImageResource(R.drawable.ic_more)
        }

        adapter.setOnItemClickListener { it, _ ->
            try {
                val exerciseRow = it as ExerciseRow
                val exercise = exerciseRow.exercise
                adapter.clear()
                viewHolder.itemView.exercises_recyclerView.removeAllViews()
                val intent = Intent(mainActivityContext, ExerciseResultsActivity::class.java)
                intent.putExtra("exerciseId", exercise.id.toString())
                intent.putExtra("exerciseGroupId", exercise.groupId)
                intent.putExtra("trainDayId", trainDayId)
                mainActivityContext.startActivity(intent)
                adapter.clear()
                viewHolder.itemView.show_exercises_icon.setImageResource(R.drawable.ic_more)
                exercisesShow = false
            } catch (e: ClassCastException) {
                e.printStackTrace()
            }
        }

        viewHolder.itemView.add_exercise_icon.setOnClickListener {
            val addExerciseDialog = Dialog(mainActivityContext)
            addExerciseDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            addExerciseDialog.setCancelable(true)
            addExerciseDialog.setContentView(R.layout.exercise_dialog)
            val dialogLayout =
                addExerciseDialog.findViewById<ConstraintLayout>(R.id.add_exercise_layout)
            val nameEditText =
                addExerciseDialog.findViewById<EditText>(R.id.add_exercise_name_edittext)
            val repsEditText =
                addExerciseDialog.findViewById<EditText>(R.id.add_exercise_repetitions_edittext)
            val weightEditText =
                addExerciseDialog.findViewById<EditText>(R.id.add_exercise_weight_edittext)
            val cancelButton = addExerciseDialog.findViewById<TextView>(R.id.cancel_button)
            val saveButton = addExerciseDialog.findViewById<TextView>(R.id.save_button)
            val title = addExerciseDialog.findViewById<TextView>(R.id.add_exercise_text)
            val muscleGroupImage =
                addExerciseDialog.findViewById<ImageView>(R.id.muscle_group_image)
            title.text = "Добавить упражнение"

            addExerciseDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            if (MainActivity.darkTheme) {
                dialogLayout.setBackgroundResource(R.drawable.dark_dialog_background)
                saveButton.setBackgroundResource(R.drawable.dark_theme_button_background)
                cancelButton.setBackgroundResource(R.drawable.dark_theme_button_background)
            } else {
                dialogLayout.setBackgroundResource(R.drawable.light_dialog_background)
                saveButton.setBackgroundResource(R.drawable.light_theme_button_background)
                cancelButton.setBackgroundResource(R.drawable.light_theme_button_background)
            }

            when (exerciseGroup.name) {
                "Руки" -> muscleGroupImage.setImageResource(R.drawable.biceps)
                "Грудь" -> muscleGroupImage.setImageResource(R.drawable.muscle)
                "Прочее" -> muscleGroupImage.setImageResource(R.drawable.dumbbell)
                "Спина" -> muscleGroupImage.setImageResource(R.drawable.human_back)
                "Ноги" -> muscleGroupImage.setImageResource(R.drawable.men_legs)
                "Плечи" -> muscleGroupImage.setImageResource(R.drawable.shoulder)
            }

            nameEditText.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {}

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    if (nameEditText.text.toString().isNotBlank() && nameEditText.text.toString()
                            .isNotEmpty() &&
                        repsEditText.text.toString().isNotBlank() && repsEditText.text.toString()
                            .isNotEmpty() &&
                        weightEditText.text.toString()
                            .isNotBlank() && weightEditText.text.toString().isNotEmpty()
                    ) {
                        saveButton.setBackgroundResource(R.drawable.active_save_button_background)
                        saveButton.setTextColor(Color.WHITE)
                    } else {
                        saveButton.setTextColor(Color.GRAY)
                        if (MainActivity.darkTheme) {
                            saveButton.setBackgroundResource(R.drawable.dark_theme_button_background)
                        } else {
                            saveButton.setBackgroundResource(R.drawable.light_theme_button_background)
                        }
                    }
                }
            })

            repsEditText.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {}

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    if (nameEditText.text.toString().isNotBlank() && nameEditText.text.toString()
                            .isNotEmpty() &&
                        repsEditText.text.toString().isNotBlank() && repsEditText.text.toString()
                            .isNotEmpty() &&
                        weightEditText.text.toString()
                            .isNotBlank() && weightEditText.text.toString().isNotEmpty()
                    ) {
                        saveButton.setBackgroundResource(R.drawable.active_save_button_background)
                        saveButton.setTextColor(Color.WHITE)
                    } else {
                        saveButton.setTextColor(Color.GRAY)
                        if (MainActivity.darkTheme) {
                            saveButton.setBackgroundResource(R.drawable.dark_theme_button_background)
                        } else {
                            saveButton.setBackgroundResource(R.drawable.light_theme_button_background)
                        }
                    }
                }
            })

            weightEditText.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {}

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    if (nameEditText.text.toString().isNotBlank() && nameEditText.text.toString()
                            .isNotEmpty() &&
                        repsEditText.text.toString().isNotBlank() && repsEditText.text.toString()
                            .isNotEmpty() &&
                        weightEditText.text.toString()
                            .isNotBlank() && weightEditText.text.toString().isNotEmpty()
                    ) {
                        saveButton.setBackgroundResource(R.drawable.active_save_button_background)
                        saveButton.setTextColor(Color.WHITE)
                    } else {
                        saveButton.setTextColor(Color.GRAY)
                        if (MainActivity.darkTheme) {
                            saveButton.setBackgroundResource(R.drawable.dark_theme_button_background)
                        } else {
                            saveButton.setBackgroundResource(R.drawable.light_theme_button_background)
                        }
                    }
                }
            })

            repsEditText.setOnFocusChangeListener { _, focused ->
                if (focused) {
                    if (repsEditText.text.toString() == "0") {
                        repsEditText.setText("")
                    }
                }
            }

            weightEditText.setOnFocusChangeListener { _, focused ->
                if (focused) {
                    if (weightEditText.text.toString() == "0.0" || weightEditText.text.toString() == "0") {
                        weightEditText.setText("")
                    }
                }
            }

            cancelButton.setOnClickListener {
                addExerciseDialog.dismiss()
            }

            saveButton.setOnClickListener {
                if (nameEditText.text.toString().isNotBlank() && nameEditText.text.toString()
                        .isNotEmpty() &&
                    repsEditText.text.toString().isNotBlank() && repsEditText.text.toString()
                        .isNotEmpty() &&
                    weightEditText.text.toString().isNotBlank() && weightEditText.text.toString()
                        .isNotEmpty()
                ) {

                    val reps = repsEditText.text.toString().trim().toIntOrNull()
                    val weight = weightEditText.text.toString().trim().toDoubleOrNull()

                    if (reps != null && weight != null && reps > 0 && weight >= 0) {
                        val date =
                            SimpleDateFormat(
                                "dd.MM.yy",
                                Locale.getDefault()
                            ).format(
                                Date()
                            )

                        val exercises: JSONArray

                        if (trainDayId == null) {
                            exercises = exercisesUtils.addExercise(
                                nameEditText.text.toString().trim(),
                                reps,
                                weight,
                                exerciseGroup.id,
                                adapter,
                                exercisesShow,
                                filePath,
                                "${MainActivity.filesDir}/${MainActivity.userDataFileName}",
                                date,
                                UUID.randomUUID(),
                                UUID.randomUUID()
                            )
                        } else {
                            exercises = trainExercisesUtils.addExercise(
                                nameEditText.text.toString().trim(),
                                repsEditText.text.toString().trim().toInt(),
                                weightEditText.text.toString().trim().toDouble(),
                                exerciseGroup.id,
                                trainDayId,
                                adapter,
                                exercisesShow,
                                trainFilePath,
                                date,
                                UUID.randomUUID(),
                                UUID.randomUUID(),
                                "${MainActivity.filesDir}/${MainActivity.userDataFileName}"
                            )
                        }

                        if (!exercisesShow) {
                            exercisesShow = true
                            viewHolder.itemView.show_exercises_icon.setImageResource(R.drawable.ic_less)
                            if (exercises.length() > 0) {
                                adapter.add(ExerciseTableRow())
                                for (i in 0 until exercises.length()) {
                                    val exercise =
                                        Gson().fromJson(
                                            exercises[i].toString(),
                                            Exercise::class.java
                                        )
                                    adapter.add(ExerciseRow(exercise, imperialSystem))
                                }
                            }
                        }
                        addExerciseDialog.dismiss()
                    } else {
                        if (weight == null && reps == null)  {
                            weightEditText.setText("")
                            repsEditText.setText("")
                            Toast.makeText(mainActivityContext, "Повторения и вес должны быть представлены числами", Toast.LENGTH_LONG).show()
                        } else if (weight == null || weight < 0) {
                            weightEditText.setText("")
                            Toast.makeText(mainActivityContext, "Вес может быть только неотрицательным числом", Toast.LENGTH_LONG).show()
                        } else if (reps == null || reps <= 0) {
                            repsEditText.setText("")
                            Toast.makeText(mainActivityContext, "Повторения могут быть только целым числом больше ноля", Toast.LENGTH_LONG).show()
                        }
                    }
                }
            }
            addExerciseDialog.show()
        }

        viewHolder.itemView.show_exercises_icon.setOnClickListener {
            exercisesShow = !exercisesShow
            val exercises = if (trainDayId == null) {
                exercisesUtils.getExercisesList(exerciseGroup.id, filePath)
            } else {
                trainExercisesUtils.getExercises(trainDayId, exerciseGroup.id, trainFilePath)
            }
            if (exercisesShow) {
                adapter.clear()
                viewHolder.itemView.exercises_recyclerView.removeAllViews()
                viewHolder.itemView.show_exercises_icon.setImageResource(R.drawable.ic_less)
                if (exercises.length() > 0) {
                    adapter.add(ExerciseTableRow())
                    for (i in 0 until exercises.length()) {
                        val exercise =
                            Gson().fromJson(
                                exercises[i].toString(),
                                Exercise::class.java
                            )
                        adapter.add(ExerciseRow(exercise, imperialSystem))
                    }
                }
            } else {
                MainActivity.allOpened = false
                openAllImage.setImageResource(R.drawable.ic_more)
                viewHolder.itemView.show_exercises_icon.setImageResource(R.drawable.ic_more)
                if (exercises.length() > 0) {
                    adapter.removeGroup(0)
                    for (i in 0 until exercises.length()) {
                        adapter.removeGroup(0)
                    }
                }
                adapter.clear()
                viewHolder.itemView.exercises_recyclerView.removeAllViews()
            }
        }

        viewHolder.itemView.constraintLayout.setOnClickListener {
            exercisesShow = !exercisesShow
            val exercises = if (trainDayId == null) {
                exercisesUtils.getExercisesList(exerciseGroup.id, filePath)
            } else {
                trainExercisesUtils.getExercises(trainDayId, exerciseGroup.id, trainFilePath)
            }
            if (exercisesShow) {
                adapter.clear()
                viewHolder.itemView.exercises_recyclerView.removeAllViews()
                viewHolder.itemView.show_exercises_icon.setImageResource(R.drawable.ic_less)
                if (exercises.length() > 0) {
                    adapter.add(ExerciseTableRow())
                    for (i in 0 until exercises.length()) {
                        val exercise =
                            Gson().fromJson(
                                exercises[i].toString(),
                                Exercise::class.java
                            )
                        adapter.add(ExerciseRow(exercise, imperialSystem))
                    }
                }
            } else {
                MainActivity.allOpened = false
                openAllImage.setImageResource(R.drawable.ic_more)
                viewHolder.itemView.show_exercises_icon.setImageResource(R.drawable.ic_more)
                if (exercises.length() > 0) {
                    adapter.removeGroup(0)
                    for (i in 0 until exercises.length()) {
                        adapter.removeGroup(0)
                    }
                }
                adapter.clear()
                viewHolder.itemView.exercises_recyclerView.removeAllViews()
            }
        }

        viewHolder.itemView.constraintLayout.setOnLongClickListener {
            if (trainDayId != null) {
                val vibratorService =
                    mainActivityContext.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
                vibratorService.vibrate(100)
            }
            false
        }

        adapter.setOnItemLongClickListener { item, view ->
            try {
                val exerciseRow = item as ExerciseRow
                val exercise = exerciseRow.exercise
                val contextMenu = PopupMenu(mainActivityContext, view)
                contextMenu.setOnMenuItemClickListener {
                    when (it.itemId) {
                        R.id.edit_item -> {
                            val editExerciseDialog = Dialog(mainActivityContext)
                            editExerciseDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                            editExerciseDialog.setCancelable(false)
                            editExerciseDialog.setContentView(R.layout.exercise_dialog)
                            val dialogLayout =
                                editExerciseDialog.findViewById<ConstraintLayout>(R.id.add_exercise_layout)
                            val nameEditText =
                                editExerciseDialog.findViewById<EditText>(R.id.add_exercise_name_edittext)
                            val repsEditText =
                                editExerciseDialog.findViewById<EditText>(R.id.add_exercise_repetitions_edittext)
                            val weightEditText =
                                editExerciseDialog.findViewById<EditText>(R.id.add_exercise_weight_edittext)
                            val cancelButton =
                                editExerciseDialog.findViewById<TextView>(R.id.cancel_button)
                            val saveButton =
                                editExerciseDialog.findViewById<TextView>(R.id.save_button)
                            saveButton.setBackgroundResource(R.drawable.active_save_button_background)
                            if (MainActivity.darkTheme) {
                                cancelButton.setBackgroundResource(R.drawable.dark_theme_button_background)
                            } else {
                                cancelButton.setBackgroundResource(R.drawable.light_theme_button_background)
                            }
                            saveButton.setTextColor(Color.WHITE)
                            val title =
                                editExerciseDialog.findViewById<TextView>(R.id.add_exercise_text)
                            val muscleGroupImage =
                                editExerciseDialog.findViewById<ImageView>(R.id.muscle_group_image)
                            title.text = "Изменить упражнение"
                            nameEditText.setText(exercise.name)
                            weightEditText.setText(exercise.weight.toString())
                            repsEditText.setText(exercise.reps.toString())

                            editExerciseDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                            if (MainActivity.darkTheme) {
                                dialogLayout.setBackgroundResource(R.drawable.dark_dialog_background)
                            } else {
                                dialogLayout.setBackgroundResource(R.drawable.light_dialog_background)
                            }

                            when (exerciseGroup.name) {
                                "Руки" -> muscleGroupImage.setImageResource(R.drawable.biceps)
                                "Грудь" -> muscleGroupImage.setImageResource(R.drawable.muscle)
                                "Прочее" -> muscleGroupImage.setImageResource(R.drawable.dumbbell)
                                "Спина" -> muscleGroupImage.setImageResource(R.drawable.human_back)
                                "Ноги" -> muscleGroupImage.setImageResource(R.drawable.men_legs)
                                "Плечи" -> muscleGroupImage.setImageResource(R.drawable.shoulder)
                            }

                            nameEditText.addTextChangedListener(object : TextWatcher {
                                override fun afterTextChanged(p0: Editable?) {}

                                override fun beforeTextChanged(
                                    p0: CharSequence?,
                                    p1: Int,
                                    p2: Int,
                                    p3: Int
                                ) {
                                }

                                override fun onTextChanged(
                                    p0: CharSequence?,
                                    p1: Int,
                                    p2: Int,
                                    p3: Int
                                ) =
                                    if (nameEditText.text.toString()
                                            .isNotBlank() && nameEditText.text.toString()
                                            .isNotEmpty() && repsEditText.text.toString()
                                            .isNotBlank() && repsEditText.text.toString()
                                            .isNotEmpty()
                                        && weightEditText.text.toString()
                                            .isNotBlank() && weightEditText.text.toString()
                                            .isNotEmpty()
                                    ) {
                                        saveButton.setBackgroundResource(R.drawable.active_save_button_background)
                                        saveButton.setTextColor(Color.WHITE)
                                    } else {
                                        saveButton.setTextColor(Color.GRAY)
                                        if (MainActivity.darkTheme) {
                                            saveButton.setBackgroundResource(R.drawable.dark_theme_button_background)
                                        } else {
                                            saveButton.setBackgroundResource(R.drawable.light_theme_button_background)
                                        }
                                    }
                            })

                            repsEditText.addTextChangedListener(object : TextWatcher {
                                override fun afterTextChanged(p0: Editable?) {}

                                override fun beforeTextChanged(
                                    p0: CharSequence?,
                                    p1: Int,
                                    p2: Int,
                                    p3: Int
                                ) {

                                }

                                override fun onTextChanged(
                                    p0: CharSequence?,
                                    p1: Int,
                                    p2: Int,
                                    p3: Int
                                ) {
                                    if (nameEditText.text.toString()
                                            .isNotBlank() && nameEditText.text.toString()
                                            .isNotEmpty() &&
                                        repsEditText.text.toString()
                                            .isNotBlank() && repsEditText.text.toString()
                                            .isNotEmpty() &&
                                        weightEditText.text.toString()
                                            .isNotBlank() && weightEditText.text.toString()
                                            .isNotEmpty()
                                    ) {
                                        saveButton.setBackgroundResource(R.drawable.active_save_button_background)
                                        saveButton.setTextColor(Color.WHITE)
                                    } else {
                                        saveButton.setTextColor(Color.GRAY)
                                        if (MainActivity.darkTheme) {
                                            saveButton.setBackgroundResource(R.drawable.dark_theme_button_background)
                                        } else {
                                            saveButton.setBackgroundResource(R.drawable.light_theme_button_background)
                                        }
                                    }
                                }
                            })

                            weightEditText.addTextChangedListener(object : TextWatcher {
                                override fun afterTextChanged(p0: Editable?) {}

                                override fun beforeTextChanged(
                                    p0: CharSequence?,
                                    p1: Int,
                                    p2: Int,
                                    p3: Int
                                ) {

                                }

                                override fun onTextChanged(
                                    p0: CharSequence?,
                                    p1: Int,
                                    p2: Int,
                                    p3: Int
                                ) {
                                    if (nameEditText.text.toString()
                                            .isNotBlank() && nameEditText.text.toString()
                                            .isNotEmpty() &&
                                        repsEditText.text.toString()
                                            .isNotBlank() && repsEditText.text.toString()
                                            .isNotEmpty() &&
                                        weightEditText.text.toString()
                                            .isNotBlank() && weightEditText.text.toString()
                                            .isNotEmpty()
                                    ) {
                                        saveButton.setBackgroundResource(R.drawable.active_save_button_background)
                                        saveButton.setTextColor(Color.WHITE)
                                    } else {
                                        saveButton.setTextColor(Color.GRAY)
                                        if (MainActivity.darkTheme) {
                                            saveButton.setBackgroundResource(R.drawable.dark_theme_button_background)
                                        } else {
                                            saveButton.setBackgroundResource(R.drawable.light_theme_button_background)
                                        }
                                    }
                                }
                            })

                            cancelButton.setOnClickListener {
                                editExerciseDialog.dismiss()
                            }

                            saveButton.setOnClickListener {
                                if (nameEditText.text.toString()
                                        .isNotBlank() && nameEditText.text.toString()
                                        .isNotEmpty() &&
                                    repsEditText.text.toString()
                                        .isNotBlank() && repsEditText.text.toString()
                                        .isNotEmpty() &&
                                    weightEditText.text.toString()
                                        .isNotBlank() && weightEditText.text.toString().isNotEmpty()
                                ) {
                                    val date = SimpleDateFormat(
                                        "dd.MM.yy",
                                        Locale.getDefault()
                                    ).format(Date())

                                    if (trainDayId == null) {
                                        exercisesUtils.editExercise(
                                            nameEditText.text.toString(),
                                            repsEditText.text.toString(),
                                            weightEditText.text.toString(),
                                            exercise,
                                            adapter,
                                            adapter.getAdapterPosition(exerciseRow),
                                            filePath,
                                            true,
                                            date,
                                            UUID.randomUUID()
                                        )
                                    } else {
                                        trainExercisesUtils.editExercise(
                                            nameEditText.text.toString(),
                                            repsEditText.text.toString(),
                                            weightEditText.text.toString(),
                                            exercise,
                                            adapter,
                                            adapter.getAdapterPosition(exerciseRow),
                                            trainDayId,
                                            trainFilePath,
                                            date,
                                            UUID.randomUUID(),
                                            true,
                                            "${MainActivity.filesDir}/${MainActivity.userDataFileName}"
                                        )
                                    }
                                    editExerciseDialog.dismiss()
                                }
                            }
                            editExerciseDialog.show()
                        }
                        R.id.delete_item -> {
                            val deleteDialog = Dialog(mainActivityContext)
                            deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                            deleteDialog.setCancelable(false)
                            deleteDialog.setContentView(R.layout.delete_exercise_dialog)
                            val dialogLayout =
                                deleteDialog.findViewById<ConstraintLayout>(R.id.delete_layout)
                            val title = deleteDialog.findViewById<TextView>(R.id.deleting_title)
                            val deleteButton =
                                deleteDialog.findViewById<TextView>(R.id.deleting_text)
                            val cancelButton =
                                deleteDialog.findViewById<TextView>(R.id.cancel_deleting_text)
                            title.text = "Удалить ${exercise.name}?"

                            if (MainActivity.darkTheme) {
                                deleteButton.setBackgroundResource(R.drawable.dark_theme_button_background)
                                cancelButton.setBackgroundResource(R.drawable.dark_theme_button_background)
                                dialogLayout.setBackgroundResource(R.drawable.dark_delete_dialog_background)
                            } else {
                                deleteButton.setBackgroundResource(R.drawable.light_theme_button_background)
                                cancelButton.setBackgroundResource(R.drawable.light_theme_button_background)
                                dialogLayout.setBackgroundResource(R.drawable.light_delete_dialog_background)
                            }

                            deleteDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

                            deleteButton.setOnClickListener {
                                deleteDialog.dismiss()
                                adapter.removeGroup(adapter.getAdapterPosition(exerciseRow))
                                if (trainDayId == null) {
                                    exercisesUtils.removeExercise(
                                        exercise.id.toString(),
                                        exercise.groupId,
                                        adapter,
                                        filePath,
                                        true
                                    )
                                } else {
                                    trainExercisesUtils.removeExercise(
                                        exercise.id.toString(),
                                        exercise.groupId,
                                        adapter,
                                        trainDayId,
                                        trainFilePath,
                                        true
                                    )
                                }
                            }
                            cancelButton.setOnClickListener {
                                deleteDialog.dismiss()
                            }
                            deleteDialog.show()
                        }
                    }

                    return@setOnMenuItemClickListener true
                }
                contextMenu.inflate(R.menu.context_menu)
                contextMenu.show()
            } catch (e: ClassCastException) {
                Log.e("ERROR", e.toString())
            }
            return@setOnItemLongClickListener false
        }
    }
}
