package com.mitrich.fitness_app.views

import com.mitrich.fitness_app.R
import com.mitrich.fitness_app.activities.MainActivity
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.names_row.view.*

class ExerciseTableRow: Item<ViewHolder>() {
    override fun getLayout(): Int {
        return R.layout.names_row
    }

    override fun bind(viewHolder: ViewHolder, position: Int) {
        if (MainActivity.darkTheme) {
            viewHolder.itemView.names_layout.setBackgroundResource(R.color.dark)
        } else {
            viewHolder.itemView.names_layout.setBackgroundResource(R.color.light_strip)
        }
    }
}
