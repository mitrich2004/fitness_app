package com.mitrich.fitness_app.views

import com.mitrich.fitness_app.R
import com.mitrich.fitness_app.activities.MainActivity
import com.mitrich.fitness_app.models.TrainDay
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.week_day_item.view.*

class WeekDayItem(
    val trainDay: TrainDay,
    private val screenWidth: Int
) : Item<ViewHolder>() {
    override fun getLayout(): Int {
        return R.layout.week_day_item
    }

    override fun bind(viewHolder: ViewHolder, position: Int) {

        if (screenWidth <= 720) {
            viewHolder.itemView.week_day_item_layout.layoutParams.width = 70
            viewHolder.itemView.week_day_item_layout.layoutParams.height = 70
        }

        viewHolder.itemView.day_name_text.text = trainDay.shortName

        if (trainDay.isDisplayed) {
            viewHolder.itemView.week_day_item_layout.setBackgroundResource(R.color.active)
        } else {
            if (MainActivity.darkTheme) {
                viewHolder.itemView.week_day_item_layout.setBackgroundResource(R.color.dark_strip)
            } else {
                viewHolder.itemView.week_day_item_layout.setBackgroundResource(R.color.light_strip)
            }
        }
    }
}