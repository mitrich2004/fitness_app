package com.mitrich.fitness_app.views

import android.app.Dialog
import android.content.Context
import android.widget.Button
import android.widget.ImageView
import com.mitrich.fitness_app.R
import com.mitrich.fitness_app.activities.MainActivity
import com.mitrich.fitness_app.models.ExerciseGroup
import com.mitrich.fitness_app.models.TrainDay
import com.mitrich.fitness_app.utils.TrainExercisesUtils
import com.google.gson.Gson
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.train_day_muscle_group_row.view.*
import kotlinx.android.synthetic.main.train_day_row.view.*
import org.json.JSONArray
import java.io.File

class TrainDayGroupRow(
    private val group: ExerciseGroup,
    private val isChecked: Boolean,
    private val trainDay: TrainDay,
    private val confirmButton: Button,
    private var trainDaysList: JSONArray,
    private val muscleGroupsDialog: Dialog,
    private val adapter: GroupAdapter<ViewHolder>,
    private val mainActivityContext: Context,
    private val openAll: Boolean,
    private val openAllImage: ImageView,
    private val muscleGroupsShow: Boolean,
    private val openIcon: ImageView
) : Item<ViewHolder>() {

    override fun getLayout(): Int {
        return R.layout.train_day_muscle_group_row
    }

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.muscle_group_name.text = group.name
        viewHolder.itemView.check_box.isChecked = isChecked
        val trainExercisesUtils = TrainExercisesUtils()
        val filePath = "${MainActivity.filesDir}/${MainActivity.trainDaysListFileName}"
        when (group.name) {
            "Руки" -> viewHolder.itemView.muscle_group_icon.setImageResource(R.drawable.biceps)
            "Грудь" -> viewHolder.itemView.muscle_group_icon.setImageResource(R.drawable.muscle)
            "Прочее" -> viewHolder.itemView.muscle_group_icon.setImageResource(R.drawable.dumbbell)
            "Спина" -> viewHolder.itemView.muscle_group_icon.setImageResource(R.drawable.human_back)
            "Ноги" -> viewHolder.itemView.muscle_group_icon.setImageResource(R.drawable.men_legs)
            "Плечи" -> viewHolder.itemView.muscle_group_icon.setImageResource(R.drawable.shoulder)
        }

        confirmButton.setOnClickListener {
            File(filePath).writeText(trainDaysList.toString())

            val trainDay = Gson().fromJson(
                trainExercisesUtils.getTrainDay(
                    filePath,
                    trainDay.id
                ).toString(),
                TrainDay::class.java
            )

            for (i in 0 until trainDay.muscleGroupsList.size) {
                val muscleGroup = trainDay.muscleGroupsList[i]
                if (muscleGroup.isDisplayed) {
                    adapter.add(
                        ExerciseGroupRow(
                            muscleGroup,
                            mainActivityContext,
                            openAll,
                            openAllImage,
                            trainDay.id
                        )
                    )
                }
            }

            if (!muscleGroupsShow) {
                TrainDayRow.muscleGroupsShow = true
                adapter.clear()
                openIcon.setImageResource(R.drawable.ic_less)
                val muscleGroupsList = trainDay.muscleGroupsList
                if (muscleGroupsList.size > 0) {
                    for (muscleGroup in muscleGroupsList) {
                        if (muscleGroup.isDisplayed) {
                            adapter.add(
                                ExerciseGroupRow(
                                    muscleGroup,
                                    mainActivityContext,
                                    openAll,
                                    openAllImage,
                                    trainDay.id
                                )
                            )
                        }
                    }
                }
            } else {
                adapter.clear()

                for (muscleGroup in trainDay.muscleGroupsList) {
                    if (muscleGroup.isDisplayed) {
                        adapter.add(
                            ExerciseGroupRow(
                                muscleGroup,
                                mainActivityContext,
                                openAll,
                                openAllImage,
                                trainDay.id
                            )
                        )
                    }
                }
            }

            muscleGroupsDialog.dismiss()
        }

        viewHolder.itemView.check_box.setOnClickListener {
            trainDaysList = trainExercisesUtils.editTrainDayGroupsList(
                trainDay.id,
                group.id,
                trainDaysList,
                viewHolder.itemView.check_box.isChecked
            )
        }
    }
}