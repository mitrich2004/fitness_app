package com.mitrich.fitness_app.views

import com.mitrich.fitness_app.R
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.profile_row.view.*

class ProfileRow(val title: String, private val value: String) : Item<ViewHolder>() {
    override fun getLayout(): Int {
        return R.layout.profile_row
    }

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.edit_title.text = title
        viewHolder.itemView.edit_value.text = value
     }
}
