package com.mitrich.fitness_app.utils

import com.mitrich.fitness_app.models.User
import com.google.gson.Gson
import org.json.JSONObject
import java.io.File

class ProfileUtils {
    fun getUser(filePath: String): JSONObject {
        var user = JSONObject()
        File(filePath).forEachLine {
            user = JSONObject(it)
        }
        return user
    }

    fun changeName(filePath: String, username: String): User {
        val user = getUser(filePath)
        user.put("username", username)
        File(filePath).writeText(
            user.toString()
        )

        return Gson().fromJson(user.toString(), User::class.java)
    }

    fun changeQuote(filePath: String, quote: String): User {
        val user = getUser(filePath)
        user.put("quote", quote)
        File(filePath).writeText(
            user.toString()
        )

        return Gson().fromJson(user.toString(), User::class.java)
    }

    fun changeSex(filePath: String, sex: String, idealWeight: Int): User {
        val user = getUser(filePath)
        user.put("sex", sex)
        user.put("goalWeight", idealWeight)
        File(filePath).writeText(user.toString())

        return Gson().fromJson(user.toString(), User::class.java)
    }

    fun changeHeight(filePath: String, idealWeight: Int, height: Int): User {
        val user = getUser(filePath)
        user.put("height", height)
        user.put("goalWeight", idealWeight)
        File(filePath).writeText(user.toString())

        return Gson().fromJson(user.toString(), User::class.java)
    }

    fun changeWeight(filePath: String, currentWeight: Int): User {
        val user = getUser(filePath)
        user.put("currentWeight", currentWeight)
        File(filePath).writeText(user.toString())

        return Gson().fromJson(user.toString(),User::class.java)
    }

    fun changeAge(filePath: String, month: Int, year: Int, idealWeight: Int): User {
        val user = getUser(filePath)
        user.put("monthOfBirth", month)
        user.put("yearOfBirth", year)
        user.put("goalWeight", idealWeight)
        File(filePath).writeText(user.toString())

        return Gson().fromJson(user.toString(), User::class.java)
    }

    fun changeTheme(filePath: String, darkTheme: Boolean): User {
        val user = getUser(filePath)
        user.put("darkTheme", darkTheme)
        File(filePath).writeText(user.toString())

        return Gson().fromJson(user.toString(), User::class.java)
    }

    fun changeSystem(filePath: String, imperialSystem: Boolean): User {
        val user = getUser(filePath)
        user.put("imperialSystem", imperialSystem)
        File(filePath).writeText(user.toString())

        return Gson().fromJson(user.toString(), User::class.java)
    }

    fun changeProgram(filePath: String, program: Boolean): User {
        val user = getUser(filePath)
        user.put("trainProgram", program)
        File(filePath).writeText(user.toString())

        return Gson().fromJson(user.toString(), User::class.java)
    }
}
