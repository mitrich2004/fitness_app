package com.mitrich.fitness_app.utils

import android.graphics.Color
import com.mitrich.fitness_app.activities.MainActivity
import com.mitrich.fitness_app.activities.MainActivity.Companion.filesDir
import com.mitrich.fitness_app.models.Exercise
import com.mitrich.fitness_app.models.ExerciseGroup
import com.mitrich.fitness_app.models.ExerciseResult
import com.mitrich.fitness_app.models.User
import com.mitrich.fitness_app.views.ExerciseResultItem
import com.mitrich.fitness_app.views.ExerciseRow
import com.mitrich.fitness_app.views.ExerciseTableRow
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.google.gson.Gson
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import org.json.JSONArray
import org.json.JSONObject
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

class ExercisesUtils {

    fun getWorkoutList(filePath: String): JSONArray {
        var workoutList = JSONArray()
        File(filePath).forEachLine {
            workoutList = JSONArray(it)
        }
        return workoutList
    }

    fun getExerciseGroup(groupId: Int, filePath: String): JSONObject {
        var exerciseGroup: JSONObject? = null
        val workoutList = getWorkoutList(filePath)
        for (i in 0 until workoutList.length()) {
            val group = JSONObject(workoutList[i].toString())
            val id = group.getInt("id")
            if (groupId == id) {
                exerciseGroup = group
            }
        }
        if (exerciseGroup != null) {
            return exerciseGroup
        } else {
            throw java.lang.NullPointerException()
        }
    }

    fun getExerciseGroupIndex(groupId: Int, filePath: String): Int {
        var exerciseGroupIndex: Int? = null
        val workoutList = getWorkoutList(filePath)
        for (i in 0 until workoutList.length()) {
            val group = JSONObject(workoutList[i].toString())
            val id = group.getInt("id")
            if (groupId == id) {
                exerciseGroupIndex = i
            }
        }
        if (exerciseGroupIndex != null) {
            return exerciseGroupIndex
        } else {
            throw NullPointerException()
        }
    }

    fun getExercisesList(groupId: Int, filePath: String): JSONArray {
        if (groupId in 0..6) {
            val exerciseGroup = getExerciseGroup(groupId, filePath)
            return exerciseGroup.getJSONArray("exercises")
        } else {
            throw NullPointerException()
        }
    }

    fun getExercise(exerciseGroupId: Int, exerciseId: String, filePath: String): JSONObject {
        var exercise: JSONObject? = null
        val exercisesList = getExercisesList(exerciseGroupId, filePath)
        for (i in 0 until exercisesList.length()) {
            val jsonExercise = JSONObject(exercisesList[i].toString())
            val id = jsonExercise.getString("id")
            if (id == exerciseId) {
                exercise = jsonExercise
            }
        }
        if (exercise != null) {
            return exercise
        } else {
            throw NullPointerException("There is no exercise with id $exerciseId")
        }
    }

    fun getExerciseIndex(
        exerciseGroupId: Int,
        exerciseId: String,
        filePath: String
    ): Int {
        var index: Int? = null
        val exercisesList = getExercisesList(exerciseGroupId, filePath)
        for (i in 0 until exercisesList.length()) {
            val jsonExercise = JSONObject(exercisesList[i].toString())
            val id = jsonExercise.getString("id")
            if (id == exerciseId) {
                index = i
            }
        }
        if (index != null) {
            return index
        } else {
            throw NullPointerException("There is no exercise with id $exerciseId")
        }
    }

    fun getImperialSystem(filePath: String): Boolean {
        var imperialSystem = false
        File(filePath).forEachLine {
            val user = Gson().fromJson(it, User::class.java)
            imperialSystem = user.imperialSystem
        }
        return imperialSystem
    }

    fun getExerciseResultsList(
        groupId: Int,
        exerciseId: String,
        filePath: String
    ): JSONArray {
        val exercise = getExercise(groupId, exerciseId, filePath)
        val exerciseResults: JSONArray? = exercise.getJSONArray("exerciseResults")
        if (exerciseResults != null) {
            return exerciseResults
        } else {
            throw NullPointerException("There is no exercise with id $exerciseId")
        }
    }

    fun getExerciseResultIndex(
        groupId: Int,
        exerciseId: String,
        exerciseResultId: UUID,
        filePath: String
    ): Int {
        var index: Int? = null
        val exerciseResults = getExerciseResultsList(groupId, exerciseId, filePath)
        for (i in 0 until exerciseResults.length()) {
            val jsonResult = exerciseResults.getJSONObject(i)
            val resultId = jsonResult.getString("id")
            if (resultId == exerciseResultId.toString()) {
                index = i
            }
        }
        if (index != null) {
            return index
        } else {
            throw NullPointerException("There is no exercise result with id $exerciseResultId")
        }
    }

    fun getExerciseReps(groupId: Int, exerciseId: String, filePath: String): Int {
        val exercise: JSONObject = getExercise(groupId, exerciseId, filePath)
        val exerciseReps: Int? = exercise.getInt("reps")
        if (exerciseReps != null) {
            return exerciseReps
        } else {
            throw NullPointerException("There is no exercise with id $exerciseId")
        }
    }

    fun getExerciseWeight(groupId: Int, exerciseId: String, filePath: String): Double {
        val exercise = getExercise(groupId, exerciseId, filePath)
        val exerciseWeight: Double? = exercise.getDouble("weight")
        if (exerciseWeight != null) {
            return exerciseWeight
        } else {
            throw NullPointerException("There is no exercise with id $exerciseId")
        }
    }

    fun addExercise(
        exerciseName: String,
        exerciseReps: Int,
        exerciseWeight: Double,
        exerciseGroupId: Int,
        adapter: GroupAdapter<ViewHolder>,
        exercisesShow: Boolean,
        filePath: String,
        userFilePath: String,
        date: String,
        exerciseId: UUID,
        exerciseResultId: UUID
    ): JSONArray {
        val imperialSystem = getImperialSystem(userFilePath)
        val workoutList = getWorkoutList(filePath)
        val group = getExerciseGroup(exerciseGroupId, filePath)
        val groupIndex = getExerciseGroupIndex(exerciseGroupId, filePath)
        val exercisesList = getExercisesList(exerciseGroupId, filePath)


        exercisesList.put(
            JSONObject(
                Gson().toJson(
                    Exercise(
                        exerciseId,
                        exerciseGroupId,
                        exerciseName,
                        exerciseReps,
                        exerciseWeight,
                        arrayListOf(
                            ExerciseResult(
                                exerciseResultId,
                                date,
                                exerciseWeight,
                                exerciseReps
                            )
                        ),
                        0,
                        0.0
                    )
                ).toString()
            )
        )

        group.put("exercises", exercisesList)
        workoutList.put(groupIndex, group)

        File(filePath).writeText(workoutList.toString())

        if (exercisesShow) {
            addNewExerciseRow(exerciseGroupId, adapter, imperialSystem, filePath)
        }

        return exercisesList
    }

    private fun addNewExerciseRow(
        exerciseGroupId: Int,
        adapter: GroupAdapter<ViewHolder>,
        imperialSystem: Boolean,
        filePath: String
    ) {
        val exercises = getExercisesList(exerciseGroupId, filePath)
        val newExercise = Gson().fromJson(
            exercises[exercises.length() - 1].toString(),
            Exercise::class.java
        )
        if (adapter.itemCount == 0) {
            adapter.add(ExerciseTableRow())
        }
        adapter.add(ExerciseRow(newExercise, imperialSystem))
    }

    fun editExercise(
        exerciseName: String,
        exerciseReps: String,
        exerciseWeight: String,
        exercise: Exercise,
        adapter: GroupAdapter<ViewHolder>,
        position: Int,
        filePath: String,
        redrawAdapter: Boolean,
        date: String,
        uuid: UUID
    ): JSONObject {
        val workoutList = getWorkoutList(filePath)
        val exerciseGroupId = exercise.groupId
        val exerciseId = exercise.id
        val exerciseGroup = getExerciseGroup(exerciseGroupId, filePath)
        val groupIndex = getExerciseGroupIndex(exerciseGroupId, filePath)
        val exercisesList = getExercisesList(exerciseGroupId, filePath)
        val newExercise = getExercise(exerciseGroupId, exerciseId.toString(), filePath)
        val exerciseIndex = getExerciseIndex(exerciseGroupId, exerciseId.toString(), filePath)

        if (exerciseWeight.toDouble() != exercise.weight || exerciseReps.toInt() != exercise.reps) {
            val results = editExerciseResults(newExercise, exerciseWeight, exerciseReps, date, uuid)
            newExercise.put("exerciseResults", results)
        }

        newExercise.put("name", exerciseName)
        newExercise.put("reps", exerciseReps)
        newExercise.put("weight", exerciseWeight)

        exercisesList.put(exerciseIndex, newExercise)
        exerciseGroup.put("exercises", exercisesList)
        workoutList.put(groupIndex, exerciseGroup)

        File(filePath).writeText(
            workoutList.toString()
        )

        if (redrawAdapter) {
            val exerciseRow = Gson().fromJson(newExercise.toString(), Exercise::class.java)
            editExerciseRow(adapter, position, exerciseRow)
        }

        return newExercise
    }

    fun changeExerciseName(
        exerciseGroupId: Int,
        exerciseId: String,
        filePath: String,
        exerciseName: String
    ): JSONObject {
        val workoutList = getWorkoutList(filePath)
        val exercisesList = getExercisesList(exerciseGroupId, filePath)
        val newExercise = getExercise(exerciseGroupId, exerciseId, filePath)
        val exerciseGroup = getExerciseGroup(exerciseGroupId, filePath)
        val groupIndex = getExerciseGroupIndex(exerciseGroupId, filePath)
        val exerciseIndex = getExerciseIndex(exerciseGroupId, exerciseId, filePath)

        newExercise.put("name", exerciseName)
        exercisesList.put(exerciseIndex, newExercise)
        exerciseGroup.put("exercises", exercisesList)
        workoutList.put(groupIndex, exerciseGroup)

        File(filePath).writeText(
            workoutList.toString()
        )

        return newExercise
    }

    private fun editExerciseResults(
        exercise: JSONObject,
        exerciseWeight: String,
        exerciseReps: String,
        date: String,
        uuid: UUID
    ): JSONArray {
        val results = exercise.getJSONArray("exerciseResults")
        results.put(
            JSONObject(
                Gson().toJson(
                    ExerciseResult(
                        uuid,
                        date,
                        exerciseWeight.toDouble(),
                        exerciseReps.toInt()
                    )
                ).toString()
            )
        )
        return results
    }

    private fun editExerciseRow(
        adapter: GroupAdapter<ViewHolder>,
        position: Int,
        exercise: Exercise
    ) {
        val imperialSystem = getImperialSystem("${filesDir}/${MainActivity.userDataFileName}")
        adapter.removeGroup(position)
        adapter.add(position, ExerciseRow(exercise, imperialSystem))
    }

    fun removeExercise(
        exerciseId: String,
        groupId: Int,
        adapter: GroupAdapter<ViewHolder>,
        filePath: String,
        redrawAdapter: Boolean
    ): JSONArray {
        val workoutList = getWorkoutList(filePath)
        val exerciseGroup = getExerciseGroup(groupId, filePath)
        val groupIndex = getExerciseGroupIndex(groupId, filePath)
        val exercisesList = getExercisesList(groupId, filePath)
        val exerciseIndex = getExerciseIndex(groupId, exerciseId, filePath)

        exercisesList.remove(exerciseIndex)

        if (redrawAdapter) {
            if (exercisesList.length() == 0) {
                adapter.removeGroup(0)
            }
        }

        exerciseGroup.put("exercises", exercisesList)
        workoutList.put(groupIndex, exerciseGroup)
        File(filePath).writeText(workoutList.toString())

        return exercisesList
    }

    fun addExerciseResult(
        exerciseResults: ArrayList<ExerciseResult>,
        groupId: Int,
        exerciseId: String,
        showReps: Boolean,
        editTextValue: Double,
        filePath: String,
        date: String,
        uuid: UUID
    ): ArrayList<ExerciseResult> {
        val workoutList = getWorkoutList(filePath)
        val exerciseGroup = getExerciseGroup(groupId, filePath)
        val groupIndex = getExerciseGroupIndex(groupId, filePath)
        val exercisesList = getExercisesList(groupId, filePath)
        val exercise = getExercise(groupId, exerciseId, filePath)
        val exerciseIndex = getExerciseIndex(groupId, exerciseId, filePath)
        val results = exercise.getJSONArray("exerciseResults")

        if (showReps) {
            exercise.put(
                "reps",
                editTextValue.toInt()
            )
        } else {
            exercise.put(
                "weight",
                editTextValue
            )
        }

        results.put(
            JSONObject(
                Gson().toJson(
                    ExerciseResult(
                        uuid,
                        date,
                        exercise.getDouble("weight"),
                        exercise.getInt("reps")
                    )
                ).toString()
            )
        )

        exercise.put("exerciseResults", results)
        exercisesList.put(exerciseIndex, exercise)
        exerciseGroup.put("exercises", exercisesList)
        workoutList.put(groupIndex, exerciseGroup)

        File(filePath).writeText(
            workoutList.toString()
        )

        exerciseResults.add(
            ExerciseResult(
                uuid,
                date,
                exercise.getDouble("weight"),
                exercise.getInt("reps")
            )
        )

        return exerciseResults
    }

    fun addExerciseResultToGraph(
        exerciseResults: ArrayList<ExerciseResult>,
        date: String,
        exerciseId: String,
        exerciseGroupId: Int,
        yAxes: ArrayList<Entry>,
        xAxes: ArrayList<String>,
        showReps: Boolean,
        editTextValue: Double,
        lineChart: LineChart,
        adapter: GroupAdapter<ViewHolder>,
        filePath: String
    ) {
        val exercise = getExercise(exerciseGroupId, exerciseId, filePath)

        xAxes.add(date)
        if (showReps) {
            yAxes.add(
                Entry(
                    editTextValue.toFloat(),
                    yAxes.size
                )
            )
            redrawLineChart(
                xAxes,
                yAxes,
                lineChart,
                exercise.getInt("goalReps")
            )
        } else {
            yAxes.add(
                Entry(
                    exercise.getDouble("weight").toFloat(),
                    yAxes.size
                )
            )
            redrawLineChart(
                xAxes,
                yAxes,
                lineChart,
                exercise.getInt("goalWeight")
            )
        }

        addExerciseResultToAdapter(adapter, exerciseResults, showReps)
    }

    private fun addExerciseResultToAdapter(
        adapter: GroupAdapter<ViewHolder>,
        exerciseResults: ArrayList<ExerciseResult>,
        showReps: Boolean
    ) {
        val imperialSystem = getImperialSystem("${filesDir}/${MainActivity.userDataFileName}")

        adapter.add(
            ExerciseResultItem(
                exerciseResults[exerciseResults.size - 1],
                showReps,
                imperialSystem
            )
        )
    }

    fun removeExerciseResult(
        exerciseResult: ExerciseResult,
        groupId: Int,
        exerciseId: String,
        previousExerciseResult: ExerciseResult,
        filePath: String,
        changeExercise: Boolean
    ): ArrayList<ExerciseResult> {
        val workoutList = getWorkoutList(filePath)
        val exerciseGroup = getExerciseGroup(groupId, filePath)
        val groupIndex = getExerciseGroupIndex(groupId, filePath)
        val exercisesList = getExercisesList(groupId, filePath)
        val exercise = getExercise(groupId, exerciseId, filePath)
        val exerciseIndex = getExerciseIndex(groupId, exerciseId, filePath)
        val exerciseResults = getExerciseResultsList(groupId, exerciseId, filePath)
        val resultIndex = getExerciseResultIndex(groupId, exerciseId, exerciseResult.id, filePath)
        val previousExerciseResultIndex = getExerciseResultIndex(groupId, exerciseId, previousExerciseResult.id, filePath)

        if (changeExercise && previousExerciseResultIndex != resultIndex) {
            exercise.put("weight", previousExerciseResult.weight)
            exercise.put("reps", previousExerciseResult.reps)
        }

        exerciseResults.remove(resultIndex)

        exercise.put("exerciseResults", exerciseResults)
        exercisesList.put(exerciseIndex, exercise)
        exerciseGroup.put("exercises", exercisesList)
        workoutList.put(groupIndex, exerciseGroup)

        File(filePath).writeText(
            workoutList.toString()
        )

        val group = Gson().fromJson(
            exerciseGroup.toString(),
            ExerciseGroup::class.java
        )
        val exercises = group.exercises
        val exerciseClass = exercises[exerciseIndex]

        return exerciseClass.exerciseResults
    }

    fun removeExerciseResultFromGraph(
        adapter: GroupAdapter<ViewHolder>,
        groupId: Int,
        exerciseId: String,
        position: Int,
        lineChart: LineChart,
        showReps: Boolean,
        xAxes: ArrayList<String>,
        yAxes: ArrayList<Entry>,
        filePath: String
    ){
        val exercise = getExercise(groupId, exerciseId, filePath)
        val goalWeight = exercise.getInt("goalWeight")
        val goalReps = exercise.getInt("goalReps")

        xAxes.removeAt(position)
        yAxes.removeAt(position)

        adapter.removeGroup(position)

        if (showReps) {
            redrawLineChart(
                xAxes,
                yAxes,
                lineChart,
                goalReps
            )
        } else {
            redrawLineChart(
                xAxes,
                yAxes,
                lineChart,
                goalWeight
            )
        }
    }

    fun addExerciseGoal(
        groupId: Int,
        exerciseId: String,
        changeReps: Boolean,
        goalValue: Int,
        filePath: String
    ): JSONObject {
        val workoutList = getWorkoutList(filePath)
        val exerciseGroup = getExerciseGroup(groupId, filePath)
        val groupIndex = getExerciseGroupIndex(groupId, filePath)
        val exercisesList = getExercisesList(groupId, filePath)
        val exercise = getExercise(groupId, exerciseId, filePath)
        val exerciseIndex = getExerciseIndex(groupId, exerciseId, filePath)

        if (changeReps) {
            exercise.put("goalReps", goalValue)
        } else {
            exercise.put("goalWeight", goalValue)
        }

        exercisesList.put(exerciseIndex, exercise)
        exerciseGroup.put("exercises", exercisesList)
        workoutList.put(groupIndex, exerciseGroup)

        File(filePath).writeText(
            workoutList.toString()
        )

        return exercise
    }


    fun redrawLineChart(
        xAxes: ArrayList<String>,
        yAxes: ArrayList<Entry>,
        lineChart: LineChart,
        goalValue: Int
    ) {
        val lineDataSets = ArrayList<ILineDataSet>()
        val goalAxes = ArrayList<Entry>()

        if (yAxes.size == 1) {
            yAxes.add(Entry(yAxes[0].`val`, yAxes[0].xIndex++))
            xAxes.add(xAxes[0])
        }

        goalAxes.add(Entry(goalValue.toFloat(), 0))
        if (yAxes.size != 0) {
            goalAxes.add(Entry(goalValue.toFloat(), yAxes.size - 1))
        } else {
            goalAxes.add(Entry(goalValue.toFloat(), 1))
        }
        val lineDataSet = LineDataSet(yAxes, "")
        val goalLineDataSet = LineDataSet(goalAxes, "Goal")

        if (goalValue > 0) {
            lineDataSets.add(goalLineDataSet)
        }

        lineDataSets.add(lineDataSet)

        goalLineDataSet.color = Color.RED
        goalLineDataSet.setCircleColor(Color.RED)

        goalLineDataSet.setDrawCircleHole(false)
        goalLineDataSet.lineWidth = 3f


        lineDataSet.color = Color.rgb(42, 143, 230)
        lineDataSet.setCircleColor(Color.rgb(42, 143, 230))

        lineDataSet.setDrawCircleHole(false)
        lineChart.data = LineData(xAxes, lineDataSets)

        lineChart.data.setDrawValues(false)
        lineDataSet.lineWidth = 3f
        lineDataSet.disableDashedLine()
        lineChart.animateX(1000)
        lineChart.animateY(750)

        lineChart.invalidate()
    }
}
