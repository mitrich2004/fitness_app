package com.mitrich.fitness_app.utils

import com.mitrich.fitness_app.activities.MainActivity
import com.mitrich.fitness_app.models.Exercise
import com.mitrich.fitness_app.models.ExerciseGroup
import com.mitrich.fitness_app.models.ExerciseResult
import com.mitrich.fitness_app.models.TrainDay
import com.mitrich.fitness_app.views.ExerciseResultItem
import com.mitrich.fitness_app.views.ExerciseRow
import com.mitrich.fitness_app.views.ExerciseTableRow
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.data.Entry
import com.google.gson.Gson
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import org.json.JSONArray
import org.json.JSONObject
import java.io.File
import java.util.*

class TrainExercisesUtils {
    private val exercisesUtils: ExercisesUtils = ExercisesUtils()

    fun getTrainDaysList(filePath: String): JSONArray {
        var trainDaysList = JSONArray()
        File(filePath).forEachLine {
            trainDaysList = JSONArray(it)
        }
        return trainDaysList
    }

    fun getTrainDay(filePath: String, trainDayId: Int): JSONObject {
        var trainDay: JSONObject? = null
        val trainDaysList = getTrainDaysList(filePath)
        for (i in 0 until trainDaysList.length()) {
            val jsonTrainDay = JSONObject(trainDaysList[i].toString())
            val id = jsonTrainDay.getInt("id")
            if (trainDayId == id) {
                trainDay = jsonTrainDay
            }
        }
        if (trainDay != null) {
            return trainDay
        } else {
            throw NullPointerException()
        }
    }

    fun getGroupsList(filePath: String, trainDayId: Int): JSONArray {
        val trainDay = getTrainDay(filePath, trainDayId)
        val exerciseGroups: JSONArray? = trainDay.getJSONArray("muscleGroupsList")
        if (exerciseGroups != null) {
            return exerciseGroups
        } else {
            throw NullPointerException()
        }
    }

    fun getGroup(filePath: String, trainDayId: Int, groupId: Int): JSONObject {
        var group: JSONObject? = null
        val exerciseGroupsList = getGroupsList(filePath, trainDayId)
        for (i in 0 until exerciseGroupsList.length()) {
            val exerciseGroup = JSONObject(exerciseGroupsList[i].toString())
            val exerciseGroupId = exerciseGroup.getInt("id")
            if (groupId == exerciseGroupId) {
                group = exerciseGroup
            }
        }
        if (group != null) {
            return group
        } else {
            throw NullPointerException()
        }
    }

    fun getExercises(trainDayId: Int, groupId: Int, filePath: String): JSONArray {
        val exerciseGroup = getGroup(filePath, trainDayId, groupId)
        val exercisesList: JSONArray? = exerciseGroup.getJSONArray("exercises")
        if (exercisesList != null) {
            return exercisesList
        } else {
            throw NullPointerException()
        }
    }

    fun getExercise(
        trainDayId: Int,
        groupId: Int,
        exerciseId: String,
        filePath: String
    ): JSONObject {
        var exercise: JSONObject? = null
        val exercises = getExercises(trainDayId, groupId, filePath)
        for (i in 0 until exercises.length()) {
            val jsonExercise = exercises.getJSONObject(i)
            val id = jsonExercise.getString("id")
            if (exerciseId == id) {
                exercise = jsonExercise
            }
        }
        if (exercise != null) {
            return exercise
        } else {
            throw NullPointerException()
        }
    }

    fun getExerciseIndex(
        trainDayId: Int,
        groupId: Int,
        exerciseId: String,
        filePath: String
    ): Int {
        var exerciseIndex: Int? = null
        val exercises = getExercises(trainDayId, groupId, filePath)
        for (i in 0 until exercises.length()) {
            val jsonExercise = exercises.getJSONObject(i)
            val id = jsonExercise.getString("id")
            if (exerciseId == id) {
                exerciseIndex = i
            }
        }
        if (exerciseIndex != null) {
            return exerciseIndex
        } else {
            throw NullPointerException()
        }
    }

    fun getExerciseResults(
        trainDayId: Int,
        groupId: Int,
        exerciseId: String,
        filePath: String
    ): JSONArray {
        val exercise = getExercise(trainDayId, groupId, exerciseId, filePath)
        val exerciseResults: JSONArray? = exercise.getJSONArray("exerciseResults")
        if (exerciseResults != null) {
            return exerciseResults
        } else {
            throw NullPointerException()
        }
    }

    fun getExerciseResultIndex(
        trainDayId: Int,
        groupId: Int,
        exerciseId: String,
        filePath: String,
        resultId: String
    ): Int {
        var resultIndex: Int? = null
        val exerciseResults = getExerciseResults(trainDayId, groupId, exerciseId, filePath)
        for (i in 0 until exerciseResults.length()) {
            val result = exerciseResults.getJSONObject(i)
            val exerciseResultId = result.getString("id")
            if (exerciseResultId == resultId) {
                resultIndex = i
            }
        }
        if (resultIndex != null) {
            return resultIndex
        } else {
            throw NullPointerException()
        }
    }

    fun changeDayVisibility(
        filePath: String,
        trainDayId: Int,
        showDay: Boolean
    ): TrainDay {
        val trainDaysList = getTrainDaysList(filePath)
        val jsonTrainDay = getTrainDay(filePath, trainDayId)
        jsonTrainDay.put("isDisplayed", showDay)
        trainDaysList.put(trainDayId - 1, jsonTrainDay)
        File(filePath).writeText(
            trainDaysList.toString()
        )
        return Gson().fromJson(jsonTrainDay.toString(), TrainDay::class.java)
    }

    fun addExercise(
        exerciseName: String,
        exerciseReps: Int,
        exerciseWeight: Double,
        exerciseGroupId: Int,
        trainDayId: Int,
        adapter: GroupAdapter<ViewHolder>,
        exercisesShow: Boolean,
        filePath: String,
        date: String,
        exerciseId: UUID,
        exerciseResultId: UUID,
        userDataFilePath: String
    ): JSONArray {
        val imperialSystem = exercisesUtils.getImperialSystem(userDataFilePath)
        val trainDaysList = getTrainDaysList(filePath)
        val trainDay = getTrainDay(filePath, trainDayId)
        val exercisesGroups = getGroupsList(filePath, trainDayId)
        val group = getGroup(filePath, trainDayId, exerciseGroupId)
        val exercises = getExercises(trainDayId, exerciseGroupId, filePath)

        exercises.put(
            JSONObject(
                Gson().toJson(
                    Exercise(
                        exerciseId,
                        exerciseGroupId,
                        exerciseName.trim(),
                        exerciseReps,
                        exerciseWeight,
                        arrayListOf(
                            ExerciseResult(
                                exerciseResultId,
                                date,
                                exerciseWeight,
                                exerciseReps
                            )
                        ),
                        0,
                        0.0
                    )
                ).toString()
            )
        )

        group.put("exercises", exercises)
        exercisesGroups.put(exerciseGroupId - 1, group)
        trainDay.put("muscleGroupsList", exercisesGroups)
        trainDaysList.put(trainDayId - 1, trainDay)

        File(filePath).writeText(
            trainDaysList.toString()
        )

        if (exercisesShow) {
            val exercisesList = getExercises(trainDayId, exerciseGroupId, filePath)
            val newExercise = Gson().fromJson(
                exercisesList[exercises.length() - 1].toString(),
                Exercise::class.java
            )
            if (adapter.itemCount == 0) {
                adapter.add(ExerciseTableRow())
            }
            adapter.add(ExerciseRow(newExercise, imperialSystem))
        }

        return exercises
    }

    fun removeExercise(
        exerciseId: String,
        groupId: Int,
        adapter: GroupAdapter<ViewHolder>,
        trainDayId: Int,
        filePath: String,
        editAdapter: Boolean
    ): JSONArray {
        val trainDaysList = getTrainDaysList(filePath)
        val trainDay = getTrainDay(filePath, trainDayId)
        val exercisesGroups = getGroupsList(filePath, trainDayId)
        val exerciseGroup = getGroup(filePath, trainDayId, groupId)
        val exercises = getExercises(trainDayId, groupId, filePath)
        val exerciseIndex = getExerciseIndex(trainDayId, groupId, exerciseId, filePath)

        exercises.remove(exerciseIndex)

        if (editAdapter) {
            if (exercises.length() == 0) {
                adapter.removeGroup(0)
            }
        }

        exerciseGroup.put("exercises", exercises)
        exercisesGroups.put(groupId - 1, exerciseGroup)
        trainDay.put("muscleGroupsList", exercisesGroups)
        trainDaysList.put(trainDayId - 1, trainDay)

        File(filePath).writeText(
            trainDaysList.toString()
        )

        return exercises
    }

    fun changeExerciseName(
        trainDayId: Int,
        exerciseGroupId: Int,
        exerciseId: String,
        filePath: String,
        exerciseName: String
    ): JSONObject {
        val trainDaysList = getTrainDaysList(filePath)
        val trainDay = getTrainDay(filePath, trainDayId)
        val exercisesGroups = getGroupsList(filePath, trainDayId)
        val exerciseGroup = getGroup(filePath, trainDayId, exerciseGroupId)
        val exercises = getExercises(trainDayId, exerciseGroupId, filePath)
        val exerciseIndex =
            getExerciseIndex(trainDayId, exerciseGroupId, exerciseId, filePath)
        val newExercise =
            getExercise(trainDayId, exerciseGroupId, exerciseId, filePath)

        newExercise.put("name", exerciseName)

        exercises.put(exerciseIndex, newExercise)
        exerciseGroup.put("exercises", exercises)
        exercisesGroups.put(exerciseGroupId - 1, exerciseGroup)
        trainDay.put("muscleGroupsList", exercisesGroups)
        trainDaysList.put(trainDayId - 1, trainDay)

        File(filePath).writeText(
            trainDaysList.toString()
        )

        return newExercise
    }

    fun editExercise(
        exerciseName: String,
        exerciseReps: String,
        exerciseWeight: String,
        exercise: Exercise,
        adapter: GroupAdapter<ViewHolder>,
        position: Int,
        trainDayId: Int,
        filePath: String,
        date: String,
        uuid: UUID,
        editAdapter: Boolean,
        userDataFilePath: String
    ): JSONArray {
        val trainDaysList = getTrainDaysList(filePath)
        val trainDay = getTrainDay(filePath, trainDayId)
        val exercisesGroups = getGroupsList(filePath, trainDayId)
        val exerciseGroup = getGroup(filePath, trainDayId, exercise.groupId)
        val exercises = getExercises(trainDayId, exercise.groupId, filePath)
        val newExercise =
            getExercise(trainDayId, exercise.groupId, exercise.id.toString(), filePath)
        val exerciseIndex =
            getExerciseIndex(trainDayId, exercise.groupId, exercise.id.toString(), filePath)
        val results =
            newExercise.getJSONArray("exerciseResults")

        if (exerciseWeight.toDouble() != exercise.weight || exerciseReps.toInt() != exercise.reps) {
            results.put(
                JSONObject(
                    Gson().toJson(
                        ExerciseResult(
                            uuid,
                            date,
                            exerciseWeight.toDouble(),
                            exerciseReps.toInt()
                        )
                    ).toString()
                )
            )
            newExercise.put("exerciseResults", results)
        }

        newExercise.put("name", exerciseName)
        newExercise.put("reps", exerciseReps)
        newExercise.put("weight", exerciseWeight)
        exercises.put(exerciseIndex, newExercise)
        exerciseGroup.put("exercises", exercises)
        exercisesGroups.put(exercise.groupId - 1, exerciseGroup)
        trainDay.put("muscleGroupsList", exercisesGroups)
        trainDaysList.put(trainDayId - 1, trainDay)

        File(filePath).writeText(
            trainDaysList.toString()
        )

        val imperialSystem =
            exercisesUtils.getImperialSystem(userDataFilePath)

        if (editAdapter) {
            adapter.removeGroup(position)
            adapter.add(
                position,
                ExerciseRow(
                    Gson().fromJson(
                        newExercise.toString(),
                        Exercise::class.java
                    ), imperialSystem
                )
            )
        }

        return exercises
    }

    fun editTrainDayGroupsList(
        trainDayId: Int,
        groupId: Int,
        trainDaysList: JSONArray,
        addGroup: Boolean
    ): JSONArray {
        val trainDay = trainDaysList.getJSONObject(trainDayId - 1)
        val groupsList = trainDay.getJSONArray("muscleGroupsList")
        val exerciseGroup = groupsList.getJSONObject(groupId - 1)

        exerciseGroup.put("isDisplayed", addGroup)
        groupsList.put(groupId - 1, exerciseGroup)
        trainDay.put("muscleGroupsList", groupsList)
        trainDaysList.put(trainDayId - 1, trainDay)

        return trainDaysList
    }


    fun addExerciseGoal(
        groupId: Int,
        id: String,
        changeReps: Boolean,
        goalValue: Int,
        trainDayId: Int,
        filePath: String
    ): JSONObject {
        val trainDaysList = getTrainDaysList(filePath)
        val trainDay = getTrainDay(filePath, trainDayId)
        val exerciseGroups = getGroupsList(filePath, trainDayId)
        val exerciseGroup = getGroup(filePath, trainDayId, groupId)
        val exercisesList = getExercises(trainDayId, groupId, filePath)
        val exercise = getExercise(trainDayId, groupId, id, filePath)
        val exerciseIndex = getExerciseIndex(trainDayId, groupId, id, filePath)

        if (changeReps) {
            exercise.put("goalReps", goalValue)
        } else {
            exercise.put("goalWeight", goalValue)
        }

        exercisesList.put(exerciseIndex, exercise)
        exerciseGroup.put("exercises", exercisesList)
        exerciseGroups.put(groupId - 1, exerciseGroup)
        trainDay.put("muscleGroupsList", exerciseGroups)
        trainDaysList.put(trainDayId - 1, trainDay)

        File(filePath).writeText(
            trainDaysList.toString()
        )

        return exercise
    }

    fun removeExerciseResult(
        resultId: String,
        groupId: Int,
        id: String,
        previousExerciseResult: ExerciseResult,
        trainDayId: Int,
        changeExercise: Boolean,
        filePath: String
    ): ArrayList<ExerciseResult> {
        val trainDaysList = getTrainDaysList(filePath)
        val trainDay = getTrainDay(filePath, trainDayId)
        val exerciseGroups = getGroupsList(filePath, trainDayId)
        val exerciseGroup = getGroup(filePath, trainDayId, groupId)
        val exercises = getExercises(trainDayId, groupId, filePath)
        val exercise = getExercise(trainDayId, groupId, id, filePath)
        val exerciseIndex = getExerciseIndex(trainDayId, groupId, id, filePath)
        val exerciseResults = getExerciseResults(trainDayId, groupId, id, filePath)
        val resultIndex = getExerciseResultIndex(trainDayId, groupId, id, filePath, resultId)

        if (changeExercise) {
            exercise.put("weight", previousExerciseResult.weight)
            exercise.put("reps", previousExerciseResult.reps)
        }

        exerciseResults.remove(resultIndex)
        exercise.put("exerciseResults", exerciseResults)
        exercises.put(exerciseIndex, exercise)
        exerciseGroup.put("exercises", exercises)
        exerciseGroups.put(groupId - 1, exerciseGroup)
        trainDay.put("muscleGroupsList", exerciseGroups)
        trainDaysList.put(trainDayId - 1, trainDay)

        File(filePath).writeText(
            trainDaysList.toString()
        )

        val group = Gson().fromJson(
            exerciseGroup.toString(),
            ExerciseGroup::class.java
        )

        val exercisesList = group.exercises
        val exerciseClass = exercisesList[exerciseIndex]

        return exerciseClass.exerciseResults
    }

    fun removeExerciseResultFromGraph(
        adapter: GroupAdapter<ViewHolder>,
        groupId: Int,
        exerciseId: String,
        position: Int,
        lineChart: LineChart,
        showReps: Boolean,
        xAxes: ArrayList<String>,
        yAxes: ArrayList<Entry>,
        filePath: String,
        trainDayId: Int
    ) {
        val exercise = getExercise(trainDayId, groupId, exerciseId, filePath)

        val goalWeight = exercise.getInt("goalWeight")
        val goalReps = exercise.getInt("goalReps")

        xAxes.removeAt(position)
        yAxes.removeAt(position)

        adapter.removeGroup(position)

        if (showReps) {
            exercisesUtils.redrawLineChart(
                xAxes,
                yAxes,
                lineChart,
                goalReps
            )
        } else {
            exercisesUtils.redrawLineChart(
                xAxes,
                yAxes,
                lineChart,
                goalWeight
            )
        }
    }

    fun addResult(
        groupId: Int,
        exerciseId: String,
        showReps: Boolean,
        exerciseResults: ArrayList<ExerciseResult>,
        trainDayId: Int,
        date: String,
        uuid: UUID,
        editTextValue: Double,
        filePath: String
    ): ArrayList<ExerciseResult> {
        val trainDaysList = getTrainDaysList(filePath)
        val trainDay = getTrainDay(filePath, trainDayId)
        val exerciseGroups = getGroupsList(filePath, trainDayId)
        val exerciseGroup = getGroup(filePath, trainDayId, groupId)
        val exercisesList = getExercises(trainDayId, groupId, filePath)
        val exercise = getExercise(trainDayId, groupId, exerciseId, filePath)
        val exerciseIndex = getExerciseIndex(trainDayId, groupId, exerciseId, filePath)
        val resultsList = getExerciseResults(trainDayId, groupId, exerciseId, filePath)

        if (showReps) {
            exercise.put(
                "reps",
                editTextValue.toInt()
            )
        } else {
            exercise.put(
                "weight",
                editTextValue
            )
        }
        resultsList.put(
            JSONObject(
                Gson().toJson(
                    ExerciseResult(
                        uuid,
                        date,
                        exercise.getDouble("weight"),
                        exercise.getInt("reps")
                    )
                ).toString()
            )
        )
        exercise.put("exerciseResults", resultsList)
        exercisesList.put(exerciseIndex, exercise)
        exerciseGroup.put("exercises", exercisesList)
        exerciseGroups.put(groupId - 1, exerciseGroup)
        trainDay.put("muscleGroupsList", exerciseGroups)
        trainDaysList.put(trainDayId - 1, trainDay)

        File(filePath).writeText(
            trainDaysList.toString()
        )

        exerciseResults.add(
            ExerciseResult(
                uuid,
                date,
                exercise.getDouble("weight"),
                exercise.getInt("reps")
            )
        )

        return exerciseResults
    }

    fun addExerciseResultToGraph(
        trainDayId: Int,
        exerciseResults: ArrayList<ExerciseResult>,
        date: String,
        exerciseId: String,
        exerciseGroupId: Int,
        yAxes: ArrayList<Entry>,
        xAxes: ArrayList<String>,
        showReps: Boolean,
        editTextValue: Double,
        lineChart: LineChart,
        adapter: GroupAdapter<ViewHolder>,
        filePath: String
    ) {
        val exercise = getExercise(trainDayId, exerciseGroupId, exerciseId, filePath)

        xAxes.add(date)
        if (showReps) {
            yAxes.add(
                Entry(
                    editTextValue.toFloat(),
                    yAxes.size
                )
            )
            exercisesUtils.redrawLineChart(
                xAxes,
                yAxes,
                lineChart,
                exercise.getInt("goalReps")
            )
        } else {
            yAxes.add(
                Entry(
                    exercise.getDouble("weight").toFloat(),
                    yAxes.size
                )
            )
            exercisesUtils.redrawLineChart(
                xAxes,
                yAxes,
                lineChart,
                exercise.getInt("goalWeight")
            )
        }

        addExerciseResultToAdapter(adapter, exerciseResults, showReps)
    }

    private fun addExerciseResultToAdapter(
        adapter: GroupAdapter<ViewHolder>,
        exerciseResults: ArrayList<ExerciseResult>,
        showReps: Boolean
    ) {
        val imperialSystem =
            exercisesUtils.getImperialSystem("${MainActivity.filesDir}/${MainActivity.userDataFileName}")

        adapter.add(
            ExerciseResultItem(
                exerciseResults[exerciseResults.size - 1],
                showReps,
                imperialSystem
            )
        )
    }
}