package com.mitrich.fitness_app.models

data class TrainDay(
    val id: Int,
    val isDisplayed: Boolean,
    val muscleGroupsList: ArrayList<ExerciseGroup>,
    val name: String,
    val shortName: String
)