package com.mitrich.fitness_app.models

data class ExerciseGroup(
    val id: Int,
    var isDisplayed: Boolean,
    var name: String,
    var exercises: ArrayList<Exercise>
)
