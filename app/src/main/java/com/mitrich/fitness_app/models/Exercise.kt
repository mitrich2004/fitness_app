package com.mitrich.fitness_app.models

import java.util.*
import kotlin.collections.ArrayList

data class Exercise(
    val id: UUID,
    val groupId: Int,
    var name: String,
    var reps: Int,
    var weight: Double,
    var exerciseResults: ArrayList<ExerciseResult>,
    var goalReps: Int,
    val goalWeight: Double
)
