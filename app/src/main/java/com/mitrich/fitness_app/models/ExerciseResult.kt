package com.mitrich.fitness_app.models

import java.util.*

data class ExerciseResult(
    val id: UUID,
    val date: String,
    val weight: Double,
    val reps: Int
)
