package com.mitrich.fitness_app.models

data class User(
    val username: String,
    val quote: String,
    val currentWeight: Int,
    val goalWeight: Int,
    val monthOfBirth: Int,
    val yearOfBirth: Int,
    val height: Int,
    val sex: String,
    val trainProgram: Boolean,
    val darkTheme: Boolean,
    val imperialSystem: Boolean
)
