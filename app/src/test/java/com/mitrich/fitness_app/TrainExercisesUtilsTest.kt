@file:Suppress("DEPRECATION")

package com.mitrich.fitness_app

import com.mitrich.fitness_app.models.Exercise
import com.mitrich.fitness_app.models.ExerciseResult
import com.mitrich.fitness_app.utils.TrainExercisesUtils
import com.google.gson.Gson
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import junit.framework.Assert.assertEquals
import org.json.JSONArray
import org.json.JSONObject
import org.junit.Before
import org.junit.Test
import java.io.File
import java.io.FileNotFoundException
import java.util.*
import kotlin.collections.ArrayList

class TrainExercisesUtilsTest {
    private lateinit var trainExercisesUtils: TrainExercisesUtils
    private lateinit var testJSONArray: JSONArray
    private lateinit var testJSONObject: JSONObject
    private val filePath =
        "C:\\Users\\User\\Documents\\Sergey\\fitness_app\\app\\src\\test\\resources\\trainTest.json"
    private val userFilePath =
        "C:\\Users\\User\\Documents\\Sergey\\fitness_app\\app\\src\\test\\resources\\userTest.json"

    @Before
    fun setUp() {
        trainExercisesUtils = TrainExercisesUtils()

        File(filePath).writeText(
            JSONArray(
                "[{\"id\":1,\"isDisplayed\":false,\"muscleGroupsList\":[{\"exercises\":[],\"id\":1,\"isDisplayed\":false,\"name\":\"Ноги\"},{\"exercises\":[],\"id\":2,\"isDisplayed\":false,\"name\":\"Грудь\"},{\"exercises\":[],\"id\":3,\"isDisplayed\":false,\"name\":\"Спина\"},{\"exercises\":[],\"id\":4,\"isDisplayed\":false,\"name\":\"Плечи\"},{\"exercises\":[],\"id\":5,\"isDisplayed\":false,\"name\":\"Руки\"},{\"exercises\":[],\"id\":6,\"isDisplayed\":false,\"name\":\"Прочее\"}],\"name\":\"Понедельник\"},{\"id\":2,\"isDisplayed\":true,\"muscleGroupsList\":[{\"exercises\":[],\"id\":1,\"isDisplayed\":false,\"name\":\"Ноги\"},{\"exercises\":[{\"exerciseResults\":[{\"date\":\"12.01.20\",\"id\":\"5b392d44-1452-4704-9daf-de4cf1e6ad80\",\"reps\":20,\"weight\":0}],\"goalReps\":30,\"goalWeight\":0,\"groupId\":2,\"id\":\"994814c0-5cf6-442d-9635-6a1b6e6de483\",\"name\":\"Отжимания\",\"reps\":20,\"weight\":0}],\"id\":2,\"isDisplayed\":true,\"name\":\"Грудь\"},{\"exercises\":[],\"id\":3,\"isDisplayed\":false,\"name\":\"Спина\"},{\"exercises\":[],\"id\":4,\"isDisplayed\":false,\"name\":\"Плечи\"},{\"exercises\":[],\"id\":5,\"isDisplayed\":false,\"name\":\"Руки\"},{\"exercises\":[{\"exerciseResults\":[{\"date\":\"12.01.20\",\"id\":\"d2accd42-8a92-49d1-9943-c32756588d29\",\"reps\":25,\"weight\":0}],\"goalReps\":0,\"goalWeight\":0,\"groupId\":6,\"id\":\"86108023-7ed8-433d-b494-de6f68f03936\",\"name\":\"Верхний пресс\",\"reps\":25,\"weight\":0},{\"exerciseResults\":[{\"date\":\"23.03.20\",\"id\":\"946b00d6-71d9-4621-87d4-777c37830161\",\"reps\":10,\"weight\":0},{\"date\":\"24.03.20\",\"id\":\"f556444f-d953-41e8-8f6f-913c18476dfd\",\"reps\":10,\"weight\":5}],\"goalReps\":30,\"goalWeight\":50,\"groupId\":6,\"id\":\"f44fa95a-6bdc-49ac-8375-cee79b3ce79d\",\"name\":\"Нижний пресс\",\"reps\":10,\"weight\":5}],\"id\":6,\"isDisplayed\":true,\"name\":\"Прочее\"}],\"name\":\"Вторник\"},{\"id\":3,\"isDisplayed\":false,\"muscleGroupsList\":[{\"exercises\":[],\"id\":1,\"isDisplayed\":false,\"name\":\"Ноги\"},{\"exercises\":[],\"id\":2,\"isDisplayed\":false,\"name\":\"Грудь\"},{\"exercises\":[],\"id\":3,\"isDisplayed\":false,\"name\":\"Спина\"},{\"exercises\":[],\"id\":4,\"isDisplayed\":false,\"name\":\"Плечи\"},{\"exercises\":[],\"id\":5,\"isDisplayed\":false,\"name\":\"Руки\"},{\"exercises\":[],\"id\":6,\"isDisplayed\":false,\"name\":\"Прочее\"}],\"name\":\"Среда\"},{\"id\":4,\"isDisplayed\":true,\"muscleGroupsList\":[{\"exercises\":[],\"id\":1,\"isDisplayed\":false,\"name\":\"Ноги\"},{\"exercises\":[{\"exerciseResults\":[{\"date\":\"13.01.20\",\"id\":\"bcef14c8-ce08-461f-a33a-d96fd93e8a66\",\"reps\":5,\"weight\":12}],\"goalReps\":0,\"goalWeight\":0,\"groupId\":2,\"id\":\"46314de8-2fe3-4df1-89db-de28ae30d758\",\"name\":\"Test\",\"reps\":5,\"weight\":12}],\"id\":2,\"isDisplayed\":true,\"name\":\"Грудь\"},{\"exercises\":[{\"exerciseResults\":[{\"date\":\"13.01.20\",\"id\":\"01fa3fed-88fa-4205-93f9-2532d604f526\",\"reps\":12,\"weight\":5}],\"goalReps\":0,\"goalWeight\":0,\"groupId\":3,\"id\":\"862a795f-0c19-4775-bfa0-d417aa8ac23f\",\"name\":\"Test\",\"reps\":12,\"weight\":5}],\"id\":3,\"isDisplayed\":true,\"name\":\"Спина\"},{\"exercises\":[],\"id\":4,\"isDisplayed\":false,\"name\":\"Плечи\"},{\"exercises\":[],\"id\":5,\"isDisplayed\":false,\"name\":\"Руки\"},{\"exercises\":[],\"id\":6,\"isDisplayed\":false,\"name\":\"Прочее\"}],\"name\":\"Четверг\"},{\"id\":5,\"isDisplayed\":false,\"muscleGroupsList\":[{\"exercises\":[],\"id\":1,\"isDisplayed\":false,\"name\":\"Ноги\"},{\"exercises\":[],\"id\":2,\"isDisplayed\":false,\"name\":\"Грудь\"},{\"exercises\":[],\"id\":3,\"isDisplayed\":false,\"name\":\"Спина\"},{\"exercises\":[],\"id\":4,\"isDisplayed\":false,\"name\":\"Плечи\"},{\"exercises\":[],\"id\":5,\"isDisplayed\":false,\"name\":\"Руки\"},{\"exercises\":[],\"id\":6,\"isDisplayed\":false,\"name\":\"Прочее\"}],\"name\":\"Пятница\"}, {\"id\":6,\"isDisplayed\":false,\"muscleGroupsList\":[{\"exercises\":[],\"id\":1,\"isDisplayed\":false,\"name\":\"Ноги\"},{\"exercises\":[],\"id\":2,\"isDisplayed\":false,\"name\":\"Грудь\"},{\"exercises\":[],\"id\":3,\"isDisplayed\":false,\"name\":\"Спина\"},{\"exercises\":[],\"id\":4,\"isDisplayed\":false,\"name\":\"Плечи\"},{\"exercises\":[],\"id\":5,\"isDisplayed\":false,\"name\":\"Руки\"},{\"exercises\":[],\"id\":6,\"isDisplayed\":false,\"name\":\"Прочее\"}],\"name\":\"Суббота\"},{\"id\":7,\"isDisplayed\":false,\"muscleGroupsList\":[{\"exercises\":[],\"id\":1,\"isDisplayed\":false,\"name\":\"Ноги\"},{\"exercises\":[],\"id\":2,\"isDisplayed\":false,\"name\":\"Грудь\"},{\"exercises\":[],\"id\":3,\"isDisplayed\":false,\"name\":\"Спина\"},{\"exercises\":[],\"id\":4,\"isDisplayed\":false,\"name\":\"Плечи\"},{\"exercises\":[],\"id\":5,\"isDisplayed\":false,\"name\":\"Руки\"},{\"exercises\":[],\"id\":6,\"isDisplayed\":false,\"name\":\"Прочее\"}],\"name\":\"Воскресенье\"}]"
            ).toString()
        )

        File(userFilePath).writeText(
            JSONObject("{\"currentWeight\":51.0,\"darkTheme\":true,\"goalWeight\":60.0,\"height\":180,\"imperialSystem\":false,\"monthOfBirth\":4,\"quote\":\"Developer\",\"sex\":\"Мужской\",\"trainProgram\":false,\"username\":\"mitrich2004\",\"yearOfBirth\":2004}").toString()
        )

        File(filePath).forEachLine {
            testJSONArray = JSONArray(it)
        }

        File(userFilePath).forEachLine {
            testJSONObject = JSONObject(it)
        }
    }

    //getTrainDaysListTest
    @Test
    fun getTrainDaysListTest() {
        assertEquals(
            trainExercisesUtils.getTrainDaysList(filePath).toString(),
            testJSONArray.toString()
        )
    }

    @Test(expected = FileNotFoundException::class)
    fun getTrainDaysListTestWithIncorrectFilePath() {
        trainExercisesUtils.getTrainDaysList("test")
    }

    //getTrainDay
    @Test
    fun getTrainDayTest() {
        assertEquals(
            trainExercisesUtils.getTrainDay(filePath, 1).toString(),
            testJSONArray.getJSONObject(0).toString()
        )
    }

    @Test(expected = FileNotFoundException::class)
    fun getTrainDayWithIncorrectFilePath() {
        trainExercisesUtils.getTrainDay("test", 1)
    }

    @Test(expected = NullPointerException::class)
    fun getTrainDayWithIncorrectDayId() {
        trainExercisesUtils.getTrainDay(filePath, 0)
    }

    //getGroupsListTest
    @Test
    fun getGroupsListTest() {
        assertEquals(
            trainExercisesUtils.getGroupsList(filePath, 1).toString(),
            testJSONArray.getJSONObject(0).getJSONArray("muscleGroupsList").toString()
        )
    }

    @Test(expected = FileNotFoundException::class)
    fun getGroupsListWithIncorrectFilePath() {
        trainExercisesUtils.getGroupsList("test", 1)
    }

    @Test(expected = NullPointerException::class)
    fun getGroupsListWithIncorrectDayId() {
        trainExercisesUtils.getGroupsList(filePath, -5)
    }

    //getExerciseGroupTest
    @Test
    fun getExerciseGroupTest() {
        assertEquals(
            trainExercisesUtils.getGroup(filePath, 1, 1).toString(),
            testJSONArray.getJSONObject(0).getJSONArray("muscleGroupsList").getJSONObject(0)
                .toString()
        )
    }

    @Test(expected = FileNotFoundException::class)
    fun getExerciseGroupWithIncorrectFilePath() {
        trainExercisesUtils.getGroup("test", 1, 1)
    }

    @Test(expected = NullPointerException::class)
    fun getExerciseGroupWithIncorrectDayId() {
        trainExercisesUtils.getGroup(filePath, 9, 1)
    }

    @Test(expected = NullPointerException::class)
    fun getExerciseGroupWithIncorrectGroupId() {
        trainExercisesUtils.getGroup(filePath, 1, 9)
    }

    //getExercisesTest
    @Test
    fun getExercisesTest() {
        assertEquals(
            trainExercisesUtils.getExercises(2, 2, filePath).toString(),
            testJSONArray.getJSONObject(1).getJSONArray("muscleGroupsList").getJSONObject(1)
                .getJSONArray("exercises").toString()
        )
    }

    @Test(expected = FileNotFoundException::class)
    fun getExercisesWithIncorrectFilePath() {
        trainExercisesUtils.getExercises(2, 2, "test")
    }

    @Test(expected = NullPointerException::class)
    fun getExercisesWithIncorrectDayId() {
        trainExercisesUtils.getExercises(8, 2, filePath)
    }

    @Test(expected = NullPointerException::class)
    fun getExercisesWithIncorrectGroupId() {
        trainExercisesUtils.getExercises(1, 8, filePath)
    }

    //getExerciseTest
    @Test
    fun getExerciseTest() {
        assertEquals(
            trainExercisesUtils.getExercise(
                2,
                2,
                "994814c0-5cf6-442d-9635-6a1b6e6de483",
                filePath
            ).toString(),
            testJSONArray.getJSONObject(1).getJSONArray("muscleGroupsList").getJSONObject(1)
                .getJSONArray("exercises").getJSONObject(0).toString()
        )
    }

    @Test(expected = FileNotFoundException::class)
    fun getExerciseWithIncorrectFilePath() {
        trainExercisesUtils.getExercise(2, 2, "994814c0-5cf6-442d-9635-6a1b6e6de483", "test")
    }

    @Test(expected = NullPointerException::class)
    fun getExerciseWithIncorrectDayId() {
        trainExercisesUtils.getExercise(8, 2, "994814c0-5cf6-442d-9635-6a1b6e6de483", filePath)
    }

    @Test(expected = NullPointerException::class)
    fun getExerciseWithIncorrectGroupId() {
        trainExercisesUtils.getExercise(2, 0, "994814c0-5cf6-442d-9635-6a1b6e6de483", filePath)
    }

    @Test(expected = NullPointerException::class)
    fun getExerciseWithIncorrectExerciseId() {
        trainExercisesUtils.getExercise(2, 2, "test", filePath)
    }

    //getExerciseIndex
    @Test
    fun getExerciseIndexTest() {
        assertEquals(
            trainExercisesUtils.getExerciseIndex(
                2,
                2,
                "994814c0-5cf6-442d-9635-6a1b6e6de483",
                filePath
            ), 0
        )
    }

    @Test(expected = FileNotFoundException::class)
    fun getExerciseIndexWithIncorrectFilePath() {
        trainExercisesUtils.getExerciseIndex(2, 2, "994814c0-5cf6-442d-9635-6a1b6e6de483", "test")
    }

    @Test(expected = NullPointerException::class)
    fun getExerciseIndexIncorrectDayId() {
        trainExercisesUtils.getExerciseIndex(8, 2, "994814c0-5cf6-442d-9635-6a1b6e6de483", filePath)
    }

    @Test(expected = NullPointerException::class)
    fun getExerciseIndexWithIncorrectGroupId() {
        trainExercisesUtils.getExerciseIndex(2, 8, "994814c0-5cf6-442d-9635-6a1b6e6de483", filePath)
    }

    @Test(expected = NullPointerException::class)
    fun getExerciseIndexWithIncorrectExerciseId() {
        trainExercisesUtils.getExerciseIndex(2, 2, "test", filePath)
    }

    //getExerciseResultsTest
    @Test
    fun getExerciseResultsTest() {
        assertEquals(
            trainExercisesUtils.getExerciseResults(
                2,
                2,
                "994814c0-5cf6-442d-9635-6a1b6e6de483",
                filePath
            ).toString(),
            testJSONArray.getJSONObject(1).getJSONArray("muscleGroupsList").getJSONObject(1)
                .getJSONArray("exercises").getJSONObject(0).getJSONArray("exerciseResults")
                .toString()
        )
    }

    @Test(expected = FileNotFoundException::class)
    fun getExerciseResultsWithIncorrectFilePath() {
        trainExercisesUtils.getExerciseResults(2, 2, "994814c0-5cf6-442d-9635-6a1b6e6de483", "test")
    }

    @Test(expected = NullPointerException::class)
    fun getExerciseResultsWithIncorrectDayId() {
        trainExercisesUtils.getExerciseResults(
            -1,
            2,
            "994814c0-5cf6-442d-9635-6a1b6e6de483",
            filePath
        )
    }

    @Test(expected = NullPointerException::class)
    fun getExerciseResultsWithIncorrectGroupId() {
        trainExercisesUtils.getExerciseResults(
            2,
            9,
            "994814c0-5cf6-442d-9635-6a1b6e6de483",
            filePath
        )
    }

    @Test(expected = NullPointerException::class)
    fun getExerciseResultsWithIncorrect() {
        trainExercisesUtils.getExerciseResults(2, 2, "test", filePath)
    }

    //getExerciseResultIndexTest
    @Test
    fun getExerciseResultIndexTest() {
        assertEquals(
            trainExercisesUtils.getExerciseResultIndex(
                2,
                2,
                "994814c0-5cf6-442d-9635-6a1b6e6de483",
                filePath,
                "5b392d44-1452-4704-9daf-de4cf1e6ad80"
            ), 0
        )
    }

    @Test(expected = FileNotFoundException::class)
    fun getExerciseResultIndexWithIncorrectFilePath() {
        trainExercisesUtils.getExerciseResultIndex(
            2,
            2,
            "994814c0-5cf6-442d-9635-6a1b6e6de483",
            "filepath",
            "5b392d44-1452-4704-9daf-de4cf1e6ad80"
        )
    }

    @Test(expected = NullPointerException::class)
    fun getExerciseResultIndexWithIncorrectDayId() {
        trainExercisesUtils.getExerciseResultIndex(
            0,
            2,
            "994814c0-5cf6-442d-9635-6a1b6e6de483",
            filePath,
            "5b392d44-1452-4704-9daf-de4cf1e6ad80"
        )
    }

    @Test(expected = NullPointerException::class)
    fun getExerciseResultIndexWithIncorrectGroupId() {
        trainExercisesUtils.getExerciseResultIndex(
            2,
            0,
            "994814c0-5cf6-442d-9635-6a1b6e6de483",
            filePath,
            "5b392d44-1452-4704-9daf-de4cf1e6ad80"
        )
    }

    @Test(expected = NullPointerException::class)
    fun getExerciseResultIndexWithIncorrectExerciseId() {
        trainExercisesUtils.getExerciseResultIndex(
            2,
            2,
            "id",
            filePath,
            "5b392d44-1452-4704-9daf-de4cf1e6ad80"
        )
    }

    @Test(expected = NullPointerException::class)
    fun getExerciseResultIndexWithIncorrectResultId() {
        trainExercisesUtils.getExerciseResultIndex(
            2,
            2,
            "994814c0-5cf6-442d-9635-6a1b6e6de483",
            filePath,
            "id"
        )
    }

    //changeDayVisibilityTest
    @Test
    fun changeDayVisibilityTest() {
        val trainDay = trainExercisesUtils.changeDayVisibility(filePath, 1, true)
        assertEquals(trainDay.isDisplayed, true)
        File(filePath).forEachLine {
            testJSONArray = JSONArray(it)
        }
        var trainDayVisibility = testJSONArray.getJSONObject(0).getBoolean("isDisplayed")
        assertEquals(trainDayVisibility, true)

        trainExercisesUtils.changeDayVisibility(filePath, 1, false)
        File(filePath).forEachLine {
            testJSONArray = JSONArray(it)
        }
        trainDayVisibility = testJSONArray.getJSONObject(0).getBoolean("isDisplayed")
        assertEquals(trainDayVisibility, false)
    }

    @Test(expected = FileNotFoundException::class)
    fun changeDayVisibilityWithIncorrectFilePath() {
        trainExercisesUtils.changeDayVisibility("filePath", 1, true)
    }

    @Test(expected = NullPointerException::class)
    fun changeDayVisibilityWithIncorrectTrainDayId() {
        trainExercisesUtils.changeDayVisibility(filePath, 0, true)
    }

    //addExerciseTest
    @Test
    fun addExerciseTest() {
        val exercisesList = trainExercisesUtils.addExercise(
            "test",
            123,
            123.0,
            1,
            1,
            GroupAdapter<ViewHolder>(),
            false,
            filePath,
            "25.03.20",
            UUID.fromString("994814c0-5cf6-442d-9635-6a1b6e6de483"),
            UUID.fromString("194814c0-5cf6-442d-9635-6a1b6e6de483"),
            userFilePath
        )

        File(filePath).forEachLine {
            testJSONArray = JSONArray(it)
        }

        val jsonExerciseList =
            testJSONArray.getJSONObject(0).getJSONArray("muscleGroupsList").getJSONObject(0)
                .getJSONArray("exercises")

        val expectedExercisesList =
            JSONArray("[{\"goalReps\":0,\"reps\":123,\"exerciseResults\":[{\"date\":\"25.03.20\",\"reps\":123,\"weight\":123,\"id\":\"194814c0-5cf6-442d-9635-6a1b6e6de483\"}],\"groupId\":1,\"name\":\"test\",\"weight\":123,\"goalWeight\":0,\"id\":\"994814c0-5cf6-442d-9635-6a1b6e6de483\"}]")

        assertEquals(exercisesList.toString(), jsonExerciseList.toString())
        assertEquals(exercisesList.toString(), expectedExercisesList.toString())
    }

    @Test(expected = FileNotFoundException::class)
    fun addExerciseWithIncorrectFilePath() {
        trainExercisesUtils.addExercise(
            "test",
            123,
            123.0,
            1,
            1,
            GroupAdapter<ViewHolder>(),
            false,
            "filePath",
            "25.03.20",
            UUID.fromString("994814c0-5cf6-442d-9635-6a1b6e6de483"),
            UUID.fromString("194814c0-5cf6-442d-9635-6a1b6e6de483"),
            userFilePath
        )
    }

    @Test(expected = FileNotFoundException::class)
    fun addExerciseWithIncorrectUserDataFilePath() {
        trainExercisesUtils.addExercise(
            "test",
            123,
            123.0,
            1,
            1,
            GroupAdapter<ViewHolder>(),
            false,
            filePath,
            "25.03.20",
            UUID.fromString("994814c0-5cf6-442d-9635-6a1b6e6de483"),
            UUID.fromString("194814c0-5cf6-442d-9635-6a1b6e6de483"),
            "filePath"
        )
    }

    @Test(expected = NullPointerException::class)
    fun addExerciseWithIncorrectDayId() {
        trainExercisesUtils.addExercise(
            "test",
            123,
            123.0,
            1,
            89,
            GroupAdapter<ViewHolder>(),
            false,
            filePath,
            "25.03.20",
            UUID.fromString("994814c0-5cf6-442d-9635-6a1b6e6de483"),
            UUID.fromString("194814c0-5cf6-442d-9635-6a1b6e6de483"),
            userFilePath
        )
    }

    @Test(expected = NullPointerException::class)
    fun addExerciseWithIncorrectGroupId() {
        trainExercisesUtils.addExercise(
            "test",
            123,
            123.0,
            98,
            1,
            GroupAdapter<ViewHolder>(),
            false,
            filePath,
            "25.03.20",
            UUID.fromString("994814c0-5cf6-442d-9635-6a1b6e6de483"),
            UUID.fromString("194814c0-5cf6-442d-9635-6a1b6e6de483"),
            userFilePath
        )
    }

    //removeExerciseTest
    @Test
    fun removeExerciseTest() {
        val exercisesList = trainExercisesUtils.removeExercise(
            "994814c0-5cf6-442d-9635-6a1b6e6de483",
            2,
            GroupAdapter<ViewHolder>(),
            2,
            filePath,
            false
        )

        File(filePath).forEachLine {
            testJSONArray = JSONArray(it)
        }

        val jsonExercisesList =
            testJSONArray.getJSONObject(0).getJSONArray("muscleGroupsList").getJSONObject(0)
                .getJSONArray("exercises")
        val expectedExercisesList = JSONArray("[]")

        assertEquals(exercisesList.toString(), jsonExercisesList.toString())
        assertEquals(exercisesList.toString(), expectedExercisesList.toString())
    }

    @Test(expected = FileNotFoundException::class)
    fun removeExerciseWithIncorrectFilePath() {
        trainExercisesUtils.removeExercise(
            "994814c0-5cf6-442d-9635-6a1b6e6de483",
            2,
            GroupAdapter<ViewHolder>(),
            2,
            "filePath",
            false
        )
    }

    @Test(expected = NullPointerException::class)
    fun removeExerciseWithIncorrectDayId() {
        trainExercisesUtils.removeExercise(
            "994814c0-5cf6-442d-9635-6a1b6e6de483",
            2,
            GroupAdapter<ViewHolder>(),
            0,
            filePath,
            false
        )
    }

    @Test(expected = NullPointerException::class)
    fun removeExerciseWithIncorrectGroupId() {
        trainExercisesUtils.removeExercise(
            "994814c0-5cf6-442d-9635-6a1b6e6de483",
            0,
            GroupAdapter<ViewHolder>(),
            2,
            filePath,
            false
        )
    }

    @Test(expected = NullPointerException::class)
    fun removeExerciseWithIncorrectExerciseId() {
        trainExercisesUtils.removeExercise(
            "994814c0-5cf6-443d-9635-6a1b6e6de483",
            2,
            GroupAdapter<ViewHolder>(),
            2,
            filePath,
            false
        )
    }

    //editExerciseTest
    @Test
    fun editExerciseTest() {
        val exercisesList = trainExercisesUtils.editExercise(
            "test",
            "123",
            "123.0",
            Gson().fromJson(
                trainExercisesUtils.getExercise(
                    2,
                    2,
                    "994814c0-5cf6-442d-9635-6a1b6e6de483",
                    filePath
                ).toString(), Exercise::class.java
            ),
            GroupAdapter<ViewHolder>(),
            0,
            2,
            filePath,
            "25.03.20",
            UUID.fromString("294814c0-5cf6-442d-9635-6a1b6e6de483"),
            false,
            userFilePath
        )

        File(filePath).forEachLine {
            testJSONArray = JSONArray(it)
        }

        assertEquals(
            exercisesList.toString(),
            testJSONArray.getJSONObject(1).getJSONArray("muscleGroupsList").getJSONObject(1)
                .getJSONArray("exercises").toString()
        )
        assertEquals(
            exercisesList.toString(),
            "[{\"goalReps\":30,\"exerciseResults\":[{\"date\":\"12.01.20\",\"reps\":20,\"weight\":0,\"id\":\"5b392d44-1452-4704-9daf-de4cf1e6ad80\"},{\"date\":\"25.03.20\",\"reps\":123,\"weight\":123,\"id\":\"294814c0-5cf6-442d-9635-6a1b6e6de483\"}],\"reps\":\"123\",\"groupId\":2,\"name\":\"test\",\"weight\":\"123.0\",\"goalWeight\":0,\"id\":\"994814c0-5cf6-442d-9635-6a1b6e6de483\"}]"
        )
    }

    @Test(expected = FileNotFoundException::class)
    fun editExerciseWithIncorrectFilePath() {
        trainExercisesUtils.editExercise(
            "test",
            "123",
            "123.0",
            Gson().fromJson(
                trainExercisesUtils.getExercise(
                    2,
                    2,
                    "994814c0-5cf6-442d-9635-6a1b6e6de483",
                    filePath
                ).toString(), Exercise::class.java
            ),
            GroupAdapter<ViewHolder>(),
            0,
            2,
            "filePath",
            "25.03.20",
            UUID.fromString("294814c0-5cf6-442d-9635-6a1b6e6de483"),
            false,
            userFilePath
        )
    }

    @Test(expected = FileNotFoundException::class)
    fun editExerciseWithIncorrectUserFilePath() {
        trainExercisesUtils.editExercise(
            "test",
            "123",
            "123.0",
            Gson().fromJson(
                trainExercisesUtils.getExercise(
                    2,
                    2,
                    "994814c0-5cf6-442d-9635-6a1b6e6de483",
                    filePath
                ).toString(), Exercise::class.java
            ),
            GroupAdapter<ViewHolder>(),
            0,
            2,
            filePath,
            "25.03.20",
            UUID.fromString("294814c0-5cf6-442d-9635-6a1b6e6de483"),
            false,
            "filePath"
        )
    }

    @Test(expected = NullPointerException::class)
    fun editExerciseWithIncorrectDayId() {
        trainExercisesUtils.editExercise(
            "test",
            "123",
            "123.0",
            Gson().fromJson(
                trainExercisesUtils.getExercise(
                    2,
                    2,
                    "994814c0-5cf6-442d-9635-6a1b6e6de483",
                    filePath
                ).toString(), Exercise::class.java
            ),
            GroupAdapter<ViewHolder>(),
            0,
            22,
            filePath,
            "25.03.20",
            UUID.fromString("294814c0-5cf6-442d-9635-6a1b6e6de483"),
            false,
            userFilePath
        )
    }

    //editTrainDaysGroupsListTest
    @Test
    fun editTrainDayGroupsListTest() {
        trainExercisesUtils.editTrainDayGroupsList(1, 1, testJSONArray, true)
        assertEquals(testJSONArray.getJSONObject(0).getJSONArray("muscleGroupsList").getJSONObject(0).getBoolean("isDisplayed"), true)
        trainExercisesUtils.editTrainDayGroupsList(1, 1, testJSONArray, false)
        assertEquals(testJSONArray.getJSONObject(0).getJSONArray("muscleGroupsList").getJSONObject(0).getBoolean("isDisplayed"), false)
    }

    @Test(expected = org.json.JSONException::class)
    fun editTrainDayGroupsListWithIncorrectDayId() {
        trainExercisesUtils.editTrainDayGroupsList(98, 1, testJSONArray, true)
    }

    @Test(expected = org.json.JSONException::class)
    fun editTrainDayGroupsListWithIncorrectGroupId() {
        trainExercisesUtils.editTrainDayGroupsList(1, 98, testJSONArray, true)
    }

    //addExerciseGoalTest
    @Test
    fun addExerciseGoalTest() {
        var exercise = trainExercisesUtils.addExerciseGoal(
            2,
            "994814c0-5cf6-442d-9635-6a1b6e6de483",
            true,
            123,
            2,
            filePath
        )

        File(filePath).forEachLine {
            testJSONArray = JSONArray(it)
        }

        assertEquals(
            exercise.toString(),
            testJSONArray.getJSONObject(1).getJSONArray("muscleGroupsList").getJSONObject(1)
                .getJSONArray("exercises").getJSONObject(0).toString()
        )
        assertEquals(
            exercise.toString(),
            "{\"goalReps\":123,\"exerciseResults\":[{\"date\":\"12.01.20\",\"reps\":20,\"weight\":0,\"id\":\"5b392d44-1452-4704-9daf-de4cf1e6ad80\"}],\"reps\":20,\"groupId\":2,\"name\":\"Отжимания\",\"weight\":0,\"goalWeight\":0,\"id\":\"994814c0-5cf6-442d-9635-6a1b6e6de483\"}"
        )

        exercise = trainExercisesUtils.addExerciseGoal(
            2,
            "994814c0-5cf6-442d-9635-6a1b6e6de483",
            false,
            123,
            2,
            filePath
        )

        File(filePath).forEachLine {
            testJSONArray = JSONArray(it)
        }

        assertEquals(
            exercise.toString(),
            testJSONArray.getJSONObject(1).getJSONArray("muscleGroupsList").getJSONObject(1)
                .getJSONArray("exercises").getJSONObject(0).toString()
        )
        assertEquals(
            exercise.toString(),
            "{\"goalReps\":123,\"exerciseResults\":[{\"date\":\"12.01.20\",\"reps\":20,\"weight\":0,\"id\":\"5b392d44-1452-4704-9daf-de4cf1e6ad80\"}],\"reps\":20,\"groupId\":2,\"name\":\"Отжимания\",\"weight\":0,\"goalWeight\":123,\"id\":\"994814c0-5cf6-442d-9635-6a1b6e6de483\"}"
        )
    }

    @Test(expected = FileNotFoundException::class)
    fun addExerciseGoalWithIncorrectFilePath() {
        trainExercisesUtils.addExerciseGoal(
            2,
            "994814c0-5cf6-442d-9635-6a1b6e6de483",
            true,
            123,
            2,
            "filePath"
        )
    }

    @Test(expected = NullPointerException::class)
    fun addExerciseGoalWithIncorrectDayId() {
        trainExercisesUtils.addExerciseGoal(
            2,
            "994814c0-5cf6-442d-9635-6a1b6e6de483",
            true,
            123,
            40,
            filePath
        )
    }

    @Test(expected = NullPointerException::class)
    fun addExerciseGoalWithIncorrectGroupId() {
        trainExercisesUtils.addExerciseGoal(
            5,
            "994814c0-5cf6-442d-9635-6a1b6e6de483",
            true,
            123,
            2,
            filePath
        )
    }

    @Test(expected = NullPointerException::class)
    fun addExerciseGoalWithIncorrectExerciseId() {
        trainExercisesUtils.addExerciseGoal(
            2,
            "995814c0-5cf6-442d-9635-6a1b6e6de483",
            true,
            123,
            2,
            filePath
        )
    }

    //removeExerciseResultTest
    @Test
    fun removeExerciseResultTest() {
        val exerciseResultsList = trainExercisesUtils.getExerciseResults(
            2,
            2,
            "994814c0-5cf6-442d-9635-6a1b6e6de483",
            filePath
        )
        val previousExerciseResult =
            Gson().fromJson(exerciseResultsList[0].toString(), ExerciseResult::class.java)

        val exerciseResults = trainExercisesUtils.removeExerciseResult(
            "5b392d44-1452-4704-9daf-de4cf1e6ad80",
            2,
            "994814c0-5cf6-442d-9635-6a1b6e6de483",
            previousExerciseResult,
            2,
            false,
            filePath
        )

        File(filePath).forEachLine {
            testJSONArray = JSONArray(it)
        }

        assertEquals(exerciseResults.toString(), "[]")
        assertEquals(
            exerciseResults.toString(),
            testJSONArray.getJSONObject(1).getJSONArray("muscleGroupsList").getJSONObject(1)
                .getJSONArray("exercises").getJSONObject(0).getJSONArray("exerciseResults")
                .toString()
        )
    }

    @Test
    fun removeExerciseResultWithChangingExercise() {
        val exerciseResultsList = trainExercisesUtils.getExerciseResults(
            2,
            6,
            "f44fa95a-6bdc-49ac-8375-cee79b3ce79d",
            filePath
        )
        val previousExerciseResult =
            Gson().fromJson(exerciseResultsList[0].toString(), ExerciseResult::class.java)

        val exerciseResults = trainExercisesUtils.removeExerciseResult(
            "f556444f-d953-41e8-8f6f-913c18476dfd",
            6,
            "f44fa95a-6bdc-49ac-8375-cee79b3ce79d",
            previousExerciseResult,
            2,
            true,
            filePath
        )

        File(filePath).forEachLine {
            testJSONArray = JSONArray(it)
        }

        assertEquals(
            exerciseResults.toString(),
            "[ExerciseResult(id=946b00d6-71d9-4621-87d4-777c37830161, date=23.03.20, weight=0.0, reps=10)]"
        )
        assertEquals(
            testJSONArray.getJSONObject(1).getJSONArray("muscleGroupsList").getJSONObject(5)
                .getJSONArray("exercises").getJSONObject(1).getJSONArray("exerciseResults")
                .toString(),
            "[{\"date\":\"23.03.20\",\"reps\":10,\"weight\":0,\"id\":\"946b00d6-71d9-4621-87d4-777c37830161\"}]"
        )
    }

    @Test(expected = FileNotFoundException::class)
    fun removeExerciseResultWithIncorrectFilePath() {
        val exerciseResultsList = trainExercisesUtils.getExerciseResults(
            2,
            6,
            "f44fa95a-6bdc-49ac-8375-cee79b3ce79d",
            filePath
        )

        val previousExerciseResult =
            Gson().fromJson(exerciseResultsList[0].toString(), ExerciseResult::class.java)

        trainExercisesUtils.removeExerciseResult(
            "f556444f-d953-41e8-8f6f-913c18476dfd",
            6,
            "f44fa95a-6bdc-49ac-8375-cee79b3ce79d",
            previousExerciseResult,
            2,
            true,
            "filePath"
        )
    }

    @Test(expected = NullPointerException::class)
    fun removeExerciseResultWithIncorrectResultId() {
        val exerciseResultsList = trainExercisesUtils.getExerciseResults(
            2,
            6,
            "f44fa95a-6bdc-49ac-8375-cee79b3ce79d",
            filePath
        )

        val previousExerciseResult =
            Gson().fromJson(exerciseResultsList[0].toString(), ExerciseResult::class.java)

        trainExercisesUtils.removeExerciseResult(
            "d556444f-d953-41e8-8f6f-913c18476dfd",
            6,
            "f44fa95a-6bdc-49ac-8375-cee79b3ce79d",
            previousExerciseResult,
            2,
            true,
            filePath
        )
    }

    @Test(expected = NullPointerException::class)
    fun removeExerciseResultWithIncorrectDayId() {
        val exerciseResultsList = trainExercisesUtils.getExerciseResults(
            2,
            6,
            "f44fa95a-6bdc-49ac-8375-cee79b3ce79d",
            filePath
        )

        val previousExerciseResult =
            Gson().fromJson(exerciseResultsList[0].toString(), ExerciseResult::class.java)

        trainExercisesUtils.removeExerciseResult(
            "f556444f-d953-41e8-8f6f-913c18476dfd",
            6,
            "f44fa95a-6bdc-49ac-8375-cee79b3ce79d",
            previousExerciseResult,
            3,
            true,
            filePath
        )
    }

    @Test(expected = NullPointerException::class)
    fun removeExerciseResultWithIncorrectGroupId() {
        val exerciseResultsList = trainExercisesUtils.getExerciseResults(
            2,
            6,
            "f44fa95a-6bdc-49ac-8375-cee79b3ce79d",
            filePath
        )

        val previousExerciseResult =
            Gson().fromJson(exerciseResultsList[0].toString(), ExerciseResult::class.java)

        trainExercisesUtils.removeExerciseResult(
            "f556444f-d953-41e8-8f6f-913c18476dfd",
            5,
            "f44fa95a-6bdc-49ac-8375-cee79b3ce79d",
            previousExerciseResult,
            2,
            true,
            filePath
        )
    }

    @Test(expected = NullPointerException::class)
    fun removeExerciseResultWithIncorrectExerciseId() {
        val exerciseResultsList = trainExercisesUtils.getExerciseResults(
            2,
            6,
            "f44fa95a-6bdc-49ac-8375-cee79b3ce79d",
            filePath
        )

        val previousExerciseResult =
            Gson().fromJson(exerciseResultsList[0].toString(), ExerciseResult::class.java)

        trainExercisesUtils.removeExerciseResult(
            "f556444f-d953-41e8-8f6f-913c18476dfd",
            6,
            "d44fa95a-6bdc-49ac-8375-cee79b3ce79d",
            previousExerciseResult,
            2,
            true,
            filePath
        )
    }

    //addExerciseResultTest
    @Test
    fun addExerciseRepsResultTest() {
        val exerciseResults = trainExercisesUtils.addResult(
            2,
            "994814c0-5cf6-442d-9635-6a1b6e6de483",
            true,
            ArrayList(),
            2,
            "29.03.20",
            UUID.fromString("d44fa95a-6bdc-49ac-8375-cee79b3ce79d"),
            "123",
            filePath
        )

        File(filePath).forEachLine {
            testJSONArray = JSONArray(it)
        }

        assertEquals(exerciseResults.toString(), "[ExerciseResult(id=d44fa95a-6bdc-49ac-8375-cee79b3ce79d, date=29.03.20, weight=0.0, reps=123)]")
        assertEquals(testJSONArray.getJSONObject(1).getJSONArray("muscleGroupsList").getJSONObject(1).getJSONArray("exercises").getJSONObject(0).getJSONArray("exerciseResults").toString(), "[{\"date\":\"12.01.20\",\"reps\":20,\"weight\":0,\"id\":\"5b392d44-1452-4704-9daf-de4cf1e6ad80\"},{\"date\":\"29.03.20\",\"reps\":123,\"weight\":0,\"id\":\"d44fa95a-6bdc-49ac-8375-cee79b3ce79d\"}]")
        assertEquals(testJSONArray.getJSONObject(1).getJSONArray("muscleGroupsList").getJSONObject(1).getJSONArray("exercises").getJSONObject(0).getInt("reps"), 123)
    }

    @Test
    fun addExerciseWeightResultTest() {
        val exerciseResults = trainExercisesUtils.addResult(
            2,
            "994814c0-5cf6-442d-9635-6a1b6e6de483",
            false,
            ArrayList(),
            2,
            "29.03.20",
            UUID.fromString("d44fa95a-6bdc-49ac-8375-cee79b3ce79d"),
            "123",
            filePath
        )

        File(filePath).forEachLine {
            testJSONArray = JSONArray(it)
        }

        assertEquals(exerciseResults.toString(), "[ExerciseResult(id=d44fa95a-6bdc-49ac-8375-cee79b3ce79d, date=29.03.20, weight=123.0, reps=20)]")
        assertEquals(testJSONArray.getJSONObject(1).getJSONArray("muscleGroupsList").getJSONObject(1).getJSONArray("exercises").getJSONObject(0).getJSONArray("exerciseResults").toString(), "[{\"date\":\"12.01.20\",\"reps\":20,\"weight\":0,\"id\":\"5b392d44-1452-4704-9daf-de4cf1e6ad80\"},{\"date\":\"29.03.20\",\"reps\":20,\"weight\":123,\"id\":\"d44fa95a-6bdc-49ac-8375-cee79b3ce79d\"}]")
        assertEquals(testJSONArray.getJSONObject(1).getJSONArray("muscleGroupsList").getJSONObject(1).getJSONArray("exercises").getJSONObject(0).getInt("weight"), 123)
    }

    @Test(expected = FileNotFoundException::class)
    fun addExerciseResultWithIncorrectFilePath() {
        trainExercisesUtils.addResult(
            2,
            "994814c0-5cf6-442d-9635-6a1b6e6de483",
            false,
            ArrayList(),
            2,
            "29.03.20",
            UUID.fromString("d44fa95a-6bdc-49ac-8375-cee79b3ce79d"),
            "123",
            "filePath"
        )
    }

    @Test(expected = NullPointerException::class)
    fun addExerciseResultWithIncorrectDayId() {
        trainExercisesUtils.addResult(
            2,
            "994814c0-5cf6-442d-9635-6a1b6e6de483",
            false,
            ArrayList(),
            3,
            "29.03.20",
            UUID.fromString("d44fa95a-6bdc-49ac-8375-cee79b3ce79d"),
            "123",
            filePath
        )
    }

    @Test(expected = NullPointerException::class)
    fun addExerciseResultWithIncorrectGroupId() {
        trainExercisesUtils.addResult(
            1,
            "994814c0-5cf6-442d-9635-6a1b6e6de483",
            false,
            ArrayList(),
            2,
            "29.03.20",
            UUID.fromString("d44fa95a-6bdc-49ac-8375-cee79b3ce79d"),
            "123",
            filePath
        )
    }

    @Test(expected = NullPointerException::class)
    fun addExerciseResultWithIncorrectExerciseId() {
        trainExercisesUtils.addResult(
            2,
            "194814c0-5cf6-442d-9635-6a1b6e6de483",
            false,
            ArrayList(),
            2,
            "29.03.20",
            UUID.fromString("d44fa95a-6bdc-49ac-8375-cee79b3ce79d"),
            "123",
            filePath
        )
    }
}