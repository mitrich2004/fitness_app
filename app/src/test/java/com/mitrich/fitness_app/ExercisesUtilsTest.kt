@file:Suppress("DEPRECATION")

package com.mitrich.fitness_app

import com.mitrich.fitness_app.models.Exercise
import com.mitrich.fitness_app.models.ExerciseResult
import com.mitrich.fitness_app.utils.ExercisesUtils
import com.google.gson.Gson
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import junit.framework.Assert.assertEquals
import org.json.JSONArray
import org.json.JSONObject
import org.junit.Assert.assertNotEquals
import org.junit.Before
import org.junit.Test
import java.io.File
import java.io.FileNotFoundException
import java.util.*
import kotlin.collections.ArrayList

class ExercisesUtilsTest {
    private lateinit var exercisesUtils: ExercisesUtils
    private lateinit var testJSONArray: JSONArray
    private lateinit var testJSONObject: JSONObject
    private val filePath =
        "C:\\Users\\User\\Documents\\Sergey\\fitness_app\\app\\src\\test\\resources\\test.json"
    private val userFilePath =
        "C:\\Users\\User\\Documents\\Sergey\\fitness_app\\app\\src\\test\\resources\\userTest.json"

    @Before
    fun setUp() {
        exercisesUtils = ExercisesUtils()
        File(filePath).writeText(
            JSONArray(
                "[{\"exercises\":[{\"exerciseResults\":[{\"date\":\"09.12.19\",\"id\":\"86e43a64-824c-4e97-bf43-96d6065a7d41\",\"reps\":\"20\",\"weight\":\"0.0\"}],\"goalReps\":0,\"goalWeight\":0,\"groupId\":1,\"id\":\"fe8656e2-99d7-47de-8687-5530a843a9d7\",\"name\":\"Приседания\",\"reps\":20,\"weight\":0}],\"id\":1,\"isDisplayed\":true,\"name\":\"Ноги\"},{\"exercises\":[{\"exerciseResults\":[{\"date\":\"09.12.19\",\"id\":\"572236c2-10b8-4c2e-af73-31e96fad5aa8\",\"reps\":\"20\",\"weight\":\"0.0\"}, {\"date\":\"22.03.20\",\"id\":\"582236c2-10b8-4c2e-af73-31e96fad5aa8\",\"reps\":\"30\",\"weight\":\"5.0\"}],\"goalReps\":0,\"goalWeight\":0,\"groupId\":2,\"id\":\"d9d044c7-c7b4-45f0-a837-dab5c883a66e\",\"name\":\"Отжимания\",\"reps\":\"30\",\"weight\":\"5.0\"}],\"id\":2,\"isDisplayed\":true,\"name\":\"Грудь\"},{\"exercises\":[{\"exerciseResults\":[{\"date\":\"09.12.19\",\"id\":\"e2724607-a184-4d43-b6fa-503e3c9988f2\",\"reps\":\"7\",\"weight\":\"0.0\"}],\"goalReps\":0,\"goalWeight\":0,\"groupId\":3,\"id\":\"bad8c833-d1c0-4b37-ac9d-04c5bac07fc4\",\"name\":\"Подтягивания\",\"reps\":7,\"weight\":0}],\"id\":3,\"isDisplayed\":true,\"name\":\"Спина\"},{\"exercises\":[], \"id\": 6, \"isDisplayed\":true, \"name\": \"Прочее\"}, {\"exercises\":[{\"exerciseResults\":[{\"date\":\"09.12.19\",\"id\":\"d69fa1df-e6fa-4728-a485-75bc4aad30dc\",\"reps\":\"20\",\"weight\":\"0.0\"}],\"goalReps\":0,\"goalWeight\":0,\"groupId\":5,\"id\":\"02cc58dd-a018-435a-a858-2687625c54bf\",\"name\":\"Отжимания\",\"reps\":20,\"weight\":0},{\"exerciseResults\":[{\"date\":\"09.12.19\",\"id\":\"c9cb2951-0939-4c96-9bdd-835c69d4e4ee\",\"reps\":\"7\",\"weight\":\"0.0\"}],\"goalReps\":0,\"goalWeight\":0,\"groupId\":5,\"id\":\"ffad6569-72aa-4fac-a6d4-1e7d5f0ec6fa\",\"name\":\"Подтягивания\",\"reps\":7,\"weight\":0}],\"id\":5,\"isDisplayed\":true,\"name\":\"Руки\"}, {\"exercises\":[],\"id\":4,\"isDisplayed\":true,\"name\":\"Плечи\"}]"
            ).toString()
        )

        File(userFilePath).writeText(
            JSONObject("{\"currentWeight\":51.0,\"darkTheme\":true,\"goalWeight\":60.0,\"height\":180,\"imperialSystem\":false,\"monthOfBirth\":4,\"quote\":\"Developer\",\"sex\":\"Мужской\",\"trainProgram\":false,\"username\":\"mitrich2004\",\"yearOfBirth\":2004}").toString()
        )

        File(filePath).forEachLine {
            testJSONArray = JSONArray(it)
        }

        File(userFilePath).forEachLine {
            testJSONObject = JSONObject(it)
        }
    }

    //getWorkoutListTest
    @Test
    fun getWorkoutListTest() {
        assertEquals(exercisesUtils.getWorkoutList(filePath).toString(), testJSONArray.toString())
    }

    @Test(expected = FileNotFoundException::class)
    fun getWorkoutListWithIncorrectFilePath() {
        exercisesUtils.getWorkoutList("test")
    }

    //getExerciseGroupTest
    @Test
    fun getExerciseGroupTest() {
        assertEquals(
            exercisesUtils.getExerciseGroup(1, filePath).toString(),
            testJSONArray.getJSONObject(0).toString()
        )
        assertEquals(
            exercisesUtils.getExerciseGroup(3, filePath).toString(),
            testJSONArray.getJSONObject(2).toString()
        )
        assertEquals(
            exercisesUtils.getExerciseGroup(6, filePath).toString(),
            testJSONArray.getJSONObject(3).toString()
        )
    }

    @Test(expected = FileNotFoundException::class)
    fun getExerciseGroupWithIncorrectFilePath() {
        exercisesUtils.getExerciseGroup(5, "test")
    }

    @Test(expected = NullPointerException::class)
    fun getExerciseGroupWithIncorrectId() {
        exercisesUtils.getExerciseGroup(7, filePath)
    }

    //getExerciseGroupIndexTest
    @Test
    fun getExerciseGroupIndexTest() {
       assertEquals(exercisesUtils.getExerciseGroupIndex(6, filePath), 3 )
    }

    @Test(expected = FileNotFoundException::class)
    fun getExerciseGroupIndexWithIncorrectFilePath() {
        exercisesUtils.getExerciseGroupIndex(6, "filePath")
    }

    @Test(expected = NullPointerException::class)
    fun getExerciseGroupIndexWithIncorrectGroupId() {
        exercisesUtils.getExerciseGroupIndex(0, filePath)
    }

    //getExercisesListTest
    @Test
    fun getExercisesListTest() {
        assertEquals(
            exercisesUtils.getExercisesList(1, filePath).toString(),
            testJSONArray.getJSONObject(0).getJSONArray("exercises").toString()
        )
        assertEquals(
            exercisesUtils.getExercisesList(3, filePath).toString(),
            testJSONArray.getJSONObject(2).getJSONArray("exercises").toString()
        )
        assertEquals(
            exercisesUtils.getExercisesList(6, filePath).toString(),
            testJSONArray.getJSONObject(3).getJSONArray("exercises").toString()
        )
    }

    @Test(expected = FileNotFoundException::class)
    fun getExercisesListWithIncorrectFilePath() {
        exercisesUtils.getExercisesList(5, "test")
    }

    @Test(expected = NullPointerException::class)
    fun getExercisesListWithIncorrectId() {
        exercisesUtils.getExercisesList(0, filePath)
    }

    //getExerciseTest
    @Test
    fun getExerciseTest() {
        assertEquals(
            exercisesUtils.getExercise(1, "fe8656e2-99d7-47de-8687-5530a843a9d7", filePath)
                .toString(),
            testJSONArray.getJSONObject(0).getJSONArray("exercises").getJSONObject(0).toString()
        )
        assertEquals(
            exercisesUtils.getExercise(5, "ffad6569-72aa-4fac-a6d4-1e7d5f0ec6fa", filePath)
                .toString(),
            testJSONArray.getJSONObject(4).getJSONArray("exercises").getJSONObject(1).toString()
        )
    }

    @Test(expected = FileNotFoundException::class)
    fun getExerciseWithIncorrectFilePath() {
        exercisesUtils.getExercise(5, "ffad6569-72aa-4fac-a6d4-1e7d5f0ec6fa", "test")
    }

    @Test(expected = NullPointerException::class)
    fun getExerciseWithIncorrectGroupId() {
        exercisesUtils.getExercise(2, "ffad6569-72aa-4fac-a6d4-1e7d5f0ec6fa", filePath)
    }

    @Test(expected = NullPointerException::class)
    fun getExerciseWithIncorrectExerciseId() {
        exercisesUtils.getExercise(4, "test", filePath)
    }

    //getExerciseIndexTest
    @Test
    fun getExerciseIndexTest() {
        assertEquals(exercisesUtils.getExerciseIndex(1, "fe8656e2-99d7-47de-8687-5530a843a9d7", filePath), 0)
    }

    @Test(expected = FileNotFoundException::class)
    fun getExerciseIndexIncorrectFilePath() {
        exercisesUtils.getExerciseIndex(5, "ffad6569-72aa-4fac-a6d4-1e7d5f0ec6fa", "test")
    }

    @Test(expected = NullPointerException::class)
    fun getExerciseIndexWithIncorrectGroupId() {
        exercisesUtils.getExerciseIndex(3, "ffad6569-72aa-4fac-a6d4-1e7d5f0ec6fa", filePath)
    }

    @Test(expected = NullPointerException::class)
    fun getExerciseIndexWithIncorrectExerciseId() {
        exercisesUtils.getExerciseIndex(4, "test", filePath)
    }

    //getImperialSystemTest
    @Test
    fun getImperialSystemTest() {
        assertEquals(
            exercisesUtils.getImperialSystem(userFilePath),
            testJSONObject.getBoolean("imperialSystem")
        )
    }

    @Test(expected = FileNotFoundException::class)
    fun getImperialSystemWithIncorrectFilePath() {
        exercisesUtils.getImperialSystem("test")
    }

    //getExerciseResultsTest
    @Test
    fun getExerciseResultsListTest() {
        assertEquals(
            exercisesUtils.getExerciseResultsList(
                1,
                "fe8656e2-99d7-47de-8687-5530a843a9d7",
                filePath
            ).toString(),
            testJSONArray.getJSONObject(0).getJSONArray("exercises").getJSONObject(0)
                .getJSONArray("exerciseResults").toString()
        )
    }

    @Test(expected = FileNotFoundException::class)
    fun getExerciseResultsListWithIncorrectPath() {
        exercisesUtils.getExerciseResultsList(1, "fe8656e2-99d7-47de-8687-5530a843a9d7", "test")
    }

    @Test(expected = NullPointerException::class)
    fun getExerciseResultsListWithIncorrectGroupId() {
        exercisesUtils.getExerciseResultsList(2, "fe8656e2-99d7-47de-8687-5530a843a9d7", filePath)
    }

    @Test(expected = NullPointerException::class)
    fun getExerciseResultsListWithIncorrectExerciseId() {
        exercisesUtils.getExerciseResultsList(1, "test", filePath)
    }

    //getExerciseResultIndexTest
    @Test
    fun getExerciseResultIndexTest() {
        assertEquals(
            exercisesUtils.getExerciseResultIndex(
                1,
                "fe8656e2-99d7-47de-8687-5530a843a9d7",
                UUID.fromString("86e43a64-824c-4e97-bf43-96d6065a7d41"),
                filePath
            ), 0
        )
    }

    @Test(expected = FileNotFoundException::class)
    fun getExerciseResultIndexWithIncorrectFilePath() {
        exercisesUtils.getExerciseResultIndex(
            1,
            "fe8656e2-99d7-47de-8687-5530a843a9d7",
            UUID.fromString("86e43a64-824c-4e97-bf43-96d6065a7d41"),
            "test"
        )
    }

    @Test(expected = NullPointerException::class)
    fun getExerciseResultIndexWithIncorrectGroupId() {
        exercisesUtils.getExerciseResultIndex(
            2,
            "fe8656e2-99d7-47de-8687-5530a843a9d7",
            UUID.fromString("86e43a64-824c-4e97-bf43-96d6065a7d41"),
            filePath
        )
    }

    @Test(expected = NullPointerException::class)
    fun getExerciseResultWithIncorrectExerciseId() {
        exercisesUtils.getExerciseResultIndex(
            1,
            "test",
            UUID.fromString("86e43a64-824c-4e97-bf43-96d6065a7d41"),
            filePath
        )
    }

    @Test(expected = NullPointerException::class)
    fun getExerciseResultIndexWithIncorrectExerciseResultId() {
        exercisesUtils.getExerciseResultIndex(
            1,
            "fe8656e2-99d7-47de-8687-5530a843a9d7",
            UUID.fromString("86e44a64-824c-4e97-bf43-96d6065a7d41"),
            filePath
        )
    }

    //getExerciseReps
    @Test
    fun getExerciseRepsTest() {
        assertEquals(
            exercisesUtils.getExerciseReps(
                1,
                "fe8656e2-99d7-47de-8687-5530a843a9d7",
                filePath
            ), 20
        )
    }

    @Test(expected = FileNotFoundException::class)
    fun getExerciseRepsWithIncorrectFilePath() {
        exercisesUtils.getExerciseReps(1, "fe8656e2-99d7-47de-8687-5530a843a9d7", "test")
    }

    @Test(expected = NullPointerException::class)
    fun getExerciseRepsWithIncorrectGroupId() {
        exercisesUtils.getExerciseReps(2, "fe8656e2-99d7-47de-8687-5530a843a9d7", filePath)
    }

    @Test(expected = NullPointerException::class)
    fun getExerciseRepsWithIncorrectExerciseId() {
        exercisesUtils.getExerciseReps(1, "ffad6569-72aa-4fac-a6d4-1e7d5f0ec6fa", filePath)
    }

    //getExerciseReps
    @Test
    fun getExerciseWeightTest() {
        assertEquals(
            exercisesUtils.getExerciseWeight(
                1,
                "fe8656e2-99d7-47de-8687-5530a843a9d7",
                filePath
            ), 0.0
        )
    }

    @Test(expected = FileNotFoundException::class)
    fun getExerciseWeightWithIncorrectFilePath() {
        exercisesUtils.getExerciseReps(1, "fe8656e2-99d7-47de-8687-5530a843a9d7", "test")
    }

    @Test(expected = NullPointerException::class)
    fun getExerciseWeightWithIncorrectGroupId() {
        exercisesUtils.getExerciseReps(2, "fe8656e2-99d7-47de-8687-5530a843a9d7", filePath)
    }

    @Test(expected = NullPointerException::class)
    fun getExerciseWeightWithIncorrectExerciseId() {
        exercisesUtils.getExerciseReps(1, "ffad6569-72aa-4fac-a6d4-1e7d5f0ec6fa", filePath)
    }

    //addExercise
    @Test
    fun addExerciseTest() {
        val exercisesList = exercisesUtils.addExercise(
            "test",
            "20",
            "0",
            1,
            GroupAdapter<ViewHolder>(),
            false,
            filePath,
            userFilePath,
            "16.03.20",
            UUID.fromString("516f2f8e-9a1d-478e-ae07-dd0bd1eb7a72"),
            UUID.fromString("6cbbaed0-dd6a-4bf2-a4bd-e436eef0f1c7")
        )

        File(filePath).forEachLine {
            testJSONArray = JSONArray(it)
        }

        assertEquals(
            exercisesList.toString(),
            "[{\"goalReps\":0,\"exerciseResults\":[{\"date\":\"09.12.19\",\"reps\":\"20\",\"weight\":\"0.0\",\"id\":\"86e43a64-824c-4e97-bf43-96d6065a7d41\"}],\"reps\":20,\"groupId\":1,\"name\":\"Приседания\",\"weight\":0,\"goalWeight\":0,\"id\":\"fe8656e2-99d7-47de-8687-5530a843a9d7\"},{\"goalReps\":0,\"reps\":20,\"exerciseResults\":[{\"date\":\"16.03.20\",\"reps\":20,\"weight\":0,\"id\":\"6cbbaed0-dd6a-4bf2-a4bd-e436eef0f1c7\"}],\"groupId\":1,\"name\":\"test\",\"weight\":0,\"goalWeight\":0,\"id\":\"516f2f8e-9a1d-478e-ae07-dd0bd1eb7a72\"}]"
        )
        assertEquals(
            exercisesList.toString(),
            testJSONArray.getJSONObject(0).getJSONArray("exercises").toString()
        )
    }

    @Test(expected = FileNotFoundException::class)
    fun addExerciseWithIncorrectFilePath() {
        exercisesUtils.addExercise(
            "test",
            "20",
            "0",
            1,
            GroupAdapter<ViewHolder>(),
            false,
            "test1",
            userFilePath,
            "16.03.20",
            UUID.randomUUID(),
            UUID.randomUUID()
        )
    }

    @Test(expected = FileNotFoundException::class)
    fun addExerciseWithIncorrectUserFilePath() {
        exercisesUtils.addExercise(
            "test",
            "20",
            "0",
            1,
            GroupAdapter<ViewHolder>(),
            false,
            filePath,
            "test2",
            "16.03.20",
            UUID.randomUUID(),
            UUID.randomUUID()
        )
    }

    @Test(expected = NullPointerException::class)
    fun addExerciseWithIncorrectGroupId() {
        exercisesUtils.addExercise(
            "test",
            "20",
            "0",
            0,
            GroupAdapter<ViewHolder>(),
            false,
            filePath,
            userFilePath,
            "16.03.20",
            UUID.randomUUID(),
            UUID.randomUUID()
        )
    }

    //editExerciseTest
    @Test
    fun editExerciseTest() {
        val newExercise = exercisesUtils.editExercise(
            "test",
            "123",
            "0",
            Gson().fromJson(
                testJSONArray.getJSONObject(0).getJSONArray("exercises").getJSONObject(0)
                    .toString(), Exercise::class.java
            ),
            GroupAdapter<ViewHolder>(),
            1,
            filePath,
            false,
            "17.03.20",
            UUID.fromString("ffad6569-72aa-4fac-a6d4-1e7d5f0ec6fa")
        )

        File(filePath).forEachLine {
            testJSONArray = JSONArray(it)
        }

        assertEquals(
            newExercise.toString(),
            "{\"goalReps\":0,\"exerciseResults\":[{\"date\":\"09.12.19\",\"reps\":\"20\",\"weight\":\"0.0\",\"id\":\"86e43a64-824c-4e97-bf43-96d6065a7d41\"},{\"date\":\"17.03.20\",\"reps\":123,\"weight\":0,\"id\":\"ffad6569-72aa-4fac-a6d4-1e7d5f0ec6fa\"}],\"reps\":\"123\",\"groupId\":1,\"name\":\"test\",\"weight\":\"0\",\"goalWeight\":0,\"id\":\"fe8656e2-99d7-47de-8687-5530a843a9d7\"}"
        )
        assertEquals(
            newExercise.toString(),
            testJSONArray.getJSONObject(0).getJSONArray("exercises").getJSONObject(0).toString()
        )
    }

    @Test(expected = FileNotFoundException::class)
    fun editExerciseWithIncorrectFilePath() {
        exercisesUtils.editExercise(
            "exerciseName",
            "123",
            "0",
            Gson().fromJson(
                testJSONArray.getJSONObject(0).getJSONArray("exercises").getJSONObject(0)
                    .toString(), Exercise::class.java
            ),
            GroupAdapter<ViewHolder>(),
            1,
            "test",
            false,
            "17.03.20",
            UUID.fromString("ffad6569-72aa-4fac-a6d4-1e7d5f0ec6fa")
        )
    }

    //removeExerciseTest
    @Test
    fun removeExerciseTest() {
        val exercisesList = exercisesUtils.removeExercise(
            "fe8656e2-99d7-47de-8687-5530a843a9d7",
            1,
            GroupAdapter<ViewHolder>(),
            filePath,
            false
        )

        File(filePath).forEachLine {
            testJSONArray = JSONArray(it)
        }

        assertEquals(exercisesList.toString(), "[]")
        val expectedExercisesList = testJSONArray.getJSONObject(0).getJSONArray("exercises")
        assertEquals(exercisesList.toString(), expectedExercisesList.toString())
    }

    @Test(expected = FileNotFoundException::class)
    fun removeExerciseWithIncorrectFilePath() {
        exercisesUtils.removeExercise(
            "fe8656e2-99d7-47de-8687-5530a843a9d7",
            1,
            GroupAdapter<ViewHolder>(),
            "test",
            false
        )
    }

    @Test(expected = NullPointerException::class)
    fun removeExerciseWithIncorrectExerciseId() {
        exercisesUtils.removeExercise(
            "test",
            1,
            GroupAdapter<ViewHolder>(),
            filePath,
            false
        )
    }

    @Test(expected = NullPointerException::class)
    fun removeExerciseWithIncorrectGroupId() {
        exercisesUtils.removeExercise(
            "fe8656e2-99d7-47de-8687-5530a843a9d7",
            6,
            GroupAdapter<ViewHolder>(),
            filePath,
            false
        )
    }
    //addExerciseResultTest
    @Test
    fun addExerciseRepsResultTest() {
        val exerciseResults = exercisesUtils.addExerciseResult(
            ArrayList(),
            1,
            "fe8656e2-99d7-47de-8687-5530a843a9d7",
            true,
            "123",
            filePath,
            "22.03.20",
            UUID.fromString("ffad6569-72aa-4fac-a6d4-1e7d5f0ec6fa")
        )

        File(filePath).forEachLine {
            testJSONArray = JSONArray(it)
        }

        val exercise = testJSONArray.getJSONObject(0).getJSONArray("exercises").getJSONObject(0)
        val jsonExerciseResults = exercise.getJSONArray("exerciseResults")

        assertEquals(
            exerciseResults.toString(),
            "[ExerciseResult(id=ffad6569-72aa-4fac-a6d4-1e7d5f0ec6fa, date=22.03.20, weight=0.0, reps=123)]"
        )
        assertEquals(
            jsonExerciseResults.toString(),
            "[{\"date\":\"09.12.19\",\"reps\":\"20\",\"weight\":\"0.0\",\"id\":\"86e43a64-824c-4e97-bf43-96d6065a7d41\"},{\"date\":\"22.03.20\",\"reps\":123,\"weight\":0,\"id\":\"ffad6569-72aa-4fac-a6d4-1e7d5f0ec6fa\"}]"
        )
    }

    @Test
    fun addExerciseWeightResultTest() {
        val oldJSONArray = testJSONArray
        val exerciseResults = exercisesUtils.addExerciseResult(
            ArrayList(),
            1,
            "fe8656e2-99d7-47de-8687-5530a843a9d7",
            false,
            "123",
            filePath,
            "22.03.20",
            UUID.fromString("ffad6569-72aa-4fac-a6d4-1e7d5f0ec6fa")
        )

        File(filePath).forEachLine {
            testJSONArray = JSONArray(it)
        }

        val exercise = testJSONArray.getJSONObject(0).getJSONArray("exercises").getJSONObject(0)
        val jsonExerciseResults = exercise.getJSONArray("exerciseResults")

        assertEquals(
            exerciseResults.toString(),
            "[ExerciseResult(id=ffad6569-72aa-4fac-a6d4-1e7d5f0ec6fa, date=22.03.20, weight=123.0, reps=20)]"
        )
        assertEquals(
            jsonExerciseResults.toString(),
            "[{\"date\":\"09.12.19\",\"reps\":\"20\",\"weight\":\"0.0\",\"id\":\"86e43a64-824c-4e97-bf43-96d6065a7d41\"},{\"date\":\"22.03.20\",\"reps\":20,\"weight\":123,\"id\":\"ffad6569-72aa-4fac-a6d4-1e7d5f0ec6fa\"}]"
        )
        assertNotEquals(testJSONArray.toString(), oldJSONArray.toString())
    }

    @Test(expected = FileNotFoundException::class)
    fun addExerciseResultWithIncorrectFilePath() {
        exercisesUtils.addExerciseResult(
            ArrayList(),
            1,
            "fe8656e2-99d7-47de-8687-5530a843a9d7",
            true,
            "123",
            "test",
            "22.03.20",
            UUID.fromString("ffad6569-72aa-4fac-a6d4-1e7d5f0ec6fa")
        )
    }

    @Test(expected = NullPointerException::class)
    fun addExerciseResultWithIncorrectGroupId() {
        exercisesUtils.addExerciseResult(
            ArrayList(),
            2,
            "fe8656e2-99d7-47de-8687-5530a843a9d7",
            true,
            "123",
            filePath,
            "22.03.20",
            UUID.fromString("ffad6569-72aa-4fac-a6d4-1e7d5f0ec6fa")
        )
    }

    @Test(expected = NullPointerException::class)
    fun addExerciseResultWithIncorrectExerciseId() {
        exercisesUtils.addExerciseResult(
            ArrayList(),
            1,
            "test",
            true,
            "123",
            filePath,
            "22.03.20",
            UUID.fromString("ffad6569-72aa-4fac-a6d4-1e7d5f0ec6fa")
        )
    }

    //removeExerciseResultTest
    @Test
    fun removeExerciseResultTest() {
        val exerciseResults = exercisesUtils.removeExerciseResult(
            ExerciseResult(
                UUID.fromString("86e43a64-824c-4e97-bf43-96d6065a7d41"),
                "09.12.19",
                0.0,
                20
            ),
            1,
            "fe8656e2-99d7-47de-8687-5530a843a9d7",
            ExerciseResult(
                UUID.fromString("86e43a64-824c-4e97-bf43-96d6065a7d41"),
                "09.12.19",
                0.0,
                20
            ),
            filePath,
            false
        )

        File(filePath).forEachLine {
            testJSONArray = JSONArray(it)
        }

        val exercise = testJSONArray.getJSONObject(0).getJSONArray("exercises").getJSONObject(0)
        val jsonExerciseResults = exercise.getJSONArray("exerciseResults")

        assertEquals(exerciseResults.toString(), "[]")
        assertEquals(jsonExerciseResults.toString(), "[]")
    }

    @Test
    fun removeExerciseResultWithChangingExercise() {
        val exerciseResults = exercisesUtils.removeExerciseResult(
            ExerciseResult(
                UUID.fromString("582236c2-10b8-4c2e-af73-31e96fad5aa8"),
                "22.03.20",
                5.0,
                30
            ),
            2,
            "d9d044c7-c7b4-45f0-a837-dab5c883a66e",
            ExerciseResult(
                UUID.fromString("572236c2-10b8-4c2e-af73-31e96fad5aa8"),
                "09.12.19",
                0.0,
                20
            ),
            filePath,
            true
        )

        File(filePath).forEachLine {
            testJSONArray = JSONArray(it)
        }

        val exercise = testJSONArray.getJSONObject(1).getJSONArray("exercises").getJSONObject(0)
        val reps = exercise.getInt("reps")
        val weight = exercise.getDouble("weight")
        val jsonExerciseResults = exercise.getJSONArray("exerciseResults")

        assertEquals(reps, 20)
        assertEquals(weight, 0.0)
        assertEquals(
            jsonExerciseResults.toString(),
            "[{\"date\":\"09.12.19\",\"reps\":\"20\",\"weight\":\"0.0\",\"id\":\"572236c2-10b8-4c2e-af73-31e96fad5aa8\"}]"
        )
        assertEquals(
            exerciseResults.toString(),
            "[ExerciseResult(id=572236c2-10b8-4c2e-af73-31e96fad5aa8, date=09.12.19, weight=0.0, reps=20)]"
        )
    }

    @Test(expected = FileNotFoundException::class)
    fun removeExerciseResultWithIncorrectFilePath() {
        exercisesUtils.removeExerciseResult(
            ExerciseResult(
                UUID.fromString("86e43a64-824c-4e97-bf43-96d6065a7d41"),
                "09.12.19",
                0.0,
                20
            ),
            1,
            "fe8656e2-99d7-47de-8687-5530a843a9d7",
            ExerciseResult(
                UUID.fromString("86e43a64-824c-4e97-bf43-96d6065a7d41"),
                "09.12.19",
                0.0,
                20
            ),
            "test",
            false
        )
    }

    @Test(expected = NullPointerException::class)
    fun removeExerciseResultWithIncorrectGroupId() {
        exercisesUtils.removeExerciseResult(
            ExerciseResult(
                UUID.fromString("86e43a64-824c-4e97-bf43-96d6065a7d41"),
                "09.12.19",
                0.0,
                20
            ),
            3,
            "fe8656e2-99d7-47de-8687-5530a843a9d7",
            ExerciseResult(
                UUID.fromString("86e43a64-824c-4e97-bf43-96d6065a7d41"),
                "09.12.19",
                0.0,
                20
            ),
            filePath,
            false
        )
    }

    @Test(expected = NullPointerException::class)
    fun removeExerciseResultWithIncorrectExerciseId() {
        exercisesUtils.removeExerciseResult(
            ExerciseResult(
                UUID.fromString("86e43a64-824c-4e97-bf43-96d6065a7d41"),
                "09.12.19",
                0.0,
                20
            ),
            1,
            "test",
            ExerciseResult(
                UUID.fromString("86e43a64-824c-4e97-bf43-96d6065a7d41"),
                "09.12.19",
                0.0,
                20
            ),
            filePath,
            false
        )
    }

    @Test(expected = NullPointerException::class)
    fun removeExerciseResultWIthIncorrectResultId() {
        exercisesUtils.removeExerciseResult(
            ExerciseResult(
                UUID.fromString("fe8656e2-99d7-47de-8687-5530a843a9d7"),
                "09.12.19",
                0.0,
                20
            ),
            1,
            "fe8656e2-99d7-47de-8687-5530a843a9d7",
            ExerciseResult(
                UUID.fromString("86e43a64-824c-4e97-bf43-96d6065a7d41"),
                "09.12.19",
                0.0,
                20
            ),
            filePath,
            false
        )
    }

    @Test(expected = NullPointerException::class)
    fun removeExerciseResultWIthIncorrectPreviousResultId() {
        exercisesUtils.removeExerciseResult(
            ExerciseResult(
                UUID.fromString("86e43a64-824c-4e97-bf43-96d6065a7d41"),
                "09.12.19",
                0.0,
                20
            ),
            1,
            "fe8656e2-99d7-47de-8687-5530a843a9d7",
            ExerciseResult(
                UUID.fromString("fe8656e2-99d7-47de-8687-5530a843a9d7"),
                "09.12.19",
                0.0,
                20
            ),
            filePath,
            true
        )
    }

    //addExerciseGoal
    @Test
    fun addExerciseRepsGoalTest() {
        val exercise = exercisesUtils.addExerciseGoal(1, "fe8656e2-99d7-47de-8687-5530a843a9d7", true, 50, filePath)
        File(filePath).forEachLine {
            testJSONArray = JSONArray(it)
        }
        assertEquals(exercise.getInt("goalReps"), testJSONArray.getJSONObject(0).getJSONArray("exercises").getJSONObject(0).getInt("goalReps"))
        assertEquals(testJSONArray.getJSONObject(0).getJSONArray("exercises").getJSONObject(0).getInt("goalReps"), 50)
    }

    @Test
    fun addExerciseWeightGoalTest() {
        val exercise = exercisesUtils.addExerciseGoal(1, "fe8656e2-99d7-47de-8687-5530a843a9d7", false, 50, filePath)
        File(filePath).forEachLine {
            testJSONArray = JSONArray(it)
        }
        assertEquals(exercise.getInt("goalWeight"), testJSONArray.getJSONObject(0).getJSONArray("exercises").getJSONObject(0).getInt("goalWeight"))
        assertEquals(testJSONArray.getJSONObject(0).getJSONArray("exercises").getJSONObject(0).getInt("goalWeight"), 50)
    }

    @Test(expected = FileNotFoundException::class)
    fun addExerciseGoalWithIncorrectFilePath() {
        exercisesUtils.addExerciseGoal(1, "fe8656e2-99d7-47de-8687-5530a843a9d7", false, 50,"test")
    }

    @Test(expected = NullPointerException::class)
    fun addExerciseGoalWithIncorrectGroupId() {
        exercisesUtils.addExerciseGoal(2, "fe8656e2-99d7-47de-8687-5530a843a9d7", false, 50, filePath)
    }

    @Test(expected = NullPointerException::class)
    fun addExerciseGoalWithIncorrectExerciseId() {
        exercisesUtils.addExerciseGoal(1, "test", false, 50, filePath)
    }
}