@file:Suppress("DEPRECATION")

package com.mitrich.fitness_app

import com.mitrich.fitness_app.utils.ProfileUtils
import junit.framework.Assert.assertEquals
import org.json.JSONObject
import org.junit.Before
import org.junit.Test
import java.io.File
import java.io.FileNotFoundException

class ProfileUtilsTest {
    private lateinit var profileExercisesUtils: ProfileUtils
    private lateinit var testJSONObject: JSONObject
    private val userFilePath = "C:\\Users\\User\\Documents\\Sergey\\fitness_app\\app\\src\\test\\resources\\userTest.json"

    @Before
    fun setUp() {
        profileExercisesUtils = ProfileUtils()

        File(userFilePath).writeText(
            JSONObject("{\"currentWeight\":51.0,\"darkTheme\":true,\"goalWeight\":60.0,\"height\":180,\"imperialSystem\":false,\"monthOfBirth\":4,\"quote\":\"Developer\",\"sex\":\"Мужской\",\"trainProgram\":false,\"username\":\"mitrich2004\",\"yearOfBirth\":2004}").toString()
        )

        File(userFilePath).forEachLine {
            testJSONObject = JSONObject(it)
        }
    }

    //getUserTest
    @Test
    fun getUserTest() {
        val actualUser = profileExercisesUtils.getUser(userFilePath)
        assertEquals(actualUser.toString(), testJSONObject.toString())
        assertEquals(actualUser.toString(), "{\"currentWeight\":51,\"quote\":\"Developer\",\"imperialSystem\":false,\"sex\":\"Мужской\",\"trainProgram\":false,\"darkTheme\":true,\"goalWeight\":60,\"monthOfBirth\":4,\"height\":180,\"username\":\"mitrich2004\",\"yearOfBirth\":2004}")
    }

    @Test(expected = FileNotFoundException::class)
    fun getUserWithIncorrectFilePath() {
        profileExercisesUtils.getUser("userFIlePath")
    }

    //changeNameTest
    @Test
    fun changeNameTest() {
        val actualUser = profileExercisesUtils.changeName(userFilePath, "Sergey")
        File(userFilePath).forEachLine {
            testJSONObject = JSONObject(it)
        }
        assertEquals(actualUser.username, testJSONObject.getString("username"))
        assertEquals(actualUser.username, "Sergey")
        assertEquals(testJSONObject.toString(), "{\"currentWeight\":51,\"quote\":\"Developer\",\"imperialSystem\":false,\"sex\":\"Мужской\",\"trainProgram\":false,\"darkTheme\":true,\"goalWeight\":60,\"monthOfBirth\":4,\"height\":180,\"username\":\"Sergey\",\"yearOfBirth\":2004}")
    }

    @Test(expected = FileNotFoundException::class)
    fun changeNameWithIncorrectFilePath() {
        profileExercisesUtils.changeName("userFilePath", "Sergey")
    }

    //changeQuoteTest
    @Test
    fun changeQuoteTest() {
        val actualUser = profileExercisesUtils.changeQuote(userFilePath, "Quote")
        File(userFilePath).forEachLine {
            testJSONObject = JSONObject(it)
        }
        assertEquals(actualUser.quote, testJSONObject.getString("quote"))
        assertEquals(actualUser.quote, "Quote")
        assertEquals(testJSONObject.toString(), "{\"currentWeight\":51,\"quote\":\"Quote\",\"imperialSystem\":false,\"sex\":\"Мужской\",\"trainProgram\":false,\"darkTheme\":true,\"goalWeight\":60,\"monthOfBirth\":4,\"height\":180,\"username\":\"mitrich2004\",\"yearOfBirth\":2004}")
    }

    @Test(expected = FileNotFoundException::class)
    fun changeQuoteWithIncorrectFilePath() {
        profileExercisesUtils.changeQuote("userFilePath", "Quote")
    }

    //changeSexTest
    @Test
    fun changeSexTest() {
        val actualUser = profileExercisesUtils.changeSex(userFilePath, "Man")
        File(userFilePath).forEachLine {
            testJSONObject = JSONObject(it)
        }
        assertEquals(actualUser.sex, testJSONObject.getString("sex"))
        assertEquals(actualUser.sex, "Man")
        assertEquals(testJSONObject.toString(), "{\"currentWeight\":51,\"quote\":\"Developer\",\"imperialSystem\":false,\"sex\":\"Man\",\"trainProgram\":false,\"darkTheme\":true,\"goalWeight\":60,\"monthOfBirth\":4,\"height\":180,\"username\":\"mitrich2004\",\"yearOfBirth\":2004}")
    }

    @Test(expected = FileNotFoundException::class)
    fun changeSexWithIncorrectFilePath() {
        profileExercisesUtils.changeSex("userFilePath", "Man")
    }

    //changeHeightTest
    @Test
    fun changeHeightTest() {
        val actualUser = profileExercisesUtils.changeHeight(userFilePath, 61, 181)
        File(userFilePath).forEachLine {
            testJSONObject = JSONObject(it)
        }
        assertEquals(actualUser.goalWeight, testJSONObject.getInt("goalWeight"))
        assertEquals(actualUser.height, testJSONObject.getInt("height"))
        assertEquals(actualUser.goalWeight, 61)
        assertEquals(actualUser.height, 181)
        assertEquals(testJSONObject.toString(), "{\"currentWeight\":51,\"quote\":\"Developer\",\"imperialSystem\":false,\"sex\":\"Мужской\",\"trainProgram\":false,\"darkTheme\":true,\"goalWeight\":61,\"monthOfBirth\":4,\"height\":181,\"username\":\"mitrich2004\",\"yearOfBirth\":2004}")
    }

    @Test(expected = FileNotFoundException::class)
    fun changeHeightWithIncorrectFilePath() {
        profileExercisesUtils.changeHeight("userFilePath", 61, 181)
    }

    //changeWeightTest
    @Test
    fun changeWeightTest() {
        val actualUser = profileExercisesUtils.changeWeight(userFilePath, 52)
        File(userFilePath).forEachLine {
            testJSONObject = JSONObject(it)
        }
        assertEquals(actualUser.currentWeight, testJSONObject.getInt("currentWeight"))
        assertEquals(actualUser.currentWeight, 52)
        assertEquals(testJSONObject.toString(), "{\"currentWeight\":52,\"quote\":\"Developer\",\"imperialSystem\":false,\"sex\":\"Мужской\",\"trainProgram\":false,\"darkTheme\":true,\"goalWeight\":60,\"monthOfBirth\":4,\"height\":180,\"username\":\"mitrich2004\",\"yearOfBirth\":2004}")
    }

    @Test(expected = FileNotFoundException::class)
    fun changeWeightWithIncorrectFilePath() {
        profileExercisesUtils.changeWeight("userFilePath", 52)
    }

    //changeAgeTest
    @Test
    fun changeAgeTest() {
        val actualUser = profileExercisesUtils.changeAge(userFilePath, 5, 2003)
        File(userFilePath).forEachLine {
            testJSONObject = JSONObject(it)
        }
        assertEquals(actualUser.monthOfBirth, testJSONObject.getInt("monthOfBirth"))
        assertEquals(actualUser.yearOfBirth, testJSONObject.getInt("yearOfBirth"))
        assertEquals(actualUser.monthOfBirth, 5)
        assertEquals(actualUser.yearOfBirth, 2003)
        assertEquals(testJSONObject.toString(), "{\"currentWeight\":51,\"quote\":\"Developer\",\"imperialSystem\":false,\"sex\":\"Мужской\",\"trainProgram\":false,\"darkTheme\":true,\"goalWeight\":60,\"monthOfBirth\":5,\"height\":180,\"username\":\"mitrich2004\",\"yearOfBirth\":2003}")
    }

    @Test(expected = FileNotFoundException::class)
    fun changeAgeWithIncorrectFilePath() {
        profileExercisesUtils.changeAge("userFilePath", 5, 2003)
    }

    //changeThemeTest
    @Test
    fun changeThemeTest() {
        val actualUser = profileExercisesUtils.changeTheme(userFilePath, false)
        File(userFilePath).forEachLine {
            testJSONObject = JSONObject(it)
        }
        assertEquals(actualUser.darkTheme, testJSONObject.getBoolean("darkTheme"))
        assertEquals(actualUser.darkTheme, false)
        assertEquals(testJSONObject.toString(), "{\"currentWeight\":51,\"quote\":\"Developer\",\"imperialSystem\":false,\"sex\":\"Мужской\",\"trainProgram\":false,\"darkTheme\":false,\"goalWeight\":60,\"monthOfBirth\":4,\"height\":180,\"username\":\"mitrich2004\",\"yearOfBirth\":2004}")
    }

    @Test(expected = FileNotFoundException::class)
    fun changeThemeWithIncorrectFilePath() {
        profileExercisesUtils.changeTheme("test", false)
    }

    //changeSystemTest
    @Test
    fun changeSystemTest() {
        val actualUser = profileExercisesUtils.changeSystem(userFilePath, true)
        File(userFilePath).forEachLine {
            testJSONObject = JSONObject(it)
        }
        assertEquals(actualUser.imperialSystem, testJSONObject.getBoolean("imperialSystem"))
        assertEquals(actualUser.imperialSystem, true)
        assertEquals(testJSONObject.toString(), "{\"currentWeight\":51,\"quote\":\"Developer\",\"imperialSystem\":true,\"sex\":\"Мужской\",\"trainProgram\":false,\"darkTheme\":true,\"goalWeight\":60,\"monthOfBirth\":4,\"height\":180,\"username\":\"mitrich2004\",\"yearOfBirth\":2004}")
    }

    @Test(expected = FileNotFoundException::class)
    fun changeSystemWithIncorrectFilePath() {
        profileExercisesUtils.changeSystem("userFilePath", true)
    }

    //changeProgramTest
    @Test
    fun changeProgramTest() {
        val actualUser = profileExercisesUtils.changeProgram(userFilePath, true)
        File(userFilePath).forEachLine {
            testJSONObject = JSONObject(it)
        }
        assertEquals(actualUser.trainProgram, testJSONObject.getBoolean("trainProgram"))
        assertEquals(actualUser.trainProgram, true)
        assertEquals(testJSONObject.toString(), "{\"currentWeight\":51,\"quote\":\"Developer\",\"imperialSystem\":false,\"sex\":\"Мужской\",\"trainProgram\":true,\"darkTheme\":true,\"goalWeight\":60,\"monthOfBirth\":4,\"height\":180,\"username\":\"mitrich2004\",\"yearOfBirth\":2004}")
    }

    @Test(expected = FileNotFoundException::class)
    fun changeProgramWithIncorrectFilePath() {
        profileExercisesUtils.changeProgram("test", true)
    }
}